## Common
---------

Any request may result in `HTTP 500 Internal Server Error` in case of server failure, thus, it will not be noted further below.

## Common types
---------------

* _CV_

	Example:

```
#!json
{
  "cv": {
    "name": {
      "firstname": "Sheldon",
      "lastname": "Cooper"
    },
    "position": "JavaScript developer",
    "summary": [],
    "expertise": [],
    "projects": [],
    "education": [],
    "certificates": [],
    "languages": [ "Russian", "English" ],
    "additionalInfo": []
  }
}
```

* _Location_

    Example:

```
#!json
{
  "code": "vitebsk",
  "descriptions": {
    "en": "Vitebsk",
    "ru": "Витебск"
  }
}
```

* _CVExcerpt_

    Example:

```
#!json
{
  "owner": "scooper",
  "firstname": "Sheldon",
  "lastname": "Cooper",
  "position": "JavaScript developer"
}
```

* _CVOverview_

    Example:

```
#!json
{
  "excerpt": <CVExcerpt>,
  "availableLanguages": ['en, 'ru', ...]
}
```

* _SearchData_

    Example:

```
#!json
{
  "users": {
    "scooper": <CVExcerpt>,
    "lhofstadter": <CVExcerpt>,
    ...
  },
  "tags": [
    "Java": ["scooper", "lhofstadter"],
    ...
  ],
  "projects": [
    "ART-TEAM": ["scooper", "lhofstadter"],
    ...
  ]
}
```

## Authentication
-----------------

* _Create new session._

	`POST /api/session`

	Request format:

	`{ "username": "my username", "password": "my password" }`

	Returns:

	* `HTTP 201 Created` with entity:

		`{ "sessionId": "my session id", role: "user" }`

	* `HTTP 400 Bad Request` response in case of authentication failure

* _Delete session._

	`DELETE /api/session/{sessionId}`

	Parameters:

	* sessionId - self explanatory

	Returns:

	* `HTTP 204 No Content` response in case of success
	* `HTTP 400 Bad Request` when non-existent session id supplied
	
* _Create new ticket._

    `POST /api/ticket`
    
    Existing session is required.

    Returns:

    * `HTTP 201 Created` with entity:

        `{ "ticketId": "my ticket id" }`

    * `HTTP 400 Bad Request` response in case of authentication failure

## CV
-----------------

* _Create new CV._

	`PUT /api/cv/{username}`

	Request format:

	`{ "cv": { ... }, "version": 0 }`

	Returns:

	* `HTTP 201 Created` response in following format:

		`{ "version": 0 }`

* _Update CV (put new version)._

	`PUT /api/cv/{username}`

	Request format:

	`{ "cv": { ... }, "version": <CV version> }`

	Returns:

	* `HTTP 201 Created` response in following format:

		`{ "version": <CV Version> }`

	* `HTTP 404 Not Found` when no matching CV found

* _Read CV._

	`GET /api/cv/{username}`

	Returns:

	* `HTTP 200 OK` response with entity:

		`{ "cv": <CV>, "language": <CV language>, "version": <CV version>, "lastModified": <date>, "lastModifiedBy": <username> }`

	* `HTTP 404 Not Found` when no matching CV found

* _Find all CVs_

    `GET /api/cv`

    Returns:

    * `HTTP 200 OK` response with entity:

    	`{ "results": <[CVOverview]> }`

* _Export CV (download file)_

    `GET /api/export/cv/pdf/username.pdf`

    Returns:

    * File download is initiated.

* _Group CV export (download archive)_

    `GET /api/export/cv/pdf`

    Ticket format:

    `{
       "entries": [
         { "owner": "scooper", "language": "en" },
         { "owner": "lhofstadter", "language": "ru" },
         ...
       ]
     }`

    Returns:

    * File download is initiated.

* _Export CV to email_

    `POST /api/export/cv/email`
    
    Request format:
    
    `{ "entries": ["scooper", "lhofstadter", ...] }`
    
    Returns:
    
    * `HTTP 200 OK` response

## CV drafts
-----------------

* _Set draft._

	`PUT /api/cv/draft/{username}`

	Request format:

	`{ "cv": { ... }, "version": <based on version> }`

	Returns:

	* `HTTP 200 OK` when draft successfully set

* _Read draft._

	`GET /api/cv/draft/{username}`

	Returns:

	* `HTTP 200 OK` response with entity:

		`{ "cv": <CV>, "language": <CV language>, "version": <based on version>, "lastModified": <date>, "lastModifiedBy": <username> }`

	* `HTTP 404 Not Found` when no matching draft found

* _Clear draft._

	`DELETE /api/cv/draft/{username}`

	Returns:

	* `HTTP 200 OK` when draft successfully cleared

	* `HTTP 404 Not Found` when no matching draft found

## Location
-----------

* _Get available locations._

    `GET /api/location`

    Returns:

    * `HTTP 200 OK` response with entity:

    `{ "locations": <[Location]> }`

## CV search data
-----------------

* _Get CV tags._

    `GET /api/searchdata`

    Returns:

    * `HTTP 200 OK` response with entity:

    `{ "results": <[SearchData]> }`
