module.exports = function (grunt) {
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    //------------------------------------------------------------
    less: { // Task less
      options: {
        expand: true
      },
      dev: { // Target
        options: {
          strictMath: true
        },
        files: {
          'css/app.css': ['less/app.less']
        }
      },
      release: { // Target
        options: {
          strictMath: true,
          yuicompress: true
        },
        files: {
          'css/app.css': ['less/app.less']
        }
      }
    },

    copy: {
      main: {
        files: [
          {expand: false, src: ['css/app.css'], dest: '../../src/frontend/src/main/resources/frontend/', filter: 'isFile'},
        ]
      }
    },

    //------------------------------------------------------------

    watch: {
      less: {
        files: 'less/**',
        tasks: ['less:dev', 'copy:main'],
        options: {
          interrupt: true
        }
      }

    }
    //------------------------------------------------------------
  });

  grunt.registerTask('default', ["less", 'watch']);
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
};