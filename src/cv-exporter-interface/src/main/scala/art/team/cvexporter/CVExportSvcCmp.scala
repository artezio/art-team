package art.team.cvexporter

import art.team.common.Format
import art.team.model.{CV, Language}
import org.joda.time.DateTime

import scala.concurrent.Future

trait CVExportSvcCmp {
  def cvExportSvc: CVExportSvc

  trait CVExportSvc {
    def export(cv: CV,
               language: Language.Value,
               version: Int,
               lastModified: DateTime,
               format: Format.Value): Future[Option[(Array[Byte], String)]]
  }
}
