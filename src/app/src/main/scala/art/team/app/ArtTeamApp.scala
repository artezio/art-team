package art.team.app

import art.team.auth.AuthenticatorRegistry
import art.team.controller.{ControllerRegistry, Routing}
import art.team.cvexporter.CVExportSvcImplCmp
import art.team.dao.DaoModuleMongoRegistry
import art.team.db.migration.MigrationModuleRegistry
import art.team.serverrenderer.ServerRendererCmp
import art.team.service.SvcModuleMongoRegistry
import spray.routing.SimpleRoutingApp

trait ArtTeamApp
  extends SimpleRoutingApp
  with DefaultAppWideDefs
  with DaoModuleMongoRegistry
  with SvcModuleMongoRegistry
  with CVExportSvcImplCmp
  with ServerRendererCmp
  with ControllerRegistry
  with AuthenticatorRegistry
  with Routing
  with MigrationModuleRegistry {

  abstract override def start(): Unit = {
    super.start()

    // setup proper shutdown routine
    actorSystem.registerOnTermination(stop())
    Runtime.getRuntime.addShutdownHook(new Thread {
      override def run(): Unit = actorSystem.shutdown()
    })

    // migrate database
    migrationSvc.runBlocking()

    // start web server
    startServer(interface = "0.0.0.0", port = appConfig.getInt(Conf.Network.Port))(routes)
  }

  abstract override def stop(): Unit = {
    super.stop()

    actorSystem.shutdown()
  }
}
