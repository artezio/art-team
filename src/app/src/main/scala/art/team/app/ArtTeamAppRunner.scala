package art.team.app

object ArtTeamAppRunner
  extends LifecycleImpl
  with ArtTeamApp
  with App {

  start()
}
