package art.team.service

import art.team.app.LifecycleImpl
import art.team.dao.SessionDaoMongoCmp
import art.team.db.TestMongoDBCmp

class SessionSvcMongoISpec
  extends SessionSvcISpec
  with LifecycleImpl
  with TestMongoDBCmp
  with SessionDaoMongoCmp
  with SessionSvcMongoCmp {

  start()
}
