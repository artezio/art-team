package art.team.service

import art.team.app.DefaultAppWideDefs
import art.team.common.util.FutureUtil
import org.scalatest.{Matchers, FlatSpec}
import scalaz._

trait SessionSvcISpec
  extends FlatSpec
  with Matchers
  with DefaultAppWideDefs
  with SessionSvcCmp {

  val username = "scooper"

  "session id" should "be hex string of length 64" in {
    val sessionId = createSession(username)
    sessionId.get should fullyMatch regex "\\p{XDigit}{64}"
  }

  "session service" should "successfully validate existing session" in {
    val result = for {
      sessionId <- OptionT.optionT(sessionSvc.create(username))
      caller <- OptionT.optionT(sessionSvc.verifyAndGetUsername(sessionId))
    } yield caller
    FutureUtil.extract(result.run) should be (Some(username))
  }

  // private

  private def createSession(username: String): Option[String] = {
    val result = sessionSvc.create(username)
    FutureUtil.extract(result)
  }
}
