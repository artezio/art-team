require.config({
  baseUrl: '/js',
  urlArgs: "v=2",
  paths: {
    react: 'libs/react/react',
    c: 'libs/react-createfactory/react-createfactory',
    immutable: 'libs/immutable/immutable.min',
    morearty: 'libs/morearty/morearty',
    transit: 'libs/transit/transit.min',
    director: 'libs/director/director-1.2.4.min',
    i18next: 'libs/i18next/i18next.min',
    jquery: 'libs/jquery/jquery-2.1.1.min',
    moment: 'libs/moment/moment-with-locales.min',
    pubsub: 'libs/pubsub/pubsub',
    bowser: 'libs/bowser/bowser-0.7.2.min',
    cookies: 'libs/cookies/cookies.min'
  }
});

require(['react', 'immutable', 'util/Polyfills'], function (React, Imm) {
  window.React = React;
  window.Immutable = Imm;

  require(['i18next', 'app/Context', 'component/AppRunner'],
    function (i18next, Ctx, AppRunner) {
      i18next.init(
        {
          resGetPath: '/js/locales/__lng__/__ns__.json',
          ns: { namespaces: ['translations'], defaultNs: 'translations' },
          load: 'unspecific',
          fallbackLng: 'en',
          preload: ['en', 'ru'],
          detectLngQS: 'lang',
          useCookie: false,
          getAsync: true
        },
        function () {
          var Bootstrap = Ctx.bootstrap(AppRunner);
          React.render(
            React.createFactory(Bootstrap)(),
            document.getElementById('root')
          );
        }
      );

    });
});
