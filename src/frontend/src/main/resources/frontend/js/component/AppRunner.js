define([
    'react',
    'morearty',
    'app/Router',
    'session/SessionStorage',
    'service/I18NSvc',
    'service/SessionSvc',
    'service/NotificationSvc',
    'service/PrefsSvc',
    'c!component/App'
  ],
  function (React, Morearty, Router, SessionStorage, I18NSvc, SessionSvc, NotificationSvc, PrefsSvc, App) {
    'use strict';

    var redirectToCorrectPage, initRouting;

    redirectToCorrectPage = function () {
      if (Router.isRootPath()) {
        var username = SessionStorage.getUsername();
        if (username) {
          Router.toCV(username);
        } else {
          Router.toLogin();
        }
      }
    };

    initRouting = function () {
      redirectToCorrectPage();
      Router.init();
    };

    var setupExitHandler = function () {
      window.onbeforeunload = function () {
        if (SessionSvc.isLoggedIn()) {
          SessionSvc.saveSession(this.getDefaultBinding().get());
        }
      }.bind(this);
    };

    return React.createClass({

      displayName: 'AppRunner',
      mixins: [Morearty.Mixin],

      componentDidMount: function () {
        var token = NotificationSvc.init();
        this.setState({ notificationSubscriptionToken: token });

        var sessionState = SessionSvc.loadSession();
        if (sessionState) {
          this.getMoreartyContext().replaceState(sessionState, { notify: false });
          NotificationSvc.cleanup();
          PrefsSvc.init();
        } else {
          I18NSvc.init();
        }

        initRouting.call(this);
        setupExitHandler.call(this);
      },

      componentWillUnmount: function () {
        NotificationSvc.destroy(this.state.notificationSubscriptionToken);
      },

      render: function () {
        return App({ binding: this.getDefaultBinding() });
      }

    });

  });
