define(['director', 'app/Context', 'app/NowShowing', 'app/Paths', 'session/SessionStorage', 'service/SessionSvc', 'service/PrefsSvc'],
  function (_Director, Ctx, NowShowing, Paths, SessionStorage, SessionSvc, PrefsSvc) {

    var binding = Ctx.getBinding();

    var getCurrentPath, storeLastPage, setNowShowing;

    getCurrentPath = function () {
      return window.location.pathname;
    };

    storeLastPage = function () {
      if (SessionSvc.isLoggedIn()) {
        binding.atomically().set('lastPage', getCurrentPath()).commit({ notify: false });
      }
    };

    setNowShowing = function (nowShowing) {
      binding.set('nowShowing', nowShowing);
    };

    var toLogin, toCV;

    toLogin = function (keepState) {
      PrefsSvc.destroy();
      if (!keepState) {
        Ctx.resetState('prefs', { notify: false });
        Ctx.resetState('sections', { notify: false });
        Ctx.getBinding().atomically().set('lastPage', null).commit({ notify: false });
      }
      router.setRoute(Paths.getLoginPath());
      setNowShowing(NowShowing.LOGIN);
    };

    toCV = function (username, edit) {
      storeLastPage();

      if (!SessionStorage.getUsername()) {
        SessionStorage.setUsername(username);
      }

      binding.atomically()
        .set('nowShowing', NowShowing.CV)
        .set('sections.cv.owner', username)
        .set('sections.cv.edit', edit)
        .commit();
    };

    var router = new Router();

    router.on(Paths.getPermissionViolationPath(), function () {
      setNowShowing(NowShowing.PERMISSION_VIOLATION);
    });
    router.on(Paths.getNotFoundPath(), function () {
      setNowShowing(NowShowing.NOT_FOUND);
    });

    router.on(Paths.getLoginPath(), function () {
      setNowShowing(NowShowing.LOGIN);
    });
    router.on(Paths.getLogoutPath(), function () {
      SessionSvc.logout().always(function () {
        setNowShowing(NowShowing.LOGOUT);
      });
    });

    router.on('/cv/:username', function (username) {
      toCV(username, false);
    });
    router.on('/cv/:username/edit', function (username) {
      toCV(username, true);
    });

    router.on(Paths.getPreferencesPath(), function () {
      storeLastPage();
      setNowShowing(NowShowing.PREFS);
    });

    router.on(Paths.getCVListPath(), function () {
      storeLastPage();
      setNowShowing(NowShowing.CVLIST);
    });
    router.on(Paths.getPickUpPath(), function () {
      storeLastPage();
      setNowShowing(NowShowing.PICKUP);
    });

    router.configure({
      html5history: true,
      notfound: function () {
        router.setRoute(Paths.getNotFoundPath());
        setNowShowing(NowShowing.NOT_FOUND);
      }
    });

    return {

      init: function () {
        window.onpopstate = function () {};
        router.init();
      },

      setRoute: function (path) {
        router.setRoute(path);
      },

      toLastVisitedPage: function () {
        var lastPage = Ctx.getBinding().get('lastPage');
        if (lastPage) {
          this.setRoute(lastPage);
        }
        return lastPage;
      },

      getCurrentPath: getCurrentPath,

      isRootPath: function () {
        var current = getCurrentPath();
        return !current || current === '/';
      },

      toLogin: toLogin,

      toLogout: function () {
        this.setRoute(Paths.getLogoutPath());
      },

      toPermissionViolation: function () {
        this.setRoute(Paths.getPermissionViolationPath());
      },

      toPreferences: function () {
        this.setRoute(Paths.getPreferencesPath());
      },

      toCV: function (username) {
        this.setRoute(Paths.getCVPath(username));
      },

      toCVEdit: function (username) {
        this.setRoute(Paths.getCVEditPath(username));
      },

      toCVList: function () {
        this.setRoute(Paths.getCVListPath());
      },

      toPickUp: function () {
        this.setRoute(Paths.getPickUpPath());
      }

    };

  });
