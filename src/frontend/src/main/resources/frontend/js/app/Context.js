define(['morearty', 'app/InitialState'], function (Morearty, InitialState) {
  'use strict';

  return Morearty.createContext(InitialState, { /* meta state */ }, {
    requestAnimationFrameEnabled: true
  });

});
