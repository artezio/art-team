package art.team.serverrenderer

import art.team.app.DefaultAppWideConf
import art.team.common.util.FutureUtil
import art.team.serverrenderer.model.NowShowing
import org.scalatest.{FlatSpec, Matchers}

class ServerRendererSpec
  extends FlatSpec
  with Matchers
  with DefaultAppWideConf
  with ServerRendererCmp {

  "render" should "TODO" in {
    FutureUtil.extract(serverRenderer.render(NowShowing.Login, None)) should include("server render")
  }
}
