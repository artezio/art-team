define(['immutable', 'morearty', 'app/InitialState'], function (Immutable, Morearty, InitialState) {
  'use strict';

  return function (spec) {
    var ctx = Morearty.createContext(InitialState, { /* meta state */ }, {
      requestAnimationFrameEnabled: false,
      renderOnce: true
    });

    var binding = ctx.getBinding();
    binding.set('nowShowing', spec.nowShowing());
    binding.set('login.username', spec.username());

    return ctx;
  };

});
