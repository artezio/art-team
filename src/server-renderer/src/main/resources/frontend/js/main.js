define(['react', 'i18next', 'app/ContextFactory', 'json!locales/en/translations', 'json!locales/ru/translations', 'util/Polyfills'],
  function (React, i18next, CtxFactory, en, ru) {
    'use strict';

    i18next.init({
      resStore: {
        en: { translations: en },
        ru: { translations: ru }
      },
      ns: { namespaces: ['translations'], defaultNs: 'translations' },
      load: 'unspecific',
      fallbackLng: 'en',
      preload: ['en', 'ru']
    });

    var UIRenderer = Java.type("art.team.serverrenderer.model.UIRenderer");
    return new UIRenderer(
      function (spec) {
        var ctx = CtxFactory(spec);
        __AMD_SCALA_PREDEFINE__('app/Context', ctx);

        var app = require('component/App');

        var Bootstrap = ctx.bootstrap(app);
        return React.renderToString(
          React.createFactory(Bootstrap)()
        );
      }
    );

  });
