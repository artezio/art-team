<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta charset="utf-8"/>

  <title>ART-TEAM</title>

  <link rel="stylesheet" type="text/css" href="/css/app.css"/>

  <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>

  <script data-main="/js/main" src="/js/libs/require/require-2.1.15.min.js"></script>
</head>
<body>

<div id="root">
  ${content}
</div>

</body>
</html>
