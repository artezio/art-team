package art.team.serverrenderer

import art.team.app.AppWideConf
import art.team.serverrenderer.model.{NowShowing, RenderSpec, UIRenderer}
import com.artezio.amd.{AMDFactory, FactoryConfig, InstanceConfig, MockSpec}

import scala.concurrent.Future

trait ServerRendererCmp extends TemplateHelperCmp {
  self: AppWideConf =>

  val serverRenderer: ServerRenderer = new ServerRenderer

  class ServerRenderer {

    def render(nowShowing: NowShowing.Value, usernameOpt: Option[String]): Future[String] =
      createRenderer(nowShowing, usernameOpt) map { renderer =>
        val content = renderer(RenderSpec(nowShowing.toString, usernameOpt.orNull))
        templateHelper.process("index.ftl", Map("content" -> content))
      }

    // private

    private val factory = AMDFactory(FactoryConfig(
      pathPrefix = "frontend.js",
      mappings = Map(
        "react" -> "libs/react/react.min",
        "c" -> "libs/react-createfactory/react-createfactory",
        "immutable" -> "libs/immutable/immutable.min",
        "morearty" -> "libs/morearty/morearty",
        "transit" -> "libs/transit/transit.min",
        "json" -> "libs/require-json/require-json",
        "i18next" -> "libs/i18next/i18next.min",
        "moment" -> "libs/moment/moment-with-locales.min",
        "pubsub" -> "libs/pubsub/pubsub"
      )
    ))

    private def createRenderer(nowShowing: NowShowing.Value, usernameOpt: Option[String]): Future[UIRenderer] = {
      val instance = factory(InstanceConfig(
        mocks = Map(
          "director" -> MockSpec(),
          "jquery" -> MockSpec(),
          "bowser" -> MockSpec(),
          "cookies" -> MockSpec(),

          "app/Router" -> MockSpec(),
          "session/SessionStorage" -> MockSpec(
            methods = Map(
              "getUsername" -> ((_, _) => usernameOpt.orNull)
            )
          )
        )
      ))

      instance.require("main") map (_.asInstanceOf[UIRenderer])
    }

  }
}
