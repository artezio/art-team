package art.team.serverrenderer

import java.io.StringWriter

import freemarker.template.{Configuration, Version}

import scala.collection.JavaConversions

trait TemplateHelperCmp {
  val templateHelper: TemplateHelper = new TemplateHelper

  class TemplateHelper {
    def process(templatePath: String, model: Map[String, String]): String = {
      val template = conf.getTemplate(templatePath)
      val output = new StringWriter
      template.process(JavaConversions.mapAsJavaMap(model), output)
      output.toString
    }

    // private

    private lazy val conf = {
      val conf = new Configuration(new Version("2.3.21"))
      conf.setClassForTemplateLoading(getClass, "")
      conf
    }
  }

}
