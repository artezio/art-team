package art.team.serverrenderer.model

object NowShowing extends Enumeration {
  val PermissionViolation = Value("permissionviolation")
  val NotFound =  Value("notfound")
  val Login =  Value("login")
  val Logout =  Value("logout")
  val Prefs =  Value("prefs")
  val CV =  Value("cv")
  val CVList =  Value("cvlist")
  val Pickup =  Value("pickup")
}
