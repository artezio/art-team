package art.team.serverrenderer.model

class UIRenderer(renderToString: java.util.function.Function[RenderSpec, String]) {
  def apply(spec: RenderSpec): String =
    renderToString(spec)
}
