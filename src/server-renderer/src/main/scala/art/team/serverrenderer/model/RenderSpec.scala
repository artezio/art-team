package art.team.serverrenderer.model

case class RenderSpec(nowShowing: String, username: String)
