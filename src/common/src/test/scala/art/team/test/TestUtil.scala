package art.team.test

object TestUtil {
  def genUniqueString(): String =
    java.util.UUID.randomUUID().toString.replace("-", "")
}
