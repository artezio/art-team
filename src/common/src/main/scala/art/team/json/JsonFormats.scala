package art.team.json

import art.team.dto.{CVExcerpt, SearchMode}
import art.team.model._
import org.joda.time.DateTime
import spray.json._

object JsonFormats extends DefaultJsonProtocol {
  implicit val dateTimeformat = new JsonFormat[DateTime] {
    override def write(dt: DateTime): JsValue = JsNumber(dt.getMillis)
    override def read(json: JsValue): DateTime = new DateTime(json.asInstanceOf[JsNumber].value.toLong)
  }

  implicit val roleFormat = enumFormat(Role)
  implicit val searchModeFormat = enumFormat(SearchMode)

  implicit val dateValueFormat = jsonFormat2(DateValue.apply)
  implicit val educationRecordFormat = jsonFormat3(EducationRecord.apply)
  implicit val nameValueFormat = jsonFormat2(NameValue.apply)
  implicit val expertiseFormat = jsonFormat2(ExpertiseRecord.apply)
  implicit val projectFormat = jsonFormat8(Project.apply)
  implicit val certificateRecordFormat = jsonFormat2(CertificateRecord.apply)
  implicit val languageFormat = enumFormat(Language)
  implicit val languageProficiencyFormat = enumFormat(LanguageProficiency)
  implicit val languageRecordFormat = jsonFormat2(LanguageRecord.apply)
  implicit val cvFormat = jsonFormat11(CV.apply)

  implicit val locationCodeFormat = new RootJsonFormat[LocationCode] {
    override def read(json: JsValue): LocationCode = LocationCode(json.asInstanceOf[JsString].value)
    override def write(code: LocationCode): JsValue = JsString(code.value)
  }
  implicit val locationFormat = jsonFormat2(Location.apply)
  implicit val ownerFormat = new RootJsonFormat[Owner] {
    override def read(json: JsValue): Owner = Owner(json.asInstanceOf[JsString].value)
    override def write(owner: Owner): JsValue = JsString(owner.value)
  }
  implicit val cvExcerptFormat = jsonFormat6(CVExcerpt.apply)

  implicit val sectionFormat = enumFormat(Section)
  implicit val prefsFormat = jsonFormat3(Prefs.apply)

  // private

  private def enumFormat[T <: Enumeration](enum: T): JsonFormat[T#Value] =
    new JsonFormat[T#Value] {
      override def read(json: JsValue): T#Value = json match {
        case s: JsString =>
          enum.withName(s.value)
        case otherwise =>
          sys.error(s"Invalid enum format: ${otherwise.toString()}")
      }
      override def write(enum: T#Value): JsValue =
        JsString(enum.toString)
    }
}
