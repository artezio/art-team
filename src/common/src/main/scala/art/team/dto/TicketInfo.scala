package art.team.dto

case class TicketInfo(username: String, data: Option[String])
