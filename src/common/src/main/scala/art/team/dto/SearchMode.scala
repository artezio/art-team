package art.team.dto

object SearchMode extends Enumeration {
  val And = Value("and")
  val Or = Value("or")
}
