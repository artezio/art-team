package art.team.dto

import art.team.model.{Language, LocationCode, Owner}

case class CVExcerpt(language: Language.Value,
                     owner: Owner,
                     location: LocationCode,
                     firstname: String,
                     lastname: String,
                     position: String)
