package art.team.dto

case class CVGroupExport(override val bytes: Array[Byte],
                         override val filename: String) extends Export
