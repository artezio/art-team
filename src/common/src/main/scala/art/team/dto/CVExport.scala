package art.team.dto

import org.joda.time.DateTime

case class CVExport(override val bytes: Array[Byte],
                    override val filename: String,
                    lastModified: DateTime) extends Export
