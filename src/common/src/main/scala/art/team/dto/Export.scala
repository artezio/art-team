package art.team.dto

trait Export {
  def bytes: Array[Byte]
  def filename: String
}
