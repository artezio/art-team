package art.team.common.util

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.control.Exception._

object FutureUtil {
  def extract[T](future: Future[T])
       (implicit executionContext: ExecutionContext): T =
    Await.result(future, 10.seconds)
  def extractAllCatch[T](future: Future[T])
               (implicit executionContext: ExecutionContext): Option[T] = allCatch opt {
    Await.result(future, 10.seconds)
  }

  def mapFutOpt[From, To](fOpt: Future[Option[From]])
                         (f: From => To)
                (implicit executionContext: ExecutionContext): Future[Option[To]] =
    fOpt map (_ map f)
  def flatMapFutOpt[From, To](fOpt: Future[Option[From]])
                             (f: From => Option[To])
                    (implicit executionContext: ExecutionContext): Future[Option[To]] =
    fOpt map (_ flatMap f)
}
