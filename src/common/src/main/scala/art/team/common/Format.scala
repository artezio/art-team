package art.team.common

object Format extends Enumeration {
  val Docx, PDF = Value
}
