package art.team.app

import scala.concurrent.Future
import scalaz.Monad

trait AppWideImplicits {
  self: AppWideConf =>

  implicit def futureMonad: Monad[Future]
}
