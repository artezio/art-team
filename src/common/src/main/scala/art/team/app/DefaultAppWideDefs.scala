package art.team.app

trait DefaultAppWideDefs
  extends AppWideDefs
  with DefaultAppWideConf
  with DefaultAppWideImplicits
