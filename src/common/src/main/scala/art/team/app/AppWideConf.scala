package art.team.app

import akka.actor.ActorSystem
import com.typesafe.config.Config
import org.slf4j.Logger

import scala.concurrent.ExecutionContext

trait AppWideConf {
  implicit def asyncContext: ExecutionContext
  def blockingContext: ExecutionContext

  implicit def actorSystem: ActorSystem

  def appConfig: Config
  def appLogger: Logger
}
