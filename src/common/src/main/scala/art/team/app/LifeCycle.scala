package art.team.app

trait Lifecycle {
  def start(): Unit
  def stop(): Unit
}
