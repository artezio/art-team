package art.team.app

object Conf {

  object Network {
    val Port = "network.port"
  }

  object Mongo {
    val Host = "mongo.host"
    val DB = "mongo.db"
  }

  object Index {
    val Dir = "index.dir"
  }

  object Email {
    object Smtp {
      val Host = "email.smtp.host"
      val Port = "email.smtp.port"
      val Auth = "email.smtp.auth"
      val Username = "email.smtp.username"
      val Password = "email.smtp.password"
      val StartTls = "email.smtp.startTls"
    }
    val Originator = "email.originator"
  }

  val Domain = "domain"

  object Auth {
    val Type = "auth.type"
    object Dummy {
      val Password = "auth.dummy.password"
    }
    object Ldap {
      val URL = "auth.ldap.url"
      val Group = "auth.ldap.group"
    }
    object Session {
      val ExpirationSeconds = "auth.session.expirationSeconds"
    }
    object Ticket {
      val ExpirationSeconds = "auth.ticket.expirationSeconds"
    }
  }

}
