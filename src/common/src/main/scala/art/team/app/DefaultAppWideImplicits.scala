package art.team.app

import scala.concurrent.Future
import scalaz.Monad

trait DefaultAppWideImplicits extends AppWideImplicits {
  self: AppWideConf =>

  implicit override val futureMonad = new Monad[Future] {
    override def point[A](a: => A): Future[A] = Future.successful(a)
    override def bind[A, B](fa: Future[A])(f: A => Future[B]): Future[B] = fa.flatMap(f)(asyncContext)
  }
}
