package art.team.app

trait LifecycleImpl extends Lifecycle {
  def start(): Unit = {}
  def stop(): Unit = {}
}
