package art.team.app

import java.util.concurrent.{Executors, LinkedBlockingQueue, ThreadPoolExecutor, TimeUnit}

import akka.actor.ActorSystem
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.{LoggerFactory, Logger}

import scala.concurrent.ExecutionContext

trait DefaultAppWideConf extends AppWideConf {
  implicit override val asyncContext: ExecutionContext = {
    val executor = Executors.newFixedThreadPool(Runtime.getRuntime.availableProcessors() + 1)
    ExecutionContext.fromExecutorService(executor)
  }

  override val blockingContext: ExecutionContext = {
    val executor = new ThreadPoolExecutor(100, 100, 1, TimeUnit.MINUTES, new LinkedBlockingQueue(1000))
    executor.allowCoreThreadTimeOut(true)
    ExecutionContext.fromExecutorService(executor)
  }

  implicit override val actorSystem: ActorSystem = ActorSystem("default")

  override val appConfig: Config = ConfigFactory.load()
  override val appLogger: Logger = LoggerFactory.getLogger("appLogger")
}
