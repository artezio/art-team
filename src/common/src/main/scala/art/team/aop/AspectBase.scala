package art.team.aop

import java.lang.reflect.{InvocationHandler, Method}

abstract class AspectBase(proxee: AnyRef) extends InvocationHandler {

  protected def invokeProxee(method: Method, args: Array[Object]): AnyRef =
    method.invoke(proxee, args: _*)

}
