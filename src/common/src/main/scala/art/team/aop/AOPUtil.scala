package art.team.aop

import java.lang.reflect.{InvocationHandler, Proxy}

object AOPUtil {

  def createProxy[I](proxee: I, interfaceClass: Class[I], handler: InvocationHandler): I = {
    assert(interfaceClass.isInterface, "interfaceClass should be an interface class")
    Proxy.newProxyInstance(interfaceClass.getClassLoader, Array(interfaceClass), handler).asInstanceOf[I]
  }

}
