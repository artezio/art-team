package art.team.model

case class CertificateRecord(description: String, year: Int)

object CertificateRecord {
  implicit val ordering =
    Ordering[Int] on ((cr: CertificateRecord) => cr.year)
}
