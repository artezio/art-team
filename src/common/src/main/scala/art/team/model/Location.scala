package art.team.model

case class Location(code: LocationCode, descriptions: Map[Language.Value, String])
