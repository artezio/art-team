package art.team.model

object Language extends Enumeration {
  val Belarussian = Value("by")
  val Czech = Value("cs")
  val German = Value("de")
  val Greek = Value("el")
  val English = Value("en")
  val Spanish = Value("es")
  val Finnish = Value("fi")
  val French = Value("fr")
  val Hebrew = Value("he")
  val Italian = Value("it")
  val Japan = Value("ja")
  val Lithuanian = Value("lt")
  val Dutch = Value("nl")
  val Norwegian = Value("no")
  val Polish = Value("pl")
  val Portuguese = Value("pt")
  val Russian = Value("ru")
  val Swedish = Value("sv")
  val Ukrainian = Value("ua")
  val Chinese = Value("zh")
}
