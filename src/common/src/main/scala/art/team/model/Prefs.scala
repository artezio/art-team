package art.team.model

case class Prefs(language: Language.Value,
                 selectedLocations: Map[LocationCode, Boolean],
                 defaultSection: Section.Value)

object Section extends Enumeration {
  val CV = Value("cv")
  val List = Value("list")
  val PickUp = Value("pickup")
}
