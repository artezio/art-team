package art.team.model

trait Tagged {
  def tags: Vector[String]
}
