package art.team.model

case class EducationRecord(startYear: Int,
                           endYear: Int,
                           institution: String)

object EducationRecord {
  implicit val ordering =
    Ordering[(Int, Int)] on ((er: EducationRecord) => (er.startYear, er.endYear))
}
