package art.team.model

case class Ticket(username: String, data: Option[String], ticketIdHash: String, used: Boolean = false)
