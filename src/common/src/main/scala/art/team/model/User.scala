package art.team.model

case class User(username: String, passwordHash: String, role: Role.Value, passwordHashExpired: Boolean)

object Role extends Enumeration {
  val User = Value("user")
  val Admin = Value("admin")
  val HR = Value("hr")
  val Manager = Value("manager")
}
