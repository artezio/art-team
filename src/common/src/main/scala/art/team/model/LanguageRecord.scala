package art.team.model

case class LanguageRecord(language: Language.Value, proficiency: LanguageProficiency.Value)

object LanguageProficiency extends Enumeration {
  val Elementary = Value("elementary")
  val Beginner = Value("beginner")
  val Intermediate = Value("intermediate")
  val Advanced = Value("advanced")
  val Fluent = Value("fluent")
  val Native = Value("native")
}
