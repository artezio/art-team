package art.team.model

case class DateValue(year: Int, month: Int)

object DateValue {
  implicit val ordering =
    Ordering[(Int, Int)] on ((dv: DateValue) => (dv.year, dv.month))
}
