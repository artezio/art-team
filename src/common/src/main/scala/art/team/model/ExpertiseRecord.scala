package art.team.model

import scalaz.Lens

case class ExpertiseRecord(area: String,
                           override val tags: Vector[String]) extends Tagged

object ExpertiseRecord {
  val tagsLens: Lens[ExpertiseRecord, Vector[String]] =
    Lens.lensu((self, tags) => self.copy(tags = tags), _.tags)
}
