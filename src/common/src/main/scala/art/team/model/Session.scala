package art.team.model

case class Session(username: String, sessionIdHash: String)
