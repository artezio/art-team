package art.team.model

import scalaz.Lens

case class CV(name: NameValue,
              location: String,
              position: String,
              description: String,
              summary: Vector[String],
              expertise: Vector[ExpertiseRecord],
              education: Vector[EducationRecord],
              certificates: Vector[CertificateRecord],
              languages: Vector[LanguageRecord],
              additionalInfo: Vector[String],
              projects: Vector[Project])

object CV {
  val expertiseLens: Lens[CV, Vector[ExpertiseRecord]] =
    Lens.lensu((self, expertise) => self.copy(expertise = expertise), _.expertise)

  val educationLens: Lens[CV, Vector[EducationRecord]] =
    Lens.lensu((self, education) => self.copy(education = education), _.education)

  val certificatesLens: Lens[CV, Vector[CertificateRecord]] =
    Lens.lensu((self, certs) => self.copy(certificates = certs), _.certificates)

  val projectsLens: Lens[CV, Vector[Project]] =
    Lens.lensu((self, projects) => self.copy(projects = projects), _.projects)
}
