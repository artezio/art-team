package art.team.model

import scalaz.Lens

case class Project(startDate: DateValue,
                   endDate: DateValue,
                   name: String,
                   company: String,
                   description: String,
                   position: String,
                   responsibilities: String,
                   override val tags: Vector[String]) extends Tagged

object Project {
  val tagsLens: Lens[Project, Vector[String]] =
    Lens.lensu((self, tags) => self.copy(tags = tags), _.tags)

  implicit val ordering =
    Ordering[(DateValue, DateValue)] on ((p: Project) => (p.startDate, p.endDate))
}
