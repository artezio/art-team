package art.team.metrics

import com.codahale.metrics.{JmxReporter, MetricRegistry}

object Metrics {

  val Registry = new MetricRegistry

  JmxReporter.forRegistry(Registry).build().start()

}
