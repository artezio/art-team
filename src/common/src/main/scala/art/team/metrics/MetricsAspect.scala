package art.team.metrics

import java.lang.reflect.Method

import art.team.aop.AspectBase
import com.codahale.metrics.MetricRegistry.name

import scala.concurrent.{ExecutionContext, Future}

class MetricsAspect(proxee: AnyRef)
                   (implicit protected val executionContext: ExecutionContext) extends AspectBase(proxee) {

  override def invoke(proxy: Object, method: Method, args: Array[Object]): AnyRef = {
    val path = s"${method.getDeclaringClass.getSimpleName}.${method.getName}"
    val timer = Metrics.Registry.timer(name(path))
    val context = timer.time()

    var async = false
    try {
      val result = invokeProxee(method, args)
      async = result match {
        case f: Future[_] =>
          f.onComplete(_ => context.stop())
          true
        case _ =>
          false
      }
      result
    } finally {
      if (!async)
        context.stop()
    }
  }
}
