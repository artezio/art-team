package art.team.util

object ConfUtil {

  def getPath(path: String): String =
    if (path.startsWith("~/") || path.startsWith("~\\"))
      System.getProperty("user.home") + path.substring(1)
    else
      path

}
