package art.team.db

import reactivemongo.api.DB

trait TestMongoDBCmp extends MongoDB {
  override val db: DB = MongoUtil.connect(db = "test")
}
