package art.team.db

import art.team.db.Records._
import art.team.model._
import org.joda.time.DateTime
import reactivemongo.bson._

object Handlers {

  // common

  implicit val jodaHandler = new BSONHandler[BSONDateTime, DateTime] {
    override def read(bson: BSONDateTime): DateTime = new DateTime(bson.value)
    override def write(t: DateTime): BSONDateTime = BSONDateTime(t.getMillis)
  }

  implicit val languageHandler = enumHandler(Language)
  implicit val languageProficiencyHandler = enumHandler(LanguageProficiency)

  implicit val ownerHandler = new BSONHandler[BSONString, Owner] {
    override def read(bson: BSONString): Owner = Owner(bson.value)
    override def write(owner: Owner): BSONString = BSONString(owner.value)
  }

  implicit val locationCodeHandler = new BSONHandler[BSONString, LocationCode] {
    override def read(bson: BSONString): LocationCode = LocationCode(bson.value)
    override def write(code: LocationCode): BSONString = BSONString(code.value)
  }

  // session

  implicit val sessionHandler = Macros.handler[Session]
  implicit val sessionRecordHandler = Macros.handler[SessionRecord]

  // ticket

  implicit val ticketHandler = Macros.handler[Ticket]
  implicit val ticketRecordHandler = Macros.handler[TicketRecord]

  // user

  implicit val roleHandler = new BSONHandler[BSONString, Role.Value] {
    override def read(bson: BSONString): Role.Value = Role.withName(bson.value)
    override def write(t: Role.Value): BSONString = BSONString(t.toString)
  }
  implicit val userHandler = Macros.handler[User]
  implicit val userRecordHandler = Macros.handler[UserRecord]

  // CV

  implicit val dateValueHandler = Macros.handler[DateValue]
  implicit val educationRecordHandler = Macros.handler[EducationRecord]
  implicit val nameValueHandler = Macros.handler[NameValue]
  implicit val expertiseHandler = Macros.handler[ExpertiseRecord]
  implicit val certificateRecordHandler = Macros.handler[CertificateRecord]
  implicit val languageRecordHandler = Macros.handler[LanguageRecord]
  implicit val projectHandler = Macros.handler[Project]
  implicit val cvHandler = Macros.handler[CV]
  implicit val cvRecordHandler = Macros.handler[CVRecord]

  // location
  implicit val locationDescriptionHandler = mapToStringHandler[Language.Value](Language.withName, _.toString)
  implicit val locationHandler = Macros.handler[Location]
  implicit val locationRecordHandler = Macros.handler[LocationRecord]

  // prefs
  implicit val prefsSelectedLocationsHandler = mapToBooleanHandler[LocationCode](LocationCode, _.value)
  implicit val sectionHandler = enumHandler(Section)
  implicit val prefsHandler = Macros.handler[Prefs]
  implicit val prefsRecordHandler = Macros.handler[PrefsRecord]

  // private

  private def enumHandler[T <: Enumeration](enum: T): BSONHandler[BSONString, T#Value] =
    new BSONHandler[BSONString, T#Value] {
      override def read(bson: BSONString): T#Value = enum.withName(bson.value)
      override def write(t: T#Value): BSONString = BSONString(t.toString)
    }

  private type Doc = BSONDocument

  private def mapToStringHandler[K](keyReader: String => K, keyWriter: K => String)
                          (implicit vHandler: BSONHandler[BSONString, String]): BSONHandler[Doc, Map[K, String]] =
    new BSONHandler[Doc, Map[K, String]] {
      override def read(bson: Doc): Map[K, String] = {
        val elements = bson.elements.map { tuple =>
          keyReader(tuple._1) -> vHandler.read(tuple._2.seeAsTry[BSONString].get)
        }
        elements.toMap
      }

      override def write(map: Map[K, String]): Doc = {
        val elements = map.toStream.map { tuple =>
          keyWriter(tuple._1) -> vHandler.write(tuple._2)
        }
        BSONDocument(elements)
      }
    }

  private def mapToBooleanHandler[K](keyReader: String => K, keyWriter: K => String)
                           (implicit vHandler: BSONHandler[BSONBoolean, Boolean]): BSONHandler[Doc, Map[K, Boolean]] =
    new BSONHandler[Doc, Map[K, Boolean]] {
      override def read(bson: Doc): Map[K, Boolean] = {
        val elements = bson.elements.map { tuple =>
          keyReader(tuple._1) -> vHandler.read(tuple._2.seeAsTry[BSONBoolean].get)
        }
        elements.toMap
      }

      override def write(map: Map[K, Boolean]): Doc = {
        val elements = map.toStream.map { tuple =>
          keyWriter(tuple._1) -> vHandler.write(tuple._2)
        }
        BSONDocument(elements)
      }
    }

}
