package art.team.db

import art.team.app.{AppWideConf, Conf}
import reactivemongo.api.DB

trait DefaultMongoDBCmp extends MongoDB {
  self: AppWideConf =>

  override val db: DB = MongoUtil.connect(
    db = appConfig.getString(Conf.Mongo.DB),
    actorSystem = Some(self.actorSystem),
    host = appConfig.getString(Conf.Mongo.Host)
  )
}
