package art.team.db

import art.team.model._
import org.joda.time.DateTime
import reactivemongo.bson.BSONObjectID

object Records {

  trait HasId {
    def _id: BSONObjectID
  }
  object HasId {
    object fields {
      val Id = "_id"
    }
  }

  trait HasLastAccessed {
    def lastAccessed: DateTime
  }
  object HasLastAccessed {
    object fields {
      val LastAccessed = "lastAccessed"
    }
  }

  trait HasVersion {
    def version: Int
  }
  object HasVersion {
    object fields {
      val Version = "version"
    }
  }

  trait HasOwner {
    def owner: Owner
  }
  object HasOwner {
    object fields {
      val Owner = "owner"
    }
  }

  trait HasLanguage {
    def language: Language.Value
  }
  object HasLanguage {
    object fields {
      val Language = "language"
    }
  }

  trait HasLastModified {
    def lastModified: DateTime
  }
  object HasLastModified {
    object fields {
      val LastModified = "lastModified"
    }
  }

  trait HasLastModifiedBy {
    def lastModifiedBy: String
  }
  object HasLastModifiedBy {
    object fields {
      val lastModifiedBy = "lastModifiedBy"
    }
  }

  case class SessionRecord(override val _id: BSONObjectID,
                           lastAccessed: DateTime,
                           session: Session)
    extends HasId
    with HasLastAccessed
  object SessionRecord {
    object fields {
      val Username = "session.username"
      val SessionIdHash = "session.sessionIdHash"
    }
  }

  case class TicketRecord(override val _id: BSONObjectID,
                          lastAccessed: DateTime,
                          ticket: Ticket)
    extends HasId
    with HasLastAccessed
  object TicketRecord {
    object fields {
      val Username = "ticket.username"
      val Data = "ticket.data"
      val TicketIdHash = "ticket.ticketIdHash"
      val Used = "ticket.used"
    }
  }

  case class CVRecord(override val _id: BSONObjectID,
                      override val version: Int,
                      override val owner: Owner,
                      override val language: Language.Value,
                      override val lastModified: DateTime,
                      override val lastModifiedBy: String,
                      cv: CV)
    extends HasId
    with HasVersion
    with HasOwner
    with HasLanguage
    with HasLastModified
    with HasLastModifiedBy
  object CVRecord {
    object fields {
      val CV = "cv"
      val Firstname = s"$CV.name.firstname"
      val Lastname = s"$CV.name.lastname"
      val Location = s"$CV.location"
      val Position = s"$CV.position"
      val Description = s"$CV.description"
      val Expertise = s"$CV.expertise"
      val ExpertiseTags = s"$Expertise.tags"
      val Projects = s"$CV.projects"
      val ProjectName = s"$Projects.name"
      val ProjectTags = s"$Projects.tags"
    }
  }

  case class UserRecord(override val _id: BSONObjectID, user: User)
    extends HasId
  object UserRecord {
    object fields {
      val User = "user"
      val Username = s"$User.username"
      val PasswordHash = s"$User.passwordHash"
      val Role = s"$User.role"
      val PasswordHashExpired = s"$User.passwordHashExpired"
    }
  }

  case class LocationRecord(override val _id: BSONObjectID, location: Location)
    extends HasId
  object LocationRecord {
    object fields {
      val Location = "location"
      val Code = s"$Location.code"
    }
  }

  case class PrefsRecord(override val _id: BSONObjectID, override val owner: Owner, prefs: Prefs)
    extends HasId
    with HasOwner
  object PrefsRecord {
    object fields {
      val Prefs = "prefs"
    }
  }
}
