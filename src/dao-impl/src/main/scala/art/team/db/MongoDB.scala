package art.team.db

import reactivemongo.api.DB

trait MongoDB {
  def db: DB
}
