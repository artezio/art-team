package art.team.db

import akka.actor.ActorSystem
import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.{DB, MongoDriver}
import reactivemongo.bson._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object MongoUtil {
  object Indexes {
    val ExpireAfterSeconds = "expireAfterSeconds"
    val Unique = "unique"
  }

  def connect(db: String,
              host: String = "localhost",
              actorSystem: Option[ActorSystem] = None,
              timeoutSeconds: Int = 10): DB = {
    val driver = new MongoDriver(actorSystem)
    val connection = driver.connection(Seq(host))
    Await.ready(connection.waitForPrimary(timeoutSeconds.seconds), timeoutSeconds.seconds)
    connection(db)
  }

  def collection(db: DB, name: String): BSONCollection =
    db.collection(name)

  def genId(): BSONObjectID = BSONObjectID.generate

  def mkDoc(fields: (String, BSONValue)*) =
    BSONDocument(fields map (field => Producer.nameValue2Producer(field)): _*)

  def mkSet(fields: BSONDocument): BSONDocument =
    BSONDocument("$set" -> fields)

  def mkSort(value: Int, fields: String*): BSONDocument =
    mkDoc(fields map (field => (field, BSONInteger(value))): _*)

  def createProjection(includedFields: String*): BSONDocument =
    mkDoc(includedFields map (field => field -> BSONInteger(1)): _*)

  def readPathAs[T](doc: BSONDocument, path: String)
          (implicit reader: BSONReader[_ <: BSONValue, T]): T = {
    val pathElements = path.split('.').toVector
    if (pathElements.isEmpty) doc.getAs[T](path).get
    else {
      val elems = pathElements.init
      val target = pathElements.last

      val doc2 = elems.foldLeft(doc) {
        case (b, name) => b.getAs[BSONDocument](name).get
      }
      doc2.getAs[T](target).get
    }
  }

  def readPathAsOpt[T](doc: BSONDocument, path: String)
                      (implicit reader: BSONReader[_ <: BSONValue, T]): Option[T] = {
    val pathElements = path.split('.').toVector
    if (pathElements.isEmpty) doc.getAs[T](path)
    else {
      val elems = pathElements.init
      val target = pathElements.last

      val doc2 = elems.foldLeft(doc) {
        case (b, name) => b.getAs[BSONDocument](name).get
      }
      doc2.getAs[T](target)
    }
  }

  def ensureCompoundIndex(coll: BSONCollection,
                          key: Seq[String],
                          indexType: IndexType = IndexType.Ascending,
                          options: BSONDocument = BSONDocument.empty): Future[Boolean] =
    coll.indexesManager.ensure(
      Index(key map (k => (k, indexType)), options = options, background = true)
    )

  def ensureIndex(coll: BSONCollection,
                  key: String,
                  indexType: IndexType = IndexType.Ascending,
                  options: BSONDocument = BSONDocument.empty): Future[Boolean] =
    ensureCompoundIndex(coll, Seq(key), indexType, options)

}
