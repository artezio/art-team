package art.team.dao

import art.team.app.{AppWideConf, Lifecycle}
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records.LocationRecord
import art.team.model.Location
import reactivemongo.bson.{BSONBoolean, BSONDocument}

import scala.concurrent.Future

trait LocationDaoMongoCmp extends LocationDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val locationDao = new LocationDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureIndex(
      locationDao.coll, LocationRecord.fields.Code,
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class LocationDaoMongo extends LocationDao {
    override def fetchAvailableLocations(): Future[Seq[Location]] =
      coll.find(mkDoc()).cursor[BSONDocument].collect[Seq](Int.MaxValue, stopOnError = true) map { docs =>
        docs map { _.as[LocationRecord].location }
      }

    // private

    private[LocationDaoMongoCmp] lazy val coll = collection(db, "location")
  }
}
