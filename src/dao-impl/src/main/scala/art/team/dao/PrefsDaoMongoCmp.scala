package art.team.dao

import art.team.app.{AppWideConf, Lifecycle}
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Handlers._
import art.team.db.Records.{HasOwner, PrefsRecord}
import art.team.model.Prefs
import reactivemongo.bson.{BSONDocument, BSON, BSONBoolean, BSONString}
import reactivemongo.core.commands.{FindAndModify, Update}

import scala.concurrent.Future

trait PrefsDaoMongoCmp extends PrefsDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val prefsDao = new PrefsDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureIndex(
      prefsDao.coll,
      HasOwner.fields.Owner,
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class PrefsDaoMongo extends PrefsDao {
    override def setPrefs(owner: String, prefs: Prefs): Future[_] = {
      val query = mkQuery(owner)
      val update = mkDoc(PrefsRecord.fields.Prefs -> BSON.write(prefs))
      db.command(
        FindAndModify(
          coll.name,
          query,
          Update(mkSet(update), fetchNewObject = false),
          upsert = true
        )
      )
    }

    override def readPrefs(owner: String): Future[Option[Prefs]] = {
      val selector = mkQuery(owner)
      coll.find(selector).cursor[BSONDocument].headOption map { docOpt =>
        docOpt map { _.as[PrefsRecord].prefs }
      }
    }

    // private

    private[PrefsDaoMongoCmp] lazy val coll = collection(db, "prefs")

    private def mkQuery(owner: String) =
      mkDoc(HasOwner.fields.Owner -> BSONString(owner))
  }
}
