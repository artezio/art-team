package art.team.dao

import art.team.app.{AppWideConf, Lifecycle}
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records._
import art.team.model.{Owner, CV, Language}
import org.joda.time.DateTime
import reactivemongo.bson.{BSONBoolean, BSONDocument, BSONInteger, BSONString}

import scala.concurrent.Future

trait CVHistoryDaoMongoCmp extends CVHistoryDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val cvHistoryDao = new CVHistoryDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureCompoundIndex(
      cvHistoryDao.coll,
      Seq(HasOwner.fields.Owner, HasLanguage.fields.Language, HasVersion.fields.Version),
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class CVHistoryDaoMongo extends CVHistoryDao {

    override def create(cv: CV, owner: Owner, language: Language.Value, version: Int): Future[Boolean] = {
      val record = CVRecord(genId(), version, owner, language, DateTime.now(), owner.value, cv)
      coll.insert(record) map (_.ok)
    }

    override def read(owner: Owner, language: Language.Value, version: Int): Future[Option[CV]] = {
      val selector = mkDoc(
        HasOwner.fields.Owner -> BSONString(owner.value),
        HasLanguage.fields.Language -> BSONString(language.toString),
        HasVersion.fields.Version -> BSONInteger(version)
      )
      coll.find(selector).cursor[BSONDocument].headOption map { docOpt =>
        docOpt map { doc =>
          val record = doc.as[CVRecord]
          record.cv
        }
      }
    }

    // private

    private[CVHistoryDaoMongoCmp] lazy val coll = collection(db, "cv.history")

  }
}
