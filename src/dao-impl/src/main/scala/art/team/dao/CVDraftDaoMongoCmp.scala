package art.team.dao

import art.team.app.{AppWideConf, Lifecycle}
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records._
import art.team.model.{Owner, CV, Language}
import org.joda.time.DateTime
import reactivemongo.bson._
import reactivemongo.core.commands.{FindAndModify, Update}

import scala.concurrent.Future

trait CVDraftDaoMongoCmp extends CVDraftDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val cvDraftDao = new CVDraftDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureCompoundIndex(
      cvDraftDao.coll,
      Seq(HasOwner.fields.Owner, HasLanguage.fields.Language),
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class CVDraftDaoMongo extends CVDraftDao {

    override def setDraft(cv: CV, owner: Owner, language: Language.Value,
                          basedOnVersion: Int, lastModifiedBy: String): Future[_] = {
      val query = mkQuery(owner, language)
      val update = mkDoc(
        HasVersion.fields.Version -> BSONInteger(basedOnVersion),
        HasLastModified.fields.LastModified -> BSON.write(DateTime.now()),
        HasLastModifiedBy.fields.lastModifiedBy -> BSONString(lastModifiedBy),
        CVRecord.fields.CV -> BSON.write(cv)
      )
      db.command(
        FindAndModify(
          coll.name,
          query,
          Update(mkSet(update), fetchNewObject = false),
          upsert = true
        )
      )
    }

    override def readDraft(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]] = {
      val selector = mkQuery(owner, language)
      coll.find(selector).cursor[BSONDocument].headOption map { docOpt =>
        docOpt map { doc =>
          val record = doc.as[CVRecord]
          (record.cv, record.version, record.lastModified, record.lastModifiedBy)
        }
      }
    }

    override def clearDraft(owner: Owner, language: Language.Value): Future[Boolean] = {
      val query = mkQuery(owner, language)
      coll.remove(query) map (_.ok)
    }

    // private

    private[CVDraftDaoMongoCmp] lazy val coll = collection(db, "cv.draft")

    private def mkQuery(owner: Owner, language: Language.Value) =
      mkDoc(
        HasOwner.fields.Owner -> BSONString(owner.value),
        HasLanguage.fields.Language -> BSONString(language.toString)
      )

  }

}
