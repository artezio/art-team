package art.team.dao

import art.team.app.AppWideDefs
import art.team.db.DefaultMongoDBCmp

trait DaoModuleMongoRegistry
  extends DaoModuleRegistry
  with DefaultMongoDBCmp
  with SessionDaoMongoCmp
  with TicketDaoMongoCmp
  with UserDaoMongoCmp
  with CVDaoMongoCmp
  with CVDraftDaoMongoCmp
  with CVHistoryDaoMongoCmp
  with LocationDaoMongoCmp
  with PrefsDaoMongoCmp {

  self: AppWideDefs =>
}
