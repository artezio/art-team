package art.team.dao

import art.team.app.{AppWideConf, Lifecycle}
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records._
import art.team.dto.CVExcerpt
import art.team.model.{LocationCode, Owner, CV, Language}
import org.joda.time.DateTime
import play.api.libs.iteratee.Enumerator
import reactivemongo.bson._
import reactivemongo.core.commands._

import scala.concurrent.Future

trait CVDaoMongoCmp extends CVDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val cvDao = new CVDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureCompoundIndex(
      cvDao.coll,
      Seq(HasOwner.fields.Owner, HasLanguage.fields.Language),
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
    ensureIndex(cvDao.coll, HasOwner.fields.Owner)
    ensureIndex(cvDao.coll, HasLanguage.fields.Language)
    ensureIndex(cvDao.coll, CVRecord.fields.Firstname)
    ensureIndex(cvDao.coll, CVRecord.fields.Lastname)
    ensureIndex(cvDao.coll, CVRecord.fields.Position)
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class CVDaoMongo extends CVDao {
    override def create(cv: CV, owner: Owner, language: Language.Value): Future[Option[Int]] = {
      val record = CVRecord(genId(), 1, owner, language, DateTime.now(), owner.value, cv)
      coll.insert(record) map { lastError =>
        if (lastError.ok) Some(record.version)
        else None
      }
    }

    override def read(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]] = {
      val selector = mkDoc(
        HasOwner.fields.Owner -> BSONString(owner.value),
        HasLanguage.fields.Language -> BSONString(language.toString)
      )
      coll.find(selector).cursor[BSONDocument].headOption map { docOpt =>
        docOpt map { doc =>
          val record = doc.as[CVRecord]
          (record.cv, record.version, record.lastModified, record.lastModifiedBy)
        }
      }
    }

    override def update(cv: CV, owner: Owner, language: Language.Value,
                        version: Int, lastModifiedBy: String): Future[Option[(CV, Int)]] = {
      val query = mkDoc(
        HasOwner.fields.Owner -> BSONString(owner.value),
        HasLanguage.fields.Language -> BSONString(language.toString),
        HasVersion.fields.Version -> BSONInteger(version)
      )
      val nextVersion = version + 1
      val updateDoc = mkDoc(
        HasVersion.fields.Version -> BSONInteger(nextVersion),
        HasLastModified.fields.LastModified -> BSON.write(DateTime.now()),
        HasLastModifiedBy.fields.lastModifiedBy -> BSONString(lastModifiedBy),
        CVRecord.fields.CV -> BSON.write(cv)
      )
      db.command(
        FindAndModify(
          coll.name,
          query,
          Update(mkSet(updateDoc), fetchNewObject = false)
        )
      ) map { docOpt =>
        docOpt map { doc =>
          (doc.as[CVRecord].cv, nextVersion)
        }
      }
    }

    override def fetchExcerpts(): Future[Map[LocationCode, Seq[CVExcerpt]]] = {
      val Excerpts = "excerpts"
      val agg = Aggregate(coll.name, Seq(
        Match(mkDoc(
          CVRecord.fields.Firstname -> mkDoc("$ne" -> BSONString("")),
          CVRecord.fields.Lastname -> mkDoc("$ne" -> BSONString(""))
        )),
        Project(
          HasId.fields.Id -> BSONInteger(0),
          HasOwner.fields.Owner -> BSONInteger(1),
          HasLanguage.fields.Language -> BSONInteger(1),
          CVRecord.fields.Firstname -> BSONInteger(1),
          CVRecord.fields.Lastname -> BSONInteger(1),
          CVRecord.fields.Position -> BSONInteger(1),
          CVRecord.fields.Location -> BSONInteger(1)
        ),
        GroupField(CVRecord.fields.Location)(
          Excerpts -> Push("$ROOT")
        )
      ))

      db.command(agg) map { stream =>
        (stream map { doc =>
          for {
            location <- doc.getAs[String](HasId.fields.Id)
            rawExcerpts <- doc.get(Excerpts)
          } yield {
            val excerpts = rawExcerpts.asInstanceOf[BSONArray].values map { rawExcerpt =>
              val rawExcerptDoc = rawExcerpt.asInstanceOf[BSONDocument]

              val language = readPathAs[Language.Value](rawExcerptDoc, HasLanguage.fields.Language)

              val owner = readPathAs[String](rawExcerptDoc, HasOwner.fields.Owner)
              val location = readPathAs[String](rawExcerptDoc, CVRecord.fields.Position)
              val firstname = readPathAs[String](rawExcerptDoc, CVRecord.fields.Firstname)
              val lastname = readPathAs[String](rawExcerptDoc, CVRecord.fields.Lastname)
              val position = readPathAs[String](rawExcerptDoc, CVRecord.fields.Position)

              CVExcerpt(language, Owner(owner), LocationCode(location), firstname, lastname, position)
            }
            (LocationCode(location), excerpts.toSeq)
          }
        }).flatten.toMap
      }
    }

    override def fetchAll(): Enumerator[(CV, Owner, Language.Value)] =
      coll.find(BSONDocument()).cursor[BSONDocument].enumerate(Int.MaxValue, stopOnError = true) map { doc =>
        val record = doc.as[CVRecord]
        (record.cv, record.owner, record.language)
      }

    // private

    private[CVDaoMongoCmp] lazy val coll = collection(db, "cv")

  }
}
