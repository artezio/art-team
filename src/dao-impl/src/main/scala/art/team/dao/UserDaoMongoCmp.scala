package art.team.dao

import art.team.app.{AppWideConf, Lifecycle}
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records.{HasId, UserRecord}
import art.team.model.{Role, User}
import reactivemongo.bson.{BSONBoolean, BSONDocument, BSONInteger, BSONString}

import scala.concurrent.Future

trait UserDaoMongoCmp extends UserDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val userDao = new UserDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureIndex(
      userDao.coll,
      UserRecord.fields.Username,
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class UserDaoMongo extends UserDao {

    override def create(username: String, passwordHash: String, role: Role.Value): Future[Boolean] = {
      val record = UserRecord(genId(), User(username, passwordHash, role, passwordHashExpired = false))
      coll.insert(record) map (_.ok)
    }

    override def read(username: String): Future[Option[User]] = {
      val selector = mkDoc(
        UserRecord.fields.Username -> BSONString(username)
      )
      coll.find(selector).cursor[BSONDocument].headOption map { _ map { _.as[UserRecord].user } }
    }

    override def expirePasswordHashes(): Future[Boolean] = {
      val selector = mkDoc(UserRecord.fields.PasswordHashExpired -> BSONBoolean(value = false))
      val update = mkSet(mkDoc(UserRecord.fields.PasswordHashExpired -> BSONBoolean(value = true)))
      coll.update(selector, update, multi = true) map { _.ok }
    }

    override def renewPasswordHash(username: String, passwordHash: String): Future[Boolean] = {
      val selector = mkDoc(
        UserRecord.fields.Username -> BSONString(username)
      )
      val update = mkSet(mkDoc(
        UserRecord.fields.PasswordHash -> BSONString(passwordHash),
        UserRecord.fields.PasswordHashExpired -> BSONBoolean(value = false)
      ))
      coll.update(selector, update) map { _.ok }
    }

    override def fetchRoles(): Future[Map[String, Role.Value]] = {
      coll.find(
        mkDoc(),
        mkDoc(
          HasId.fields.Id -> BSONInteger(0),
          UserRecord.fields.Username -> BSONInteger(1),
          UserRecord.fields.Role -> BSONInteger(1)
        )
      ).cursor[BSONDocument].collect[Seq](Int.MaxValue, stopOnError = true) map { docs =>
        (docs map { doc =>
          val username = readPathAs[String](doc, UserRecord.fields.Username)
          val role = Role.withName(readPathAs[String](doc, UserRecord.fields.Role))
          (username, role)
        }).toMap
      }
    }

    override def updateUser(username: String, role: Option[Role.Value]): Future[Boolean] =
      if (role.isDefined) {
        val selector = mkDoc(
          UserRecord.fields.Username -> BSONString(username)
        )
        val update = {
          var updateDoc = mkDoc()
          role foreach { r =>
            updateDoc = updateDoc ++ mkDoc(UserRecord.fields.Role -> BSONString(r.toString))
          }

          mkSet(updateDoc)
        }
        coll.update(selector, update) map { lastError =>
          lastError.ok && lastError.updated > 0
        }
      } else Future.successful(false)

    // private

    private[UserDaoMongoCmp] lazy val coll = collection(db, "user")

  }
}
