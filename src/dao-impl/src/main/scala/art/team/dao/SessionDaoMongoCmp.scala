package art.team.dao

import art.team.app.{Conf, AppWideConf, Lifecycle}
import art.team.common.util.FutureUtil
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records.{HasLastAccessed, SessionRecord}
import art.team.model.Session
import org.joda.time.DateTime
import reactivemongo.bson.{BSONBoolean, BSONDocument, BSONInteger, BSONString}
import reactivemongo.core.commands.{FindAndModify, Update}

import scala.concurrent.Future

trait SessionDaoMongoCmp extends SessionDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val sessionDao = new SessionDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureIndex(
      sessionDao.coll,
      HasLastAccessed.fields.LastAccessed,
      options =
        mkDoc(Indexes.ExpireAfterSeconds -> BSONInteger(appConfig.getInt(Conf.Auth.Session.ExpirationSeconds)))
    )
    ensureIndex(
      sessionDao.coll, SessionRecord.fields.SessionIdHash,
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class SessionDaoMongo extends SessionDao {

    override def create(username: String, sessionIdHash: String): Future[Boolean] = {
      val session = Session(username, sessionIdHash)
      val record = SessionRecord(genId(), DateTime.now(), session)
      coll.insert(record) map (_.ok)
    }

    override def delete(sessionIdHash: String): Future[Boolean] = {
      val query = mkBySessionIdHashCriteria(sessionIdHash)
      coll.remove(query) map { lastError =>
        lastError.ok && lastError.updated == 1
      }
    }

    override def getUsername(sessionIdHash: String): Future[Option[String]] = {
      val criteria = mkBySessionIdHashCriteria(sessionIdHash)
      val update = Update(
        mkSet(
          mkDoc(HasLastAccessed.fields.LastAccessed -> jodaHandler.write(DateTime.now()))
        ),
        fetchNewObject = false
      )
      val projection = createProjection(SessionRecord.fields.Username)

      val result = db.command(FindAndModify(coll.name, criteria, update, fields = Some(projection)))
      FutureUtil.mapFutOpt(result) { doc =>
        readPathAs[String](doc, SessionRecord.fields.Username)
      }
    }

    // private

    private[SessionDaoMongoCmp] lazy val coll = collection(db, "session")

    private def mkBySessionIdHashCriteria(sessionIdHash: String): BSONDocument =
      mkDoc(SessionRecord.fields.SessionIdHash -> BSONString(sessionIdHash))

  }
}
