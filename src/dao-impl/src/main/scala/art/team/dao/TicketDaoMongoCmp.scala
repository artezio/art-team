package art.team.dao

import art.team.app.{Conf, AppWideConf, Lifecycle}
import art.team.common.util.FutureUtil
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records.{HasLastAccessed, TicketRecord}
import art.team.dto.TicketInfo
import art.team.model.Ticket
import org.joda.time.DateTime
import reactivemongo.bson.{BSONBoolean, BSONInteger, BSONString}
import reactivemongo.core.commands.{FindAndModify, Update}

import scala.concurrent.Future

trait TicketDaoMongoCmp extends TicketDaoCmp with Lifecycle {
  self: AppWideConf with MongoDB =>

  override val ticketDao = new TicketDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureIndex(
      ticketDao.coll,
      HasLastAccessed.fields.LastAccessed,
      options =
        mkDoc(Indexes.ExpireAfterSeconds -> BSONInteger(appConfig.getInt(Conf.Auth.Ticket.ExpirationSeconds)))
    )
    ensureIndex(
      ticketDao.coll, TicketRecord.fields.TicketIdHash,
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
    ensureCompoundIndex(
      ticketDao.coll, Seq(TicketRecord.fields.TicketIdHash, TicketRecord.fields.Used)
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class TicketDaoMongo extends TicketDao {

    override def create(username: String, ticketIdHash: String, data: Option[String]): Future[Boolean] = {
      val ticket = Ticket(username, data, ticketIdHash)
      val record = TicketRecord(genId(), DateTime.now(), ticket)
      coll.insert(record) map (_.ok)
    }

    override def use(ticketIdHash: String): Future[Option[TicketInfo]] = {
      val criteria = mkDoc(
        TicketRecord.fields.TicketIdHash -> BSONString(ticketIdHash),
        TicketRecord.fields.Used -> BSONBoolean(value = false)
      )
      val update = Update(
        mkSet(
          mkDoc(TicketRecord.fields.Used -> BSONBoolean(value = true))
        ),
        fetchNewObject = false
      )
      val projection = createProjection(TicketRecord.fields.Username, TicketRecord.fields.Data)

      val result = db.command(FindAndModify(coll.name, criteria, update, fields = Some(projection)))
      FutureUtil.mapFutOpt(result) { doc =>
        TicketInfo(
          readPathAs[String](doc, TicketRecord.fields.Username),
          readPathAsOpt[String](doc, TicketRecord.fields.Data)
        )
      }
    }

    // private

    private[TicketDaoMongoCmp] lazy val coll = collection(db, "ticket")

  }
}
