import sbt._, Keys._

object common {
  val commonSettings = Seq(
    organizationName := "Artezio",
    version := "0.1-SNAPSHOT",

    scalaVersion := "2.11.4",
    scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-Xlint", "-encoding", "utf8"),

    javaOptions += "-Xmx2G",

    resolvers ++= Seq(
      "Typesafe repo" at "http://repo.typesafe.com/typesafe/releases/",
      "Spray repo" at "http://repo.spray.io/",
      "Sonatype snapshots repo" at "https://oss.sonatype.org/content/repositories/snapshots/",
      "softprops-maven" at "http://dl.bintray.com/content/softprops/maven"
    )
  )

  def DefProject(name: String,
                 path: Option[String] = None,
                 group: String = "art.team"): Project =
    Project(name, file(path getOrElse name)).
      settings(organization := group).
      settings(commonSettings: _*).
      configs(IntTest).
      settings(inConfig(IntTest)(Defaults.testTasks): _*).
      settings(
        testOptions in Test := Seq(Tests.Filter(unitFilter)),
        testOptions in IntTest := Seq(Tests.Filter(itFilter))
      )

  def itFilter(name: String): Boolean = name endsWith "ISpec"
  def unitFilter(name: String): Boolean = (name endsWith "Spec") && !itFilter(name)

  lazy val IntTest = config("it") extend Test
}
