import sbt._

object dependencies {
  // versions

  val typesafeConfigVersion = "1.2.1"
  val scalaARMVersion = "1.4"
  val scalaAsyncVersion = "0.9.2"

  val jodaTimeVersion = "2.5"
  val jodaConvertVersion = "1.7"

  val apachePOIVersion = "3.10.1"
  val commonsCodecVersion = "1.9"

  val scalazVersion = "7.1.0"

  val metricsVersion = "3.0.2"
  val courierVersion = "0.1.3"

  val luceneVersion = "4.10.1"

  val akkaVersion = "2.3.6"

  val sprayVersion = "1.3.1"
  val sprayJsonVersion = "1.2.6"

  val playIterateesVersion = "2.3.5"
  val reactiveMongoVersion = "0.10.5.0.akka23"

  val docx4jVersion = "3.2.1"
  val itextVersion = "5.5.3"

  val freemarkerVersion = "2.3.21"
  val amdScalaVersion = "0.1-SNAPSHOT"

  val slf4jVersion = "1.7.7"
  val log4jVersion = "2.1"

  val scalaTestVersion = "2.2.1"
  val scalaCheckVersion = "1.11.6"

  // libraries

  val typesafeConfig = "com.typesafe" % "config" % typesafeConfigVersion
  val scalaARM = "com.jsuereth" %% "scala-arm" % scalaARMVersion
  val scalaAsync = "org.scala-lang.modules" %% "scala-async" % scalaAsyncVersion

  val jodaTime = "joda-time" % "joda-time" % jodaTimeVersion
  val jodaConvert = "org.joda" % "joda-convert" % jodaConvertVersion

  val apachePOI = "org.apache.poi" % "poi-scratchpad" % apachePOIVersion exclude ("commons-codec", "commons-codec")
  val commonsCodec = "commons-codec" % "commons-codec" % commonsCodecVersion

  val scalazCore = "org.scalaz" %% "scalaz-core" % scalazVersion
  val scalazConcurrent = "org.scalaz" %% "scalaz-concurrent" % scalazVersion

  val metricsCore = "com.codahale.metrics" % "metrics-core" % metricsVersion
  val courier = "me.lessis" %% "courier" % courierVersion

  val lucene = "org.apache.lucene" % "lucene-core" % luceneVersion
  val luceneAnalyzers = "org.apache.lucene" % "lucene-analyzers-common" % luceneVersion
  val luceneQueryParsers = "org.apache.lucene" % "lucene-queryparser" % luceneVersion
  val luceneJoin = "org.apache.lucene" % "lucene-join" % luceneVersion

  val akka = "com.typesafe.akka" %% "akka-actor" % akkaVersion

  val sprayCan = "io.spray" %% "spray-can" % sprayVersion
  val sprayRouting = "io.spray" %% "spray-routing" % sprayVersion
  val sprayCaching = "io.spray" %% "spray-caching" % sprayVersion
  val sprayJson = "io.spray" %% "spray-json" % sprayJsonVersion

  val playIteratees = "com.typesafe.play" %% "play-iteratees" % playIterateesVersion
  val reactiveMongo = "org.reactivemongo" %% "reactivemongo" % reactiveMongoVersion exclude ("org.apache.logging.log4j", "log4j-core")

  val docx4j = "org.docx4j" % "docx4j" % docx4jVersion exclude ("org.slf4j", "slf4j-log4j12") exclude ("log4j", "log4j") exclude ("commons-codec", "commons-codec") exclude ("org.apache.xmlgraphics", "fop")
  val itext = "com.itextpdf" % "itextpdf" % itextVersion

  val freemarker = "org.freemarker" % "freemarker" % freemarkerVersion
  val amdScala = "com.artezio" % "amd-scala" % amdScalaVersion

  val slf4jApi = "org.slf4j" % "slf4j-api" % slf4jVersion
  val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jVersion
  val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jVersion

  val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion
  val scalaCheck = "org.scalacheck" %% "scalacheck" % scalaCheckVersion

  // projects

  val commonDependencies = Seq(
    typesafeConfig,
    scalaARM,
    scalaAsync,
    jodaTime, jodaConvert,
    scalazCore,
    sprayJson,
    akka,
    metricsCore,
    slf4jApi,
    log4jApi,
    scalaTest % "test", scalaCheck % "test"
  )

  val daoInterfaceDependencies = commonDependencies ++ Seq(
    playIteratees
  )
  val daoImplDependencies = daoInterfaceDependencies ++ Seq(
    reactiveMongo
  )

  val cvImporterDependencies = commonDependencies ++ Seq(
    apachePOI
  )

  val cvExporterInterfaceDependencies = commonDependencies ++ Seq(
  )
  val cvExporterImplDependencies = cvExporterInterfaceDependencies ++ Seq(
    docx4j, itext
  )

  val serviceInterfaceDependencies = commonDependencies ++ Seq(
  )
  val serviceImplDependencies = serviceInterfaceDependencies ++ Seq(
    commonsCodec,
    courier,
    lucene, luceneAnalyzers, luceneQueryParsers, luceneJoin
  )

  val serverRendererDependencies = commonDependencies ++ Seq(
    freemarker,
    scalazConcurrent
  )

  val controllerDependencies = serviceInterfaceDependencies ++ Seq(
    sprayCan, sprayRouting, sprayCaching,
    akka
  )

  val mongoMigrationDependencies = daoImplDependencies ++ Seq(
  )

  val appDependencies = Seq(
    log4jCore
  )
}
