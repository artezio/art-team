package art.team.cvexporter

import java.util.{Locale, ResourceBundle}

import art.team.model

object I18N {

  object Header {
    val Body = "header.text.body"
    val Email = "header.text.email"
  }

  object Section {
    val Position = "section.position"
    val Description = "section.description"
    val Summary = "section.summary"
    val Expertise = "section.expertise"
    val Education = "section.education"
    val Certificates = "section.certificates"
    val Languages = "section.languages"
    val AdditionalInfo = "section.additionalInfo"
    val Projects = "section.projects"
  }

  def Language(language: model.Language.Value): String = s"language.$language"
  def Proficiency(proficiency: model.LanguageProficiency.Value): String = s"proficiency.$proficiency"

  object Project {
    val StartDate = "project.startDate"
    val EndDate = "project.endDate"
    val Company = "project.company"
    val Description = "project.description"
    val Position = "project.position"
    val Tags = "project.tags"
    val Responsibilities = "project.responsibilities"
  }

  def forLang(language: model.Language.Value): String => String = Bundles(language).getString

  // private

  private lazy val Bundles = Map(
    model.Language.English -> ResourceBundle.getBundle("art.team.cvexporter.text", Locale.ENGLISH),
    model.Language.Russian -> ResourceBundle.getBundle("art.team.cvexporter.text", new Locale("ru"))
  )

}
