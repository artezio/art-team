package art.team.cvexporter.pdf

import com.itextpdf.text.{Anchor, Font}

private[pdf] object Util {

  def mkLink(text: String, url: String, font: Font): Anchor = {
    val link = new Anchor(text, font)
    link.setReference(url)
    link
  }

}
