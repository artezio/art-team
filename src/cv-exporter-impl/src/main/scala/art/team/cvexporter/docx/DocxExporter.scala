package art.team.cvexporter.docx

import java.io.ByteArrayOutputStream
import java.math.BigInteger

import art.team.cvexporter.{Exporter, I18N}
import art.team.model._
import org.docx4j.model.table.TblFactory
import org.docx4j.openpackaging.io3.Save
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.TcPrInner.TcBorders
import org.docx4j.wml._
import org.joda.time.DateTime

import scala.collection.JavaConversions._

class DocxExporter extends Exporter {

  override def exportToBytes(cv: CV, language: Language.Value, version: Int, lastModified: DateTime): Array[Byte] = {
    val doc = modelToDocx(language, cv)

    fill(cv, language, doc)

    docxToBytes(doc)
  }

  override val extension: String = "docx"

  // protected

  override protected type Doc = WordprocessingMLPackage

  override protected def addHeader(doc: Doc, language: Language.Value, t: String => String): Unit = {}

  override protected def addExpertise(doc: Doc, expertise: Seq[ExpertiseRecord], t: String => String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    mainDoc.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Expertise))
    expertise foreach { record =>
      val text = s"${record.area}: ${record.tags.mkString(", ")}"
      mainDoc.addStyledParagraphOfText(Styles.P, text)
    }
  }

  override protected def addDescription(doc: Doc, description: String, t: String => String): Unit = {
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Description))
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.P, description)
  }

  override protected def addSummary(doc: Doc, summary: Seq[String], t: String => String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    mainDoc.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Summary))
    summary foreach (mainDoc.addStyledParagraphOfText(Styles.P, _))
  }

  override protected def addCertificates(doc: Doc, certs: Seq[CertificateRecord], t: String => String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Certificates))
    certs foreach { record =>
      val text = s"${record.year} ${record.description}"
      mainDoc.addStyledParagraphOfText(Styles.P, text)
    }
  }

  override protected def addLanguages(doc: Doc, languages: Seq[LanguageRecord], t: String => String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Languages))
    languages foreach { record =>
      val text = s"${t(I18N.Language(record.language))} - ${t(I18N.Proficiency(record.proficiency))}"
      mainDoc.addStyledParagraphOfText(Styles.P, text)
    }
  }

  override protected def addProjects(doc: Doc, projects: Seq[Project], t: String => String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Projects))

    projects foreach { record =>
      mainDoc.addObject(projectHeaderTbl(doc, record, t))
      mainDoc.addObject(projectInfoTbl(doc, record, t))
      mainDoc.addParagraphOfText(EmptyParagraph)
    }
  }

  override protected def addTitle(doc: Doc, title: String): Unit = {
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H1, title)
  }

  override protected def addPosition(doc: Doc, position: String, t: String => String): Unit = {
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Position))
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.P, position)
  }

  override protected def addEducation(doc: Doc, education: Seq[EducationRecord], t: String => String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H2, t(I18N.Section.Education))
    education foreach { record =>
      val text = s"${record.startYear} - ${record.endYear} ${record.institution}"
      mainDoc.addStyledParagraphOfText(Styles.P, text)
    }
  }

  override protected def addAdditionalInfo(doc: Doc, additionalInfo: Seq[String], t: String => String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    doc.getMainDocumentPart.addStyledParagraphOfText(Styles.H2, t(I18N.Section.AdditionalInfo))
    additionalInfo foreach (mainDoc.addStyledParagraphOfText(Styles.P, _))
  }

  // private

  private val ByteBufferSize = math.pow(2, 16).toInt // 65536

  private val EmptyParagraph = ""

  private object Styles {
    lazy val H1 = "Heading1"
    lazy val H2 = "Heading2"
    lazy val H3 = "Heading3"
    lazy val P = "Normal"
    lazy val SmallRight = "SmallRight"
    lazy val NormalRight = "NormalRight"
  }

  private lazy val noBorders = {
    val border = new CTBorder()
    border.setColor("auto")
    border.setSz(new BigInteger("0"))
    border.setSpace(new BigInteger("0"))
    border.setVal(STBorder.NONE)
    border
  }

  private lazy val singleBorder = {
    val border = new CTBorder()
    border.setColor("auto")
    border.setSz(new BigInteger("4"))
    border.setSpace(new BigInteger("2"))
    border.setVal(STBorder.SINGLE)
    border
  }

  private lazy val tcBorderRight = {
    val tcBorder = new TcBorders
    tcBorder.setRight(singleBorder)
    tcBorder
  }

  private lazy val noTblBorders = {
    val tblBorder = new TblBorders
    tblBorder.setBottom(noBorders)
    tblBorder.setLeft(noBorders)
    tblBorder.setRight(noBorders)
    tblBorder.setTop(noBorders)
    tblBorder.setInsideH(noBorders)
    tblBorder.setInsideV(noBorders)
    tblBorder
  }

  private object HeaderTable {
    val HeaderCellW = 7204
    val DatesCellW = 3000
    val Grid = buildGrid(HeaderCellW, DatesCellW)
    val ProjectHeaderCellTcPr = buildTcPrBottomWithWidth(HeaderCellW)
    val ProjectDatesCellTcPr = buildTcPrBottomWithWidth(DatesCellW)
  }

  private object ProjectTable {
    val TableSectionNameCellW = 3000
    val TableSectionValueCellW = 7204
    val Grid = buildGrid(TableSectionNameCellW, TableSectionValueCellW)
    val SectionNameTcPr = buildTcPrWithBorderAndWidth(tcBorderRight, TableSectionNameCellW)
    val SectionValueTcPr = buildTcPrWithWidth(TableSectionValueCellW)
  }

  private def buildGrid(leftCellWidth: Int, rightCellWidth: Int) = {
    val tblGrid = new TblGrid
    val col1 = new TblGridCol
    col1.setW(BigInteger.valueOf(leftCellWidth))

    val col2 = new TblGridCol
    col2.setW(BigInteger.valueOf(rightCellWidth))

    tblGrid.getGridCol.addAll(Seq(col1, col2))
    tblGrid
  }

  private def buildTcPrWithBorderAndWidth(border: TcBorders, width: Int): TcPr = {
    val tcPr = buildTcPrWithWidth(width)
    tcPr.setTcBorders(border)
    tcPr
  }

  private def buildTcPrBottomWithWidth(width: Int): TcPr = {
    val tcPr = buildTcPrWithWidth(width)
    val vAlight = new CTVerticalJc
    vAlight.setVal(STVerticalJc.BOTTOM)
    tcPr.setVAlign(vAlight)
    tcPr
  }

  private def buildTcPrWithWidth(width: Int): TcPr = {
    val tcPr = buildTcPr
    val tw = new TblWidth
    tw.setW(BigInteger.valueOf(width))
    tcPr.setTcW(tw)
    tcPr
  }

  private def buildTcPr = new TcPr

  private def modelToDocx(language: Language.Value, cv: CV): Doc =
    WordprocessingMLPackage.load(
      getClass.getResourceAsStream(s"/art/team/cvexporter/templates/docx/template-$language.docx"))

  private def docxToBytes(doc: Doc): Array[Byte] = {
    val saver = new Save(doc)
    val baos = new ByteArrayOutputStream(ByteBufferSize)
    saver.save(baos)
    baos.toByteArray
  }

  private def getWritableDocWidth(doc: Doc) =
    doc.getDocumentModel.getSections.get(0).getPageDimensions.getWritableWidthTwips

  private def projectHeaderTbl(doc: Doc, record: Project, t: String => String) = {
    val mainDoc = doc.getMainDocumentPart
    val tbl = TblFactory.createTable(1, 2, HeaderTable.HeaderCellW)
    tbl.getTblPr.setTblBorders(noTblBorders)
    tbl.setTblGrid(HeaderTable.Grid)

    val row = tbl.getContent.get(0).asInstanceOf[Tr]
    val cells = row.getContent

    val projectNameCell = cells.get(0).asInstanceOf[Tc]
    projectNameCell.setTcPr(HeaderTable.ProjectHeaderCellTcPr)
    projectNameCell.getContent.clear()
    projectNameCell.getContent.add(mainDoc.createStyledParagraphOfText(Styles.H2, record.name))

    val datesRageCell = cells.get(1).asInstanceOf[Tc]
    datesRageCell.setTcPr(HeaderTable.ProjectDatesCellTcPr)
    datesRageCell.getContent.clear()
    val startDateStr = s"${t(I18N.Project.StartDate)}: ${formatDate(record.startDate)}"
    val endDateStr = s"${t(I18N.Project.EndDate)}: ${formatDate(record.endDate)}"
    datesRageCell.getContent.add(mainDoc.createStyledParagraphOfText(Styles.SmallRight, startDateStr))
    datesRageCell.getContent.add(mainDoc.createStyledParagraphOfText(Styles.SmallRight, endDateStr))

    tbl
  }

  private def projectInfoTbl(doc: Doc, record: Project, t: String => String) = {
    val tbl = TblFactory.createTable(5, 2, ProjectTable.TableSectionNameCellW)
    tbl.getTblPr.setTblBorders(noTblBorders)
    tbl.setTblGrid(ProjectTable.Grid)

    val trFunc = getTrByIndex(tbl) _

    fillProjectInfoRow(doc, trFunc(0), t(I18N.Project.Company), record.company)
    fillProjectInfoRow(doc, trFunc(1), t(I18N.Project.Description), record.description)
    fillProjectInfoRow(doc, trFunc(2), t(I18N.Project.Position), record.position)
    fillProjectInfoRow(doc, trFunc(3), t(I18N.Project.Tags), record.tags.mkString(", "))
    fillProjectInfoRow(doc, trFunc(4), t(I18N.Project.Responsibilities), record.responsibilities.split("\n").toSeq)

    tbl
  }

  private def fillProjectInfoRow(doc: Doc, row: Tr, sectionName: String, sectionValue: String) = {
    val cells = row.getContent

    val sectionNameCell = cells.get(0).asInstanceOf[Tc]
    fillSectionNameCell(doc, sectionNameCell, sectionName)

    val sectionValueCell = cells.get(1).asInstanceOf[Tc]
    fillSectionValueCell(doc, sectionValueCell, sectionValue)
  }

  private def fillProjectInfoRow(doc: Doc, row: Tr, sectionName: String, sectionValues: Seq[String]) = {
    val cells = row.getContent

    val sectionNameCell = cells.get(0).asInstanceOf[Tc]
    fillSectionNameCell(doc, sectionNameCell, sectionName)

    val sectionValueCell = cells.get(1).asInstanceOf[Tc]
    fillSectionValueCell(doc, sectionValueCell, sectionValues)
  }

  private def fillSectionNameCell(doc: Doc, cell: Tc, value: String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    setTcPrAndClear(cell, ProjectTable.SectionNameTcPr)
    cell.getContent.add(mainDoc.createStyledParagraphOfText(Styles.NormalRight, value))
  }

  private def fillSectionValueCell(doc: Doc, cell: Tc, value: String): Unit = {
    val mainDoc = doc.getMainDocumentPart
    setTcPrAndClear(cell, ProjectTable.SectionValueTcPr)
    cell.getContent.add(mainDoc.createStyledParagraphOfText(Styles.P, value))
  }

  private def fillSectionValueCell(doc: Doc, cell: Tc, values: Seq[String]): Unit = {
    val mainDoc = doc.getMainDocumentPart
    setTcPrAndClear(cell, ProjectTable.SectionValueTcPr)
    values foreach (v => cell.getContent.add(mainDoc.createStyledParagraphOfText(Styles.P, v)))
  }

  private def setTcPrAndClear(cell: Tc, tcPr: TcPr): Unit = {
    cell.setTcPr(tcPr)
    cell.getContent.clear()
  }

  private def getTrByIndex(tbl: Tbl)(index: Int) = tbl.getContent.get(index).asInstanceOf[Tr]
}
