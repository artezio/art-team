package art.team.cvexporter

import art.team.aop.AOPUtil._
import art.team.app.AppWideConf
import art.team.common.Format
import art.team.cvexporter.docx.DocxExporter
import art.team.cvexporter.pdf.PDFExporter
import art.team.metrics.MetricsAspect
import art.team.model.{CV, Language}
import org.joda.time.DateTime

import scala.concurrent.Future

trait CVExportSvcImplCmp extends CVExportSvcCmp {
  self: AppWideConf =>

  override val cvExportSvc: CVExportSvc = new CVExportSvcImpl

  class CVExportSvcImpl extends CVExportSvc {

    override def export(cv: CV,
                        language: Language.Value,
                        version: Int,
                        lastModified: DateTime,
                        format: Format.Value): Future[Option[(Array[Byte], String)]] =
      Future {
        try {
          val exporter = Exporters(format)
          Some((exporter.exportToBytes(cv, language, version, lastModified), exporter.extension))
        } catch {
          case e: Exception =>
            appLogger.error("Cannot export CV", e)
            None
        }
      }(blockingContext)

    // private

    private lazy val Exporters = Map(
      Format.Docx -> {
        val proxee = new DocxExporter
        createProxy(proxee, classOf[Exporter], new MetricsAspect(proxee))
      },
      Format.PDF -> {
        val proxee = new PDFExporter
        createProxy(proxee, classOf[Exporter], new MetricsAspect(proxee))
      }
    )

  }

}
