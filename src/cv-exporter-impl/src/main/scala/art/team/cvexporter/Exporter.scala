package art.team.cvexporter

import art.team.model._
import org.joda.time.DateTime

trait Exporter {
  def exportToBytes(cv: CV, language: Language.Value, version: Int, lastModified: DateTime): Array[Byte]
  def extension: String

  // protected

  protected type Doc

  protected def fill(cv: CV, language: Language.Value, doc: Doc): Doc = {
    val t = I18N.forLang(language)

    addHeader(doc, language, t)
    addTitle(doc, cv.name.firstname + " " + cv.name.lastname)
    if (cv.position.nonEmpty) addPosition(doc, cv.position, t)
    if (cv.description.nonEmpty) addDescription(doc, cv.description, t)
    if (cv.summary.nonEmpty) addSummary(doc, cv.summary, t)
    if (cv.expertise.nonEmpty) addExpertise(doc, cv.expertise, t)
    if (cv.education.nonEmpty) addEducation(doc, cv.education, t)
    if (cv.certificates.nonEmpty) addCertificates(doc, cv.certificates, t)
    if (cv.languages.nonEmpty) addLanguages(doc, cv.languages, t)
    if (cv.additionalInfo.nonEmpty) addAdditionalInfo(doc, cv.additionalInfo, t)
    if (cv.projects.nonEmpty) addProjects(doc, cv.projects, t)

    doc
  }

  protected def addHeader(doc: Doc, language: Language.Value, t: String => String): Unit
  protected def addTitle(doc: Doc, title: String): Unit
  protected def addPosition(doc: Doc, position: String, t: String => String): Unit
  protected def addDescription(doc: Doc, description: String, t: String => String): Unit
  protected def addSummary(doc: Doc, summary: Seq[String], t: String => String): Unit
  protected def addExpertise(doc: Doc, expertise: Seq[ExpertiseRecord], t: String => String): Unit
  protected def addEducation(doc: Doc, education: Seq[EducationRecord], t: String => String): Unit
  protected def addCertificates(doc: Doc, certs: Seq[CertificateRecord], t: String => String): Unit
  protected def addLanguages(doc: Doc, languages: Seq[LanguageRecord], t: String => String): Unit
  protected def addAdditionalInfo(doc: Doc, additionalInfo: Seq[String], t: String => String): Unit
  protected def addProjects(doc: Doc, projects: Seq[Project], t: String => String): Unit

  protected def formatDate(date: DateValue): String = date.year + "." + ("%02d" format date.month)

}
