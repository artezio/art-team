package art.team.cvexporter.pdf

import java.io.ByteArrayOutputStream

import art.team.cvexporter.{Exporter, I18N}
import art.team.model._
import com.itextpdf.text._
import com.itextpdf.text.pdf._
import com.itextpdf.text.pdf.draw.DottedLineSeparator
import org.joda.time.DateTime

class PDFExporter extends Exporter {

  override def exportToBytes(cv: CV, language: Language.Value, version: Int, lastModified: DateTime): Array[Byte] = {
    val doc = new Document(PageSize.LETTER)
    val baos = new ByteArrayOutputStream(ByteBufferSize)
    val writer = PdfWriter.getInstance(doc, baos)
    writer.setPageEvent(new PageEventListener)

    doc.open()

    setCommonInfo(doc, cv)
    fill(cv, language, doc)

    doc.close()

    baos.toByteArray
  }

  override val extension: String = "pdf"

  // protected

  override protected type Doc = Document

  override protected def addHeader(doc: Doc, language: Language.Value, t: String => String): Unit = {
    doc.add(createHeader(t))
  }

  override protected def addTitle(doc: Document, title: String): Unit = {
    doc.add(mkP(title, Fonts.Title))
  }

  override protected def addPosition(doc: Doc, position: String, t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Position)))
    doc.add(mkP(position))
  }

  override protected def addDescription(doc: Doc, description: String, t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Description)))
    doc.add(mkP(description))
  }

  override protected def addSummary(doc: Doc, summary: Seq[String], t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Summary)))
    summary foreach { line =>
      doc.add(mkP(line))
    }
  }

  override protected def addExpertise(doc: Doc, expertise: Seq[ExpertiseRecord], t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Expertise)))
    expertise foreach { record =>
      val text = record.area + ": " + record.tags.mkString(", ")
      doc.add(mkP(text))
    }
  }

  override protected def addEducation(doc: Doc, education: Seq[EducationRecord], t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Education)))
    education foreach { record =>
      val text = s"${record.startYear} - ${record.endYear} ${record.institution}"
      doc.add(mkP(text))
    }
  }

  override protected def addCertificates(doc: Doc, certs: Seq[CertificateRecord], t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Certificates)))
    certs foreach { record =>
      val text = s"${record.year} ${record.description}"
      doc.add(mkP(text))
    }
  }

  override protected def addLanguages(doc: Doc, languages: Seq[LanguageRecord], t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Languages)))
    languages foreach { record =>
      val text = s"${t(I18N.Language(record.language))} - ${t(I18N.Proficiency(record.proficiency))}"
      doc.add(mkP(text))
    }
  }

  override protected def addAdditionalInfo(doc: Doc, additionalInfo: Seq[String], t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.AdditionalInfo)))
    additionalInfo foreach { line =>
      doc.add(mkP(line))
    }
  }

  override protected def addProjects(doc: Doc, projects: Seq[Project], t: String => String): Unit = {
    doc.add(mkSectionHeader(t(I18N.Section.Projects)))
    projects foreach {
      mkProject(_, t) foreach doc.add
    }
  }

  // private

  private val ByteBufferSize = math.pow(2, 16).toInt // 65536

  private object Fonts {
    lazy val Base =
      FontFactory.getFont(
        "/art/team/cvexporter/resources/fonts/Roboto-Light.ttf",
        BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 1, Font.NORMAL, BaseColor.BLACK
      ).getBaseFont

    def get(size: Int, style: Int = Font.NORMAL, color: BaseColor = BaseColor.BLACK): Font =
      new Font(Base, size, style, color)

    lazy val Title = get(22, Font.BOLD)
    lazy val SectionTitle = get(16, Font.BOLD)
    lazy val SubSectionTitle = get(14, Font.BOLD)
    lazy val Default = get(11)
    lazy val PageNumber = Fonts.get(10, color = BaseColor.DARK_GRAY)
    lazy val Small = Fonts.get(8, color = BaseColor.DARK_GRAY)
  }

  private class PageEventListener extends PdfPageEventHelper {
    override def onEndPage(writer: PdfWriter, document: Document): Unit = {
      val x = writer.getPageSize.getWidth / 2
      ColumnText.showTextAligned(writer.getDirectContent,
        Element.ALIGN_CENTER, new Phrase("%d" format writer.getPageNumber, Fonts.PageNumber),
        x, 25, 0
      )
    }
  }

  private def setCommonInfo(doc: Doc, cv: CV): Unit = {
    val name = cv.name.firstname + " " + cv.name.lastname
    doc.addSubject(name)
    doc.addTitle(name)
    doc.addCreationDate()
    doc.addLanguage(language.toString)
    doc.addCreator("ArtTeam")
  }

  private def createHeader(t: String => String): Element = {
    val table = new PdfPTable(2)
    table.setWidthPercentage(100)

    table addCell {
      val logo = Image.getInstance(getClass.getResource("/art/team/cvexporter/resources/images/logo.png"))
      logo.scaleToFit(80, 80)

      val cell = new PdfPCell(logo, false)
      cell.setBorder(0)
      cell.setPaddingLeft(25)
      cell
    }

    table addCell {
      val cell = new PdfPCell()
      cell.setBorder(0)
      cell.addElement(getHeaderText(t))
      cell
    }

    table
  }

  private def getHeaderText(t: String => String): Element = {
    val p = new Paragraph(t(I18N.Header.Body), Fonts.Small)
    val email = t(I18N.Header.Email)
    val link = Util.mkLink(email, "mailto:" + email, Fonts.Small)
    p.add(" ")
    p.add(link)
    p.setAlignment(Element.ALIGN_RIGHT)
    p
  }

  private def mkSectionHeader(label: String): Element = {
    val section = new Paragraph(label, Fonts.SectionTitle)
    section.setLeading(0, 2.2f)
    section
  }

  private def mkSubSectionHeader(label: String): Element = {
    val section = new Paragraph(label, Fonts.SubSectionTitle)
    section.setLeading(0, 2)
    section
  }

  private def mkP(text: String, font: Font = Fonts.Default): Paragraph = {
    val p = new Paragraph(text, font)
    p.setLeading(0, 2)
    p
  }

  private def mkProject(project: Project, t: String => String): Seq[Element] = {

    def mkProjectHeader = {
      val table = new PdfPTable(Array(0.75f, 0.25f))
      table.setWidthPercentage(100)

      table addCell {
        val cell = new PdfPCell()
        cell.setBorder(0)
        cell.setPaddingLeft(0)
        cell.addElement(mkSubSectionHeader(project.name))
        cell
      }
      table addCell {
        val cell = new PdfPCell()
        cell.setBorder(0)

        val text =
          s"""${t(I18N.Project.StartDate)}: ${formatDate(project.startDate)}
             |${t(I18N.Project.EndDate)}: ${formatDate(project.endDate)}""".stripMargin
        val p = mkP(text, Fonts.Small)
        p.setAlignment(Element.ALIGN_RIGHT)

        cell.addElement(p)
        cell
      }

      table
    }

    def mkProjectInfo = {
      val table = new PdfPTable(Array(0.25f, 0.75f))
      table.setWidthPercentage(100)
      table.setSpacingBefore(4)
      table.setSpacingAfter(20)

      def mkLeft(text: String) = {
        val cell = new PdfPCell()
        cell.setBorder(Rectangle.RIGHT)
        cell.setBorderWidth(1)
        cell.setBorderColor(BaseColor.GRAY)
        cell.setPaddingRight(10)

        val p = mkP(text)
        p.setAlignment(Element.ALIGN_RIGHT)

        cell.addElement(p)
        cell
      }
      def mkRight(text: String) = {
        val cell = new PdfPCell()
        cell.setBorder(0)
        cell.setPaddingLeft(10)
        cell.addElement(mkP(text))
        cell
      }

      table addCell mkLeft(t(I18N.Project.Company))
      table addCell mkRight(project.company)

      table addCell mkLeft(t(I18N.Project.Description))
      table addCell mkRight(project.description)

      table addCell mkLeft(t(I18N.Project.Position))
      table addCell mkRight(project.position)

      table addCell mkLeft(t(I18N.Project.Tags))
      table addCell mkRight(project.tags.mkString(", "))

      table addCell mkLeft(t(I18N.Project.Responsibilities))
      table addCell mkRight(project.responsibilities)

      table
    }

    def mkSeparator = {
      val sep = new DottedLineSeparator
      sep.setLineColor(BaseColor.GRAY)
      sep
    }

    Seq(mkProjectHeader, mkProjectInfo, mkSeparator)
  }

}
