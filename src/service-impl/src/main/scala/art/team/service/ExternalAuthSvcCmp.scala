package art.team.service

import art.team.app.{AppWideConf, Conf}
import art.team.authenticator.{Authenticator, DummyAuthenticator, LDAPAuthenticator}

import scala.concurrent.Future

trait ExternalAuthSvcCmp {
  self: AppWideConf =>

  val externalAuthSvc: ExternalAuthSvc = new ExternalAuthSvc()

  class ExternalAuthSvc {
    def isValid(username: String, password: String): Future[Boolean] = {
      val authType = appConfig.getString(Conf.Auth.Type)
      authenticators.get(authType).fold {
        appLogger.error(s"Invalid auth type: $authType")
        Future.successful(false)
      } { authenticator =>
        authenticator.isValid(username, password, self)
      }
    }

    // private

    private val authenticators: Map[String, Authenticator] = Map(
      "dummy" -> DummyAuthenticator,
      "ldap" -> LDAPAuthenticator
    )
  }
}
