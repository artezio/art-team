package art.team.service

import java.io.ByteArrayOutputStream
import java.nio.file.{Paths, Files}
import java.util.zip.{ZipEntry, ZipOutputStream}

import art.team.aop.AOPUtil._
import art.team.app.{AppWideDefs, Conf, Lifecycle}
import art.team.common.Format
import art.team.cvexporter.CVExportSvcCmp
import art.team.dao.{CVDaoCmp, CVDraftDaoCmp, CVHistoryDaoCmp, LocationDaoCmp}
import art.team.dto._
import art.team.metrics.MetricsAspect
import art.team.model._
import art.team.util.ConfUtil
import org.joda.time.DateTime
import play.api.libs.iteratee.Iteratee
import resource.managed

import scala.async.Async.{async, await}
import scala.concurrent.Future

trait CVSvcMongoCmp extends CVSvcCmp with Lifecycle {
  self: AppWideDefs
    with CVExportSvcCmp with EmailSenderSvcCmp with CVIndexingSvcLuceneCmp
    with CVDaoCmp with CVDraftDaoCmp with CVHistoryDaoCmp with LocationDaoCmp =>

  override val cvSvc = {
    val proxee = new CVSvcMongo
    createProxy(proxee, classOf[CVSvc], new MetricsAspect(proxee))
  }

  abstract override def start(): Unit = {
    super.start()

    rebuildIndexes()
  }

  abstract override def stop(): Unit = {
    super.stop()

    indexingSvc.close()
  }

  class CVSvcMongo extends CVSvc {

    override def create(cv: CV, owner: Owner, language: Language.Value): Future[Option[Int]] = {
      cvDraftDao.clearDraft(owner, language) flatMap { _ =>
        val result = cvDao.create(prepareForPublishing(cv), owner, language)
        result onSuccess {
          case _ =>
            indexingSvc.create(cv, owner, language)
            indexingSvc.commit()
        }
        result
      }
    }

    override def read(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]] = async {
      val draft = await(cvDraftDao.readDraft(owner, language))
      if (draft.isDefined) draft else await(cvDao.read(owner, language))
    }

    override def update(cv: CV, owner: Owner, language: Language.Value,
                        version: Int, caller: String): Future[Either[Failure, Int]] =
      async {
        await(cvDao.update(prepareForPublishing(cv), owner, language, version, caller)) match {
          case Some((oldCV, nextVersion)) =>
            val draftCleared = await(cvDraftDao.clearDraft(owner, language))
            val historySaved = await(cvHistoryDao.create(oldCV, owner, language, version))
            if (draftCleared && historySaved) {
              indexingSvc.update(cv, owner, language)
              indexingSvc.commit()
              Right(nextVersion)
            } else {
              Left(InternalServerError)
            }
          case None =>
            Left(GenericFailure)
        }
      }

    override def setDraft(cv: CV, owner: Owner, language: Language.Value,
                          basedOnVersion: Int, caller: String): Future[_] =
      cvDraftDao.setDraft(cv, owner, language, basedOnVersion, caller)

    override def readDraft(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]] =
      cvDraftDao.readDraft(owner, language)

    override def clearDraft(owner: Owner, language: Language.Value): Future[_] =
      cvDraftDao.clearDraft(owner, language)

    override def fetchAvailableLocations(): Future[Seq[Location]] =
      locationDao.fetchAvailableLocations()

    override def fetchExcerpts(): Future[Map[LocationCode, Map[Owner, Map[Language.Value, CVExcerpt]]]] =
      cvDao.fetchExcerpts() map {
        _ mapValues {
          _ groupBy {
            _.owner
          } mapValues {
            _ groupBy {
              _.language
            } mapValues {
              _.head
            }
          }
        }
      }

    override def find(query: String,
                      mode: SearchMode.Value): Future[Map[Language.Value, Map[LocationCode, Map[Owner, CVExcerpt]]]] =
      Future.successful(indexingSvc.find(query, mode))

    override def export(owner: Owner, language: Language.Value, format: Format.Value): Future[Option[CVExport]] = {
      import scalaz._, syntax.monadPlus._
      val result = for {
        (cv, version, lastModified, _) <- OptionT.optionT(read(owner, language))
        (bytes, extension) <- OptionT.optionT(cvExportSvc.export(cv, language, version, lastModified, format))
      } yield CVExport(bytes, getFilename(owner, language, extension), lastModified)
      result.run
    }

    override def groupExport(spec: Map[Owner, Set[Language.Value]]): Future[Option[CVGroupExport]] = {
      import scalaz.Scalaz._

      val exports = spec.toList flatMap {
        case (owner, languages) =>
          languages flatMap { language =>
            Seq(export(owner, language, Format.PDF), export(owner, language, Format.Docx))
          }
      }

      exports.sequence[Future, Option[CVExport]] map { exportOpts =>
        val baos = new ByteArrayOutputStream()

        (managed(new ZipOutputStream(baos)) map { zos =>
          exportOpts foreach { exportOpt =>
            exportOpt foreach { export =>
              val entry = new ZipEntry(export.filename)
              entry.setSize(export.bytes.length)
              zos.putNextEntry(entry)
              zos.write(export.bytes)
              zos.closeEntry()
            }
          }
        }).opt map { _ =>
          CVGroupExport(baos.toByteArray, "cvs.zip")
        }
      }
    }

    override def exportToEmail(caller: String, spec: Map[Owner, Set[Language.Value]]): Future[Unit] = {
      groupExport(spec) map { exportOpt =>
        exportOpt foreach { export =>
          emailSenderSvc.send(getEmailByUsername(caller), export, "application/zip")
        }
      }
    }

    // private

    private val stringOrdering: Ordering[String] = Ordering.comparatorToOrdering(String.CASE_INSENSITIVE_ORDER)

    private def prepareForPublishing(cv: CV): CV = {
      val projectOrd = implicitly[Ordering[Project]].reverse

      CV.projectsLens.mod(
        _.sorted(projectOrd) map (p => Project.tagsLens.mod(_.filter(_.nonEmpty).sorted(stringOrdering), p)),
        CV.certificatesLens.mod(_.sorted,
          CV.educationLens.mod(_.sorted,
            CV.expertiseLens.mod(_ map { e =>
              ExpertiseRecord.tagsLens.mod(_.filter(_.nonEmpty).sorted(stringOrdering), e)
            }, cv)
          )
        )
      )
    }

    private def getFilename(owner: Owner, language: Language.Value, extension: String): String =
      s"${owner.value}_$language.$extension"

    private def getEmailByUsername(username: String): String =
      username + "@" + appConfig.getString(Conf.Domain)

  }

  // private

  private def rebuildIndexes() = {
    if (!indexExists()) {
      appLogger.info("Building CVs index...")

      val job = cvDao.fetchAll().apply(Iteratee.foreach((indexingSvc.create _).tupled))

      job onSuccess {
        case _ =>
          appLogger.info("Indexing finished")
          indexingSvc.commit()
      }
      job onFailure {
        case t =>
          appLogger.info("Indexing failed", t)
          indexingSvc.rollback()
      }
    }
  }

  private def indexExists(): Boolean = {
    val indexDir = ConfUtil.getPath(appConfig.getString(Conf.Index.Dir))
    val indexDirPath = Paths.get(indexDir)
    Files.exists(indexDirPath) && {
      (managed(Files.newDirectoryStream(indexDirPath)) map {
        _.iterator().hasNext
      }).opt getOrElse false
    }
  }

}
