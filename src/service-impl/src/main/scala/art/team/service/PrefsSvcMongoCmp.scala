package art.team.service

import art.team.aop.AOPUtil._
import art.team.app.AppWideDefs
import art.team.dao.PrefsDaoCmp
import art.team.metrics.MetricsAspect
import art.team.model.Prefs

import scala.concurrent.Future

trait PrefsSvcMongoCmp extends PrefsSvcCmp {
  self: AppWideDefs with PrefsDaoCmp =>

  override val prefsSvc = {
    val proxee = new PrefsSvcMongo
    createProxy(proxee, classOf[PrefsSvc], new MetricsAspect(proxee))
  }

  class PrefsSvcMongo extends PrefsSvc {
    override def setPrefs(owner: String, prefs: Prefs): Future[_] =
      prefsDao.setPrefs(owner, prefs)

    override def readPrefs(owner: String): Future[Option[Prefs]] =
      prefsDao.readPrefs(owner)
  }
}
