package art.team.service

import java.io.File
import java.util.concurrent.ExecutorService

import art.team.aop.AOPUtil._
import art.team.app.{AppWideConf, Conf}
import art.team.dao.LocationDaoCmp
import art.team.dto.{CVExcerpt, SearchMode}
import art.team.metrics.MetricsAspect
import art.team.model._
import art.team.util.ConfUtil
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.en.EnglishAnalyzer
import org.apache.lucene.document._
import org.apache.lucene.index._
import org.apache.lucene.queryparser.classic.{ParseException, MultiFieldQueryParser}
import org.apache.lucene.queryparser.classic.QueryParser.Operator
import org.apache.lucene.sandbox.queries.DuplicateFilter
import org.apache.lucene.search._
import org.apache.lucene.search.join.{JoinUtil, ScoreMode}
import org.apache.lucene.store.{Directory, MMapDirectory}
import org.apache.lucene.util.Version
import resource.managed

import scala.collection.mutable

trait CVIndexingSvcLuceneCmp extends CVIndexingSvcCmp {
  self: AppWideConf with CVSvcCmp with LocationDaoCmp =>

  override val indexingSvc: IndexingSvc = {
    val proxee = new IndexingSvcLucene
    createProxy(proxee, classOf[IndexingSvc], new MetricsAspect(proxee))
  }

  class IndexingSvcLucene extends IndexingSvc {

    override def create(cv: CV, owner: Owner, language: Language.Value): Unit = {
      if (isIndexable(cv))
        indexWriter.addDocument(createDocument(cv, owner, language))
    }

    override def update(cv: CV, owner: Owner, language: Language.Value): Unit = {
      if (isIndexable(cv))
        indexWriter.updateDocument(new Term(mkId(owner, language)), createDocument(cv, owner, language))
    }

    override def commit(): Unit = {
      indexWriter.commit()
    }

    override def rollback(): Unit = {
      indexWriter.rollback()
    }

    override def close(): Unit = {
      indexWriter.close()
    }

    override def find(query: String, mode: SearchMode.Value): Map[Language.Value, Map[LocationCode, Map[Owner, CVExcerpt]]] =
      (managed(DirectoryReader.open(index)) map { implicit indexReader =>
        implicit val indexSearcher = mkIndexSearcher()
        findImpl(mkQuery(query, mode))
      }).either match {
        case Right(results) =>
          results
        case Left(errors) =>
          errors.head match {
            case _: ParseException =>
            case e =>
              appLogger.error("Lucene search failed", e)
          }
          Map.empty
      }

    // private

    private val MaxResults = 1000

    private def isIndexable(cv: CV): Boolean =
      cv.name.firstname.nonEmpty && cv.name.lastname.nonEmpty

    private object Fields {
      val Id = "id"
      val Language = "language"
      val Location = "location"
      val Owner = "owner"
      val Firstname = "firstname"
      val Lastname = "lastname"
      val Position = "position"
      val Tag = "tag"
      val Project = "project"
    }

    private lazy val SearchFields =
      Array(Fields.Owner, Fields.Firstname, Fields.Lastname, Fields.Position, Fields.Tag, Fields.Project)

    private lazy val analyzer: Analyzer = new EnglishAnalyzer()
    private lazy val index: Directory =
      new MMapDirectory(new File(ConfUtil.getPath(appConfig.getString(Conf.Index.Dir))))

    private lazy val indexWriter: IndexWriter = new IndexWriter(index, new IndexWriterConfig(Version.LATEST, analyzer))

    private lazy val executor: ExecutorService = blockingContext.prepare().asInstanceOf[ExecutorService]

    private def mkIndexSearcher()(implicit indexReader: IndexReader): IndexSearcher =
      new IndexSearcher(indexReader, executor)

    private def createDocument(cv: CV, owner: Owner, language: Language.Value): Document = {
      val doc = new Document()

      doc.add(new TextField(Fields.Id, mkId(owner, language), Field.Store.YES))

      doc.add(new StoredField(Fields.Language, language.toString))
      doc.add(new StoredField(Fields.Location, cv.location))

      doc.add(new StringField(Fields.Owner, owner.value, Field.Store.YES))
      doc.add(new TextField(Fields.Firstname, cv.name.firstname, Field.Store.YES))
      doc.add(new TextField(Fields.Lastname, cv.name.lastname, Field.Store.YES))
      doc.add(new TextField(Fields.Position, cv.position, Field.Store.YES))

      val tags = getTags(cv)
      tags foreach { tag =>
        doc.add(new TextField(Fields.Tag, tag, Field.Store.YES))
      }

      cv.projects foreach { project =>
        doc.add(new TextField(Fields.Project, project.name, Field.Store.YES))
      }

      doc
    }

    private def mkId(owner: Owner, language: Language.Value): String = s"${owner.value}_${language.toString}"

    private def getTags(cv: CV): Iterable[String] = {
      val tags = mutable.Set[String]()

      def addTags(seq: Seq[Tagged]): Unit =
        (seq.view map (_.tags)).flatten foreach (tags += _)

      addTags(cv.expertise)
      addTags(cv.projects)

      tags
    }

    private def mkQuery(queryString: String, mode: SearchMode.Value)(implicit indexSearcher: IndexSearcher): Query = {
      val parser = new MultiFieldQueryParser(SearchFields, analyzer)
      val operator = mode match {
        case SearchMode.And => Operator.AND
        case SearchMode.Or => Operator.OR
      }
      parser.setDefaultOperator(operator)

      val query = parser.parse(queryString)
      JoinUtil.createJoinQuery(Fields.Owner, false, Fields.Owner, query, indexSearcher, ScoreMode.None)
    }

    private def findImpl(query: Query)
               (implicit indexSearcher: IndexSearcher): Map[Language.Value, Map[LocationCode, Map[Owner, CVExcerpt]]] = {
      val filter = new DuplicateFilter(Fields.Id)
      val collector = TopScoreDocCollector.create(MaxResults, false)

      indexSearcher.search(query, filter, collector)

      val hits = collector.topDocs().scoreDocs

      hits.view map { hit =>
        val doc = indexSearcher.doc(hit.doc)

        val language = Language.withName(doc.getField(Fields.Language).stringValue())
        val owner = Owner(doc.getField(Fields.Owner).stringValue())
        val location = LocationCode(doc.getField(Fields.Location).stringValue())
        val firstname = doc.getField(Fields.Firstname).stringValue()
        val lastname = doc.getField(Fields.Lastname).stringValue()
        val position = doc.getField(Fields.Position).stringValue()

        CVExcerpt(language, owner, location, firstname, lastname, position)
      } groupBy {
        _.language
      } mapValues {
        _.view groupBy (_.location) mapValues (_.view groupBy (_.owner) mapValues (_.head))
      }
    }
  }

}
