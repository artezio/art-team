package art.team.service

import art.team.aop.AOPUtil._
import art.team.app.AppWideConf
import art.team.metrics.MetricsAspect
import art.team.model.Role
import art.team.security.{Security, Task}
import org.apache.commons.codec.digest.DigestUtils

import scala.async.Async.{async, await}
import scala.concurrent.Future

trait AuthSvcMongoCmp extends AuthSvcCmp {
  self: AppWideConf with SessionSvcCmp with UserSvcCmp with ExternalAuthSvcCmp =>

  override val authSvc = {
    val proxee = new AuthSvcMongo
    createProxy(proxee, classOf[AuthSvc], new MetricsAspect(proxee))
  }

  class AuthSvcMongo extends AuthSvc {

    override def authenticate(username: String, password: String): Future[Either[Failure, (String, Role.Value)]] =
      async {
        await(userSvc.read(username)) match {
          case Some(user) =>
            if (!user.passwordHashExpired) {
              val passwordHash = getPasswordHash(password)
              if (passwordHash == user.passwordHash)
                await(createSession(username, user.role))
              else
                Left(GenericFailure)
            } else {
              val valid = await(externalAuthSvc.isValid(username, password))
              if (valid) {
                val passwordHashSet = await(userSvc.renewPasswordHash(username, getPasswordHash(password)))
                if (passwordHashSet)
                  await(createSession(username, user.role))
                else
                  Left(InternalServerError)
              } else {
                Left(GenericFailure)
              }
            }
          case None =>
            val valid = await(externalAuthSvc.isValid(username, password))
            if (valid) {
              val defaultRole = Role.User
              val created = await(userSvc.create(username, getPasswordHash(password), defaultRole))
              if (created)
                await(createSession(username, defaultRole))
              else
                Left(InternalServerError)
            } else {
              Left(GenericFailure)
            }
        }
      }

    override def canPerform(caller: String, task: Task.Value): Future[Either[Failure, Boolean]] = async {
      await(userSvc.read(caller)) match {
        case Some(user) =>
          Right(canPerformTask(user.role, task))
        case None =>
          Left(InternalServerError)
      }
    }

    // private

    private def getPasswordHash(password: String): String =
      DigestUtils.md5Hex(password)

    private def canPerformTask(role: Role.Value, task: Task.Value): Boolean =
      Security.Config(role).contains(task)

    private def createSession(username: String, role: Role.Value): Future[Either[Failure, (String, Role.Value)]] =
      sessionSvc.create(username) map {
        case Some(sessionId) =>
          Right((sessionId, role))
        case None =>
          Left(InternalServerError)
      }

  }
}
