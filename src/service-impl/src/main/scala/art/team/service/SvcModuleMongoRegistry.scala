package art.team.service

import art.team.actor.ActorRegistry
import art.team.app.AppWideDefs
import art.team.cvexporter.CVExportSvcCmp
import art.team.dao.DaoModuleRegistry

trait SvcModuleMongoRegistry
  extends SvcModuleRegistry
  with ActorRegistry
  with SessionSvcMongoCmp
  with TicketSvcMongoCmp
  with UserSvcMongoCmp
  with ExternalAuthSvcCmp
  with AuthSvcMongoCmp
  with EmailSenderSvcCmp
  with CVIndexingSvcLuceneCmp
  with CVSvcMongoCmp
  with PrefsSvcMongoCmp
  with SchedulingSvcCmp {

  self: AppWideDefs with CVExportSvcCmp with DaoModuleRegistry =>

}
