package art.team.service

import akka.actor.{Actor, ActorRef, Props, Status}
import art.team.actor.message.PasswordHashesExpired
import art.team.actor.{PasswordHashExpiratorActorCmp, Tick}
import art.team.app.{AppWideConf, Lifecycle}
import org.joda.time.DateTime

import scala.concurrent.duration._

trait SchedulingSvcCmp extends Lifecycle {
  self: AppWideConf with PasswordHashExpiratorActorCmp =>

  abstract override def start(): Unit = {
    super.start()

    schedulingActor ! "start"
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  // private

  private val schedulingActor: ActorRef = actorSystem.actorOf(Props(new SchedulingActor))

  private class SchedulingActor extends Actor {

    override def receive: Receive = {
      case "start" =>
        start()
      case PasswordHashesExpired(success) =>
        if (success) appLogger.info("Password hashes expired successfully")
        else appLogger.error("Failed to expire password hashes")
      case Status.Failure(e) =>
        appLogger.error("SchedulingActor: failure response received", e)
    }

    // private

    private def start(): Unit = {
      val passwordHashExpirator = context.actorOf(Props(new PasswordHashExpiratorActor))
      context.system.scheduler.schedule(getTimeTillMidnight, 1.day, passwordHashExpirator, Tick())
    }

    private def getTimeTillMidnight: FiniteDuration = {
      val dt = DateTime.now()
      val currentMillis = dt.getMillis
      val millisAtMidnight = dt.withTime(23, 59, 59, 999).getMillis
      (millisAtMidnight - currentMillis).milliseconds
    }

  }

}
