package art.team.service

import javax.mail.internet.InternetAddress

import art.team.app.{AppWideConf, Conf}
import art.team.dto.Export
import courier._

import scala.language.implicitConversions

trait EmailSenderSvcCmp {
  self: AppWideConf =>

  val emailSenderSvc = new EmailSenderSvc

  class EmailSenderSvc {

    def send(to: String, export: Export, mimeType: String) = {
      val env = Envelope.from(appConfig.getString(Conf.Email.Originator)).
        to(to).
        subject(ExportSubject).
        content(Multipart().
          attachBytes(export.bytes, export.filename, mimeType).
          text(ExportBodyText)
        )
      sendEnvelope(env)
    }

    // private

    private val ExportSubject = "ART-TEAM export"
    private val ExportBodyText = "See attachement"

    private val mailer =
      Mailer(appConfig.getString(Conf.Email.Smtp.Host), appConfig.getInt(Conf.Email.Smtp.Port)).
        auth(appConfig.getBoolean(Conf.Email.Smtp.Auth)).
        as(appConfig.getString(Conf.Email.Smtp.Username), appConfig.getString(Conf.Email.Smtp.Password)).
        startTtls(appConfig.getBoolean(Conf.Email.Smtp.StartTls))()

    private implicit def toInternetAddress(address: String): InternetAddress =
      new InternetAddress(address)

    private def sendEnvelope(env: Envelope) =
      mailer(env)(blockingContext).onFailure({
        case e =>
          appLogger.error("Failed to send email", e)
      })(blockingContext)

  }

}
