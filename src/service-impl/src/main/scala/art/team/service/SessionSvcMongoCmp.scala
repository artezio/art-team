package art.team.service

import art.team.aop.AOPUtil._
import art.team.app.AppWideConf
import art.team.dao.SessionDaoCmp
import art.team.metrics.MetricsAspect
import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.DigestUtils

import scala.concurrent.Future
import scala.util.Random

trait SessionSvcMongoCmp extends SessionSvcCmp {
  self: AppWideConf with SessionDaoCmp =>

  override val sessionSvc = {
    val proxee = new SessionSvcMongo
    createProxy(proxee, classOf[SessionSvc], new MetricsAspect(proxee))
  }

  class SessionSvcMongo extends SessionSvc {

    override def create(username: String): Future[Option[String]] = {
      val sessionId = genSessionId()
      val sessionIdHash = getSessionIdHash(sessionId)
      sessionDao.create(username, sessionIdHash) map { created =>
        if (created) Some(sessionId) else None
      }
    }

    override def delete(sessionId: String): Future[Boolean] = {
      val sessionIdHash = getSessionIdHash(sessionId)
      sessionDao.delete(sessionIdHash)
    }

    override def verifyAndGetUsername(sessionId: String): Future[Option[String]] = {
      val sessionIdHash = getSessionIdHash(sessionId)
      sessionDao.getUsername(sessionIdHash)
    }

    // private

    private def genSessionId(): String = {
      val ba = new Array[Byte](32)
      Random.nextBytes(ba)
      Hex.encodeHexString(ba)
    }

    private def getSessionIdHash(sessionId: String): String =
      DigestUtils.md5Hex(sessionId)

  }
}
