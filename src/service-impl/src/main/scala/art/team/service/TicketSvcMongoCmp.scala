package art.team.service

import art.team.aop.AOPUtil._
import art.team.app.AppWideConf
import art.team.dao.TicketDaoCmp
import art.team.dto.TicketInfo
import art.team.metrics.MetricsAspect
import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.DigestUtils

import scala.concurrent.Future
import scala.util.Random

trait TicketSvcMongoCmp extends TicketSvcCmp {
  self: AppWideConf with TicketDaoCmp =>

  override val ticketSvc = {
    val proxee = new TicketSvcMongo
    createProxy(proxee, classOf[TicketSvc], new MetricsAspect(proxee))
  }

  class TicketSvcMongo extends TicketSvc {

    override def create(username: String, data: Option[String]): Future[Option[String]] = {
      val ticketId = genTicketId()
      val ticketIdHash = getTicketIdHash(ticketId)
      ticketDao.create(username, ticketIdHash, data) map { created =>
        if (created) Some(ticketId) else None
      }
    }

    override def use(ticketId: String): Future[Option[TicketInfo]] = {
      val ticketIdHash = getTicketIdHash(ticketId)
      ticketDao.use(ticketIdHash)
    }

    // private

    private def genTicketId(): String = {
      val ba = new Array[Byte](32)
      Random.nextBytes(ba)
      Hex.encodeHexString(ba)
    }

    private def getTicketIdHash(ticketId: String): String =
      DigestUtils.md5Hex(ticketId)

  }
}
