package art.team.service

import art.team.aop.AOPUtil._
import art.team.app.AppWideConf
import art.team.dao.UserDaoCmp
import art.team.metrics.MetricsAspect
import art.team.model.{Role, User}

import scala.concurrent.Future

trait UserSvcMongoCmp extends UserSvcCmp {
  self: AppWideConf with UserDaoCmp =>

  override val userSvc = {
    val proxee = new UserSvcMongo
    createProxy(proxee, classOf[UserSvc], new MetricsAspect(proxee))
  }

  class UserSvcMongo extends UserSvc {

    override def create(username: String, passwordHash: String, role: Role.Value): Future[Boolean] =
      userDao.create(username, passwordHash, role)

    override def read(username: String): Future[Option[User]] =
      userDao.read(username)

    override def renewPasswordHash(username: String, passwordHash: String): Future[Boolean] =
      userDao.renewPasswordHash(username, passwordHash)

    override def fetchRoles(): Future[Map[String, Role.Value]] =
      userDao.fetchRoles()

    override def updateUser(username: String, role: Option[Role.Value]): Future[Boolean] =
      userDao.updateUser(username, role)

  }
}
