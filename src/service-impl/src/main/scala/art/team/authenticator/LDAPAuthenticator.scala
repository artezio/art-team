package art.team.authenticator

import java.util
import javax.naming.Context
import javax.naming.directory.InitialDirContext

import art.team.app.{AppWideConf, Conf}

import scala.concurrent.Future
import scala.util.control.Exception._

object LDAPAuthenticator extends Authenticator {

  override def isValid(username: String, password: String, config: AppWideConf): Future[Boolean] = {
    val env: util.Hashtable[String, String] = new util.Hashtable()

    val url = config.appConfig.getString(Conf.Auth.Ldap.URL)
    val group = config.appConfig.getString(Conf.Auth.Ldap.Group)

    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")
    env.put(Context.PROVIDER_URL, url)

    env.put(Context.SECURITY_AUTHENTICATION, "simple")
    env.put(Context.SECURITY_PRINCIPAL, s"$group\\$username")
    env.put(Context.SECURITY_CREDENTIALS, password)

    Future {
      val validOpt = allCatch opt {
        val ctx = new InitialDirContext(env)
        ctx != null
      }
      validOpt getOrElse false
    }(config.blockingContext)
  }

}
