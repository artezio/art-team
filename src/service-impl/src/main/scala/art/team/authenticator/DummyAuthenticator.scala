package art.team.authenticator

import art.team.app.{AppWideConf, Conf}

import scala.concurrent.Future

object DummyAuthenticator extends Authenticator {

  override def isValid(username: String, password: String, config: AppWideConf): Future[Boolean] =
    Future.successful(!username.isEmpty && password == config.appConfig.getString(Conf.Auth.Dummy.Password))

}
