package art.team.authenticator

import art.team.app.AppWideConf

import scala.concurrent.Future

trait Authenticator {

  def isValid(username: String, password: String, config: AppWideConf): Future[Boolean]

}
