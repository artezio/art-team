package art.team.actor

import akka.actor.Actor
import akka.pattern.pipe
import art.team.actor.message.PasswordHashesExpired
import art.team.app.AppWideConf
import art.team.dao.UserDaoCmp

trait PasswordHashExpiratorActorCmp {
  self: AppWideConf with UserDaoCmp =>

  class PasswordHashExpiratorActor extends Actor {
    override def receive: Receive = {
      case x: Tick =>
        userDao.expirePasswordHashes().map(PasswordHashesExpired) pipeTo sender
    }
  }

}
