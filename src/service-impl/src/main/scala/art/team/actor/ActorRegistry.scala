package art.team.actor

import art.team.app.AppWideConf
import art.team.dao.DaoModuleRegistry

trait ActorRegistry
  extends PasswordHashExpiratorActorCmp {

  self: AppWideConf with DaoModuleRegistry =>
}
