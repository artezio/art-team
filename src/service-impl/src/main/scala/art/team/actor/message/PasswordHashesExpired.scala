package art.team.actor.message

case class PasswordHashesExpired(success: Boolean) extends AnyVal
