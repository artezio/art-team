define(['jquery', 'app/Context', 'session/SessionStorage', 'event/EventType', 'event/EventSvc'],
  function ($, Context, SessionStorage, EventType, EventSvc) {
    'use strict';

    var getRouter = function () {
      return require('app/Router');
    };

    var checkErrors, showLoading, notify;

    checkErrors = function (xhr) {
      return xhr.error(function (xhr) {
        var Router = getRouter();
        switch (xhr.status) {
          case 401:
            Router.toLogin(true);
            break;
          case 403:
            Router.toPermissionViolation();
            break;
        }
      });
    };

    showLoading = function (xhr) {
      var binding = Context.getBinding();
      binding.set('loading', true);
      return xhr.always(function () {
        binding.set('loading', false);
      });
    };

    notify = function (xhr, params) {
      if (params.notification) {
        xhr.done(function () {
          EventSvc.notify(EventType.UI_NOTIFICATION, {
            group: params.notification,
            type: 'INFO'
          });
        });
      }

      return xhr.error(function (_xhr, _status, errorThrown) {
        if (!errorThrown) {
          EventSvc.notify(EventType.UI_NOTIFICATION, {
            group: 'notification.common.connectionError',
            type: 'ERROR'
          });
        }
      });
    };

    return {

      call: function (url, settings, params) {
        params = params || {};

        if (!params.disableAuth) {
          var sessionId = params.overridenSessionId || SessionStorage.getSessionId();
          settings.headers = { Authorization: 'ARTAuth sessionId="' + sessionId + '"' };
        }

        var xhr = $.ajax(url, settings);
        var xhr2 = checkErrors(xhr);

        return notify(params.noLoading ? xhr2 : showLoading(xhr2), params);
      }

    }
  });
