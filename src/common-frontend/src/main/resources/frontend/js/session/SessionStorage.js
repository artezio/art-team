define(['cookies'], function (Cookies) {
  'use strict';

  return {

    clearSessionInfo: function () {
      localStorage.clear();
      Cookies.expire('username').expire('role').expire('sessionId');
    },

    setSessionInfo: function (username, role, sessionId) {
      Cookies.set('username', username).set('role', role).set('sessionId', sessionId);
    },

    setUsername: function (username) {
      Cookies.set('username', username);
    },

    getUsername: function () {
      return Cookies.get('username');
    },

    getRole: function () {
      return Cookies.get('role');
    },

    getSessionId: function () {
      return Cookies.get('sessionId');
    },

    saveState: function (state) {
      localStorage.setItem('savedState', state);
    },

    getSavedState: function () {
      return localStorage.getItem('savedState');
    }

  };

});
