define([
    'react',
    'morearty',
    'immutable',
    'app/Router',
    'service/I18NSvc',
    'service/NotificationSvc',
    'service/CVSvc',
    'c!component/menu/SideMenu',
    'c!component/menu/MenuAction',
    'c!component/menu/ActionGroup',
    'c!component/pickup/SearchMenu',
    'c!component/cv/CVView',
    'c!component/pickup/Empty'
  ],
  function (React, Morearty, Imm, Router, I18NSvc, NotificationSvc, CVSvc, SideMenu, MenuAction, ActionGroup, SearchMenu, CVView, Empty) {
    'use strict';

    var t = I18NSvc.t;

    var fetchLatest = function () {
      var defaultBinding = this.getDefaultBinding(), languageBinding = this.getBinding('language');
      var owner = defaultBinding.get('cv.active');
      var language = languageBinding.get();
      if (owner) {
        return CVSvc.fetchCV(owner, language).done(function (response) {
          defaultBinding
            .atomically()
            .set('cv.data', Imm.fromJS(response))
            .set('language', language)
            .commit();
        }).fail(function () {
          defaultBinding.delete('cv.data');
        });
      }
    };

    var someSelected, getExportSpec, onGroupExport, onEmail, onEdit, onClear;

    someSelected = function (binding) {
      return binding.get('selected').find(Morearty.Util.identity);
    };

    getExportSpec = function (selected) {
      var spec = {};
      selected.filter(Morearty.Util.identity).forEach(function (selected, owner) {
        spec[owner] = ['en', 'ru'];
      });
      return spec;
    };

    onGroupExport = function () {
      var spec = getExportSpec(this.getDefaultBinding().get('selected'));
      CVSvc.groupExport(spec);
    };

    onEmail = function () {
      var spec = getExportSpec(this.getDefaultBinding().get('selected'));
      CVSvc.groupExportToEmail(spec);
    };

    onEdit = function () {
      var owner = this.getDefaultBinding().get('cv.active');
      Router.toCVEdit(owner);
    };

    onClear = function () {
      NotificationSvc.showModal('modal.clearPickUp', function (confirmed) {
        if (confirmed) {
          this.getDefaultBinding().atomically()
            .clear('selectedResults')
            .set('filter', '')
            .clear('selected')
            .clear('searchResults')
            .clear('cv.active')
            .commit();
        }
      }.bind(this));
    };

    return React.createClass({

      displayName: 'PickUpPage',
      mixins: [Morearty.Mixin],

      getDefaultState: function () {
        var I = Imm;
        return I.Map({
          searchResults: I.Map(),
          filter: '',
          mode: 'and',
          selected: I.Map(),
          selectedResults: I.Map(),
          /* language */
          cv: I.Map()
        });
      },

      componentDidMount: function () {
        var defaultBinding = this.getDefaultBinding();
        var data = defaultBinding.sub('cv.data');
        if (!data.get()) {
          fetchLatest.call(this);
        }

        var cb = fetchLatest.bind(this);
        this.addBindingListener(defaultBinding, 'cv.active', cb);
        this.addBindingListener(this.getBinding('language'), cb);
      },

      shouldRemoveListeners: function () { return true; },

      componentWillUnmount: function () {
        var binding = this.getDefaultBinding();
        binding.atomically().delete('cv.data').commit(false);
      },

      render: function () {
        var defaultBinding = this.getDefaultBinding(), languageBinding = this.getBinding('language');
        var noneSelected = !someSelected(defaultBinding);
        var filterEmpty = !defaultBinding.get('filter');

        var cvSelected = defaultBinding.get('cv.active');
        var dataFetched = defaultBinding.get('cv.data');

        var _ = React.DOM;
        return _.div({ className: 'wrapper' },
          SideMenu({
            binding: languageBinding,
            main: SearchMenu({ binding: this.getBinding() }),
            actions: [
              ActionGroup({ key: 'export-group' },
                MenuAction({ key: 'export', onClick: onGroupExport.bind(this), disabled: noneSelected, icon: 'export', label: t('component.common.action.export'), liClass: 'export' }),
                MenuAction({ key: 'email', onClick: onEmail.bind(this), disabled: noneSelected, icon: 'email', label: t('component.common.action.email'), liClass: 'export' })
              ),
              ActionGroup({ key: 'edit-clear-group' },
                MenuAction({ key: 'edit', onClick: onEdit.bind(this), disabled: !cvSelected, icon: 'edit', label: t('component.common.action.edit')}),
                MenuAction({ key: 'clear', onClick: onClear.bind(this), disabled: noneSelected && filterEmpty, icon: 'clear', label: t('component.common.action.clear')})
              )
            ]
          }),
          cvSelected && dataFetched ?
            (dataFetched ? CVView({ binding: { default: defaultBinding.sub('cv'), language: languageBinding, filter: defaultBinding.sub('filter') } }) : null) :
            Empty()
        );
      }

    });

  });
