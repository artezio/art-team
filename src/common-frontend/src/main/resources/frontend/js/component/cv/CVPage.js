define([
    'jquery',
    'react',
    'morearty',
    'immutable',
    'app/Router',
    'util/CommonUtil',
    'service/I18NSvc',
    'service/CVSvc',
    'service/NotificationSvc',
    'c!component/menu/SideMenu',
    'c!component/cv/CV',
    'c!component/menu/MenuAction',
    'c!component/menu/ActionGroup'
  ],
  function ($, React, Morearty, Imm, Router, CommonUtil, I18NSvc, CVSvc, NotificationSvc, SideMenu, CV, MenuAction, ActionGroup) {
    'use strict';

    var t = I18NSvc.t;

    var getOwner, getLanguage;

    getOwner = function (self) {
      return self.getDefaultBinding().get('owner');
    };

    getLanguage = function (self) {
      return self.getBinding('language').get();
    };

    var ignoringChangeListener, listenForCVChange, toJson;

    ignoringChangeListener = function (listenerId, f) {
      this.getDefaultBinding().withDisabledListener(listenerId, f);
    };

    listenForCVChange = function () {
      var binding = this.getDefaultBinding();
      var listenerId = binding.addListener('data.cv', binding.set.bind(binding, 'changed', true));
      CommonUtil.updateTransientState(this, function (transientState) {
        transientState.cvChangeListenerId = listenerId;
      });
      return listenerId;
    };

    toJson = function () {
      return this.getDefaultBinding().get('data').toJS();
    };

    var setCV, fetchDraft, fetchLatest, fetchCV, fetchAndSetCV, fetchCVAndClearHistory;

    setCV = function (self, draft, changed, data) {
      self.getDefaultBinding().atomically()
        .set('data', Imm.fromJS(data))
        .set('changed', changed)
        .set('draft', draft)
        .commit();
    };

    fetchDraft = function (self, owner, language, dfd) {
      return CVSvc.fetchDraft(owner, language).done(function (response) {
        dfd.resolve(true, false, response);
      });
    };

    fetchLatest = function (self, owner, language, dfd) {
      return CVSvc.fetchCV(owner, language).done(function (response) {
        dfd.resolve(false, false, response);
      }).fail(function (xhr) {
        if (xhr.status === 404) {
          dfd.resolve(false, true, CVSvc.getTemplate(language));
        }
      });
    };

    fetchCV = function (self, owner, language) {
      var dfd = $.Deferred();

      fetchDraft(self, owner, language, dfd).fail(function (xhr) {
        if (xhr.status === 404) {
          fetchLatest(self, owner, language, dfd);
        }
      });

      return dfd.promise();
    };

    fetchAndSetCV = function (self, owner, language) {
      return fetchCV(self, owner, language).done(function (draft, changed, data) {
        if (self.isMounted()) {
          setCV(self, draft, changed, data);
        }
      });
    };

    fetchCVAndClearHistory = function (self, owner, language) {
      var transientState = CommonUtil.getTransientState(self);
      ignoringChangeListener.call(self, transientState.cvChangeListenerId, function () {
        return fetchAndSetCV(self, owner, language).always(function () {
          Morearty.History.clear(self.getDefaultBinding().sub('data.cv'));
          initDraftAutoSave.call(self);
        });
      });
    };

    var saveDraft, initDraftAutoSave, clearDraftAutoSave;

    saveDraft = function () {
      var binding = this.getDefaultBinding();
      if (binding.get('changed')) {
        var value = toJson.call(this);
        CVSvc.saveDraft(getOwner(this), value).done(function () {
          binding.atomically()
            .set('changed', false)
            .set('draft', true)
            .commit();
        });
      }
    };

    initDraftAutoSave = function () {
      var intervalId = setInterval(saveDraft.bind(this), 60000);
      CommonUtil.updateTransientState(this, function (transientState) {
        transientState.autoSaveInternalId = intervalId;
      });
      return intervalId;
    };

    clearDraftAutoSave = function () {
      var transientState = CommonUtil.getTransientState(this);
      var autoSaveInternalId = transientState.autoSaveInternalId;
      if (autoSaveInternalId) {
        clearInterval(autoSaveInternalId);
        delete transientState.autoSaveInternalId;
      }
    };

    var initSideMenu = function (self) {
      var binding = self.getDefaultBinding(), languageBinding = self.getBinding('language');
      var dataBinding = binding.sub('data.cv');

      var published = binding.get('data.version');
      var edit = binding.get('edit');

      return CommonUtil.papply(SideMenu, {
        binding: languageBinding,
        actions: [
          ActionGroup({ key: 'export-group' },
            MenuAction({ key: 'export', onClick: self.onExport, disabled: !published, icon: 'export', label: t('component.common.action.export'), liClass: 'export' }),
            MenuAction({ key: 'email', onClick: self.onEmail, disabled: !published, icon: 'email', label: t('component.common.action.email'), liClass: 'export' })
          ),
          edit ? [
            MenuAction({ key: 'finishEditing', onClick: self.onFinishEditing, icon: 'draft', label: t('component.cvsidemenu.finishEditing') }),
            MenuAction({ key: 'revertDraft', onClick: self.onRevertDraft, disabled: !binding.get('draft'), icon: 'revert', label: t('component.cvsidemenu.revertDraft') }),
            MenuAction({ key: 'publish', onClick: self.onPublish, disabled: !(binding.get('draft') || binding.get('changed')), icon: 'save', label: t('component.cvsidemenu.publish') }),

            ActionGroup({ key: 'history-group' },
              MenuAction({
                key: 'undo',
                onClick: function () { Morearty.History.undo(dataBinding); },
                disabled: !Morearty.History.hasUndo(dataBinding),
                icon: 'undo',
                title: t('component.cvsidemenu.undo')
              }),
              MenuAction({
                key: 'redo',
                onClick: function () { Morearty.History.redo(dataBinding); },
                disabled: !Morearty.History.hasRedo(dataBinding),
                icon: 'redo',
                title: t('component.cvsidemenu.redo')
              })
            )
          ] : [
            MenuAction({ key: 'edit', onClick: self.onEdit, icon: 'edit', label: t('component.cvsidemenu.edit') })
          ]
        ]
      });
    };

    return React.createClass({

      displayName: 'CVPage',
      mixins: [Morearty.Mixin],

      getMergeStrategy: function () {
        return Morearty.MergeStrategy.MERGE_REPLACE;
      },

      getDefaultState: function () {
        return Imm.Map({
          language: I18NSvc.getLanguage(),
          history: Imm.Map(),
          changed: false,
          draft: false
        });
      },

      componentDidMount: function () {
        var self = this;
        var binding = self.getDefaultBinding();
        var languageBinding = self.getBinding('language');

        var cvChangeListenerId = listenForCVChange.call(self);
        initDraftAutoSave.call(self);

        ignoringChangeListener.call(self, cvChangeListenerId, function () {
          return fetchAndSetCV(self, getOwner(self), getLanguage(self)).done(function () {
            if (self.isMounted()) {
              Morearty.History.init(binding.sub('data.cv'));
            }
          });
        });

        this.addBindingListener(binding, 'owner', function () {
          clearDraftAutoSave.call(self);
          fetchCVAndClearHistory(self, getOwner(self), languageBinding.get());
        });

        this.addBindingListener(languageBinding, function () {
          if (binding.get('edit')) {
            saveDraft.call(self);
          }
          clearDraftAutoSave.call(self);
          fetchCVAndClearHistory(self, getOwner(self), languageBinding.get());
        });
      },

      shouldRemoveListeners: function () { return true; },

      componentWillUnmount: function () {
        var binding = this.getDefaultBinding();
        var transientState = CommonUtil.getTransientState(this);
        binding.removeListener(transientState.cvChangeListenerId);

        clearDraftAutoSave.call(this);
        Morearty.History.destroy(binding.sub('data.cv'), { notify: false });
        binding.atomically().delete('data').commit({ notify: false });
      },

      onExport: function () {
        CVSvc.exportTo(getOwner(this), getLanguage(this));
      },

      onEmail: function () {
        CVSvc.exportToEmail(getOwner(this));
      },

      onEdit: function () {
        Router.toCVEdit(getOwner(this));
      },

      onFinishEditing: function () {
        saveDraft.call(this);
        Router.toCV(getOwner(this));
      },

      onRevertDraft: function () {
        var self = this;

        NotificationSvc.showModal('modal.revertDraft', function (confirmed) {
          if (confirmed) {
            var owner = getOwner(self);
            var language = getLanguage(self);
            CVSvc.clearDraft(owner, language).done(function () {
              var transientState = CommonUtil.getTransientState(self);
              ignoringChangeListener.call(self, transientState.cvChangeListenerId, function () {
                var dfd = $.Deferred();
                fetchLatest(self, owner, language, dfd);
                dfd.done(function (draft, changed, data) {
                  setCV(self, draft, changed, data);
                });
                return dfd.promise();
              });
            });
          }
        });
      },

      onPublish: function () {
        var self = this;

        NotificationSvc.showModal('modal.publishCV', function (confirmed) {
          if (confirmed) {
            var binding = self.getDefaultBinding();
            var owner = getOwner(self);
            if (binding.get('draft') || binding.get('changed')) {
              var data = toJson.call(self);
              CVSvc.publishCV(owner, data).done(function () {
                var transientState = CommonUtil.getTransientState(self);
                ignoringChangeListener.call(self, transientState.cvChangeListenerId, function () {
                  var dfd = $.Deferred();
                  fetchLatest(self, owner, data.language, dfd);
                  dfd.done(function (draft, changed, data) {
                    setCV(self, draft, changed, data);
                  });
                  return dfd.promise();
                });
              });
            }
          }
        });
      },

      render: function () {
        var defaultBinding = this.getDefaultBinding();
        var SideMenu = initSideMenu(this);

        var _ = React.DOM;
        return _.div({ className: 'wrapper' },
          SideMenu(),
          defaultBinding.get('data.cv') ?
            CV({ binding: this.getBinding(), readOnly: !defaultBinding.get('edit') }) :
            null
        );
      }

    });

  });
