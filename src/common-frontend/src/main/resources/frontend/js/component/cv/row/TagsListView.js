define(['react', 'morearty', 'util/CommonUtil', 'c!component/common/HighlightedText'],
  function (React, Morearty, CommonUtil, HighlightedText) {
    'use strict';

    return React.createClass({

      displayName: 'TagsListView',
      mixins: [Morearty.Mixin],

      shouldComponentUpdateOverride: function (shouldComponentUpdate, nextProps) {
        return this.props.filter !== nextProps.filter || shouldComponentUpdate();
      },

      render: function () {
        var self = this;
        var binding = self.getDefaultBinding();

        var filter = self.props.filter;
        var patterns = filter && filter.toLowerCase().split(' ');

        var _ = React.DOM;
        var mkTag = function (tag, index) {
          var text = binding.get(index);
          var highlight = patterns && CommonUtil.mentions(patterns, text.toLowerCase(), true);
          var spanClass = React.addons.classSet({
            tag: true,
            highlight: highlight
          });

          return _.span({ key: index, className: spanClass }, highlight ? text : HighlightedText({ text: text, pattern: filter }));
        };

        return _.span(null,
          binding.get().map(mkTag).toArray()
        );
      }

    });

  });
