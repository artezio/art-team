define(['react', 'morearty', 'service/I18NSvc', 'c!component/common/AutoInput', 'c!component/cv/row/TagsList'],
  function (React, Morearty, I18NSvc, AutoInput, TagsList) {
    'use strict';

    return React.createClass({

      displayName: 'ExpertiseRow',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();
        var area = binding.sub('area');
        var tags = binding.sub('tags');

        var tl = I18NSvc.forLang(this.props.language);

        var _ = React.DOM;
        return _.div({ className: 'tags-wrapper' },
          _.span({ className: 'label' },
            AutoInput({ binding: area, onChange: Morearty.Callback.set(area), placeholder: tl('component.cv.placeholder.expertise.area'), autofocus: this.props.autofocus })
          ),
          TagsList({ binding: tags, placeholder: tl('component.cv.placeholder.expertise.tag') })
        );
      }

    });

  });
