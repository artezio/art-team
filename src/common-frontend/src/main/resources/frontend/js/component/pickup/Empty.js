define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  return React.createClass({

    displayName: 'Empty',
    mixins: [Morearty.Mixin],

    render: function () {
      var _ = React.DOM;
      return _.div({ className: 'wrapper empty' },
        _.div({ className: 'cv-empty' })
      );
    }
  })
});
