define([
    'react',
    'morearty',
    'immutable',
    'app/Paths',
    'util/CommonUtil',
    'session/SessionStorage',
    'service/I18NSvc',
    'service/SharedDataSvc',
    'service/CVSvc',
    'service/SecuritySvc',
    'c!component/common/Link',
    'c!component/common/HighlightedText',
    'component/cvlist/Constants'
  ],
  function (React, Morearty, Imm, Paths, CommonUtil, SessionStorage, I18NSvc, SharedDataSvc, CVSvc, SecuritySvc, Link, HighlightedText, Constants) {

    var input = React.createFactory(Morearty.DOM.input);
    var option = React.createFactory(Morearty.DOM.option);

    var t = I18NSvc.t;

    var onSelectExportLanguage, onExport;

    onSelectExportLanguage = function (binding, event) {
      binding.set(event.target.checked);
    };

    onExport = function (owner, exportLanguagesBinding, event) {
      event.preventDefault();
      var en = exportLanguagesBinding.get('en'), ru = exportLanguagesBinding.get('ru');

      var languages = [];
      if (en) languages.push('en');
      if (ru) languages.push('ru');

      var spec = {};
      spec[owner] = languages;
      CVSvc.groupExport(spec);
    };

    var showMoreResults = function (event) {
      event.preventDefault();
      var binding = this.getDefaultBinding();
      binding.sub('numResults').update(function (numResults) {
        var found = binding.get('shown').count();
        return Math.min(numResults + Constants.RESULTS_BUCKET_SIZE, found);
      });
    };

    return React.createClass({

      displayName: 'CVList',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();
        var languageBinding = this.getBinding('language'), language = languageBinding.get();

        var filter = binding.sub('filter'), filterValue = filter.get();
        var searchResultsBinding = binding.sub('searchResults'), searchResults = searchResultsBinding.get(language);
        var rolesBinding = binding.sub('roles'), roles = rolesBinding.get();
        var exportLanguagesBinding = binding.sub('exportLanguages');
        var shown = binding.get('shown');
        var numResults = binding.get('numResults');

        var Hl = CommonUtil.papply(HighlightedText, { pattern: filterValue });

        var _ = React.DOM;

        var mkRow = function (item, owner) {
          var languages = exportLanguagesBinding.sub(owner);
          var en = languages.sub('en');
          var ru = languages.sub('ru');

          var liExportClass = React.addons.classSet({
            button: true,
            'export': true,
            disabled: !en.get() && !ru.get()
          });

          var excerpt = item.get(language);

          var availableInEnglish = item.has('en');
          var availableInRussian = item.has('ru');

          var mkRoleBlock = function () {
            var role = roles.get(owner);
            if (role && owner !== SessionStorage.getUsername()) {
              var mkOption = function (role) {
                return option({ key: role, value: role }, t('component.common.role.' + role));
              };

              var onChangeRole = function (event) {
                var newRole = event.target.value;
                SecuritySvc.setRole(owner, newRole).done(function () {
                  rolesBinding.set(owner, newRole);
                });
              };

              return SecuritySvc.isAdmin() ? _.span({ className: 'roles' },
                _.span({ className: 'label' }, t('component.cvlist.role')),
                _.select({ value: role, onChange: onChangeRole.bind(this) },
                  SecuritySvc.getRolesCodes().map(mkOption)
                )
              ) : null;
            } else {
              return null;
            }
          };

          return excerpt ? _.li({ key: owner },
            _.div(null,
              _.ul({ className: 'export-languages' },
                _.li(null,
                  _.label(null, input({ type: 'checkbox', checked: availableInEnglish && en.get(), disabled: !availableInEnglish, onChange: onSelectExportLanguage.bind(null, en) }), t('component.common.language.english'))
                ),
                _.li(null,
                  _.label(null, input({ type: 'checkbox', checked: availableInRussian && ru.get(), disabled: !availableInRussian, onChange: onSelectExportLanguage.bind(null, ru) }), t('component.common.language.russian'))
                ),
                _.li({ className: liExportClass },
                  _.a({ onClick: onExport.bind(null, owner, languages) }, t('component.common.action.export'))
                )
              ),
              mkRoleBlock(),
              Link({ href: Paths.getCVPath(owner) },
                _.span(null, Hl({ text: excerpt.get('lastname') }), ' ', Hl({ text: excerpt.get('firstname') }))
              ),
              _.br(),
              _.span(null, Hl({ text: excerpt.get('position') }), ', ', Hl({ text: SharedDataSvc.getLocationDescription(excerpt.get('location'), language) }))
            )
          ) : null;
        };

        return _.div(null,
          _.ul({ className: 'listing' },
            searchResults ?
              searchResults.filter(function (_, owner) { return shown.has(owner); }).take(numResults).map(mkRow).toArray() :
              null
          ),
          numResults < shown.count() ?
            _.a({ className: 'show-more', href: '#', onClick: showMoreResults.bind(this) }, t('component.cvlist.more')) :
            null
        );
      }

    });

  });
