define(['react', 'morearty', 'app/Paths', 'app/Router', 'c!component/common/Icon'], function (React, Morearty, Paths, Router, Icon) {
  'use strict';

  var isCurrent = function (url) {
    return url === Router.getCurrentPath();
  };

  var onClick = function (href, event) {
    event.preventDefault();
    if (!this.props.disabled) Router.setRoute(href);
  };

  return React.createClass({

    displayName: 'Link',
    mixins: [Morearty.Mixin],

    render: function () {
      var href = this.props.href;
      var aClass = React.addons.classSet({
        current: isCurrent(href) && !this.props.disabled,
        disabled: this.props.disabled
      });

      var _ = React.DOM;
      return _.a({ className: aClass, href: href, onClick: onClick.bind(this, href), title: this.props.title },
        this.props.icon ? Icon({ type: this.props.icon }) : null,
        this.props.label || this.props.children
      );
    }

  });

});
