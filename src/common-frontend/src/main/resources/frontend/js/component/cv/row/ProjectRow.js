define(['react', 'morearty', 'service/I18NSvc', 'service/CVSvc', 'c!component/common/AutoInput', 'c!component/common/AutoTextarea', 'c!component/cv/row/TagsList'],
  function (React, Morearty, I18NSvc, CVSvc, AutoInput, AutoTextarea, TagsList) {
    'use strict';

    var option = React.createFactory(Morearty.DOM.option);

    var focusIfNeeded, onDidMountOrUpdate;

    focusIfNeeded = function (self) {
      if (self.props.autofocus) {
        self.refs.name.getDOMNode().focus();
      }
    };

    onDidMountOrUpdate = function (self) {
      focusIfNeeded(self);
    };

    return React.createClass({

      displayName: 'ProjectRow',
      mixins: [Morearty.Mixin],

      componentDidMount: function () {
        onDidMountOrUpdate(this);
      },

      componentDidUpdate: function () {
        onDidMountOrUpdate(this);
      },

      render: function () {
        var binding = this.getDefaultBinding();
        var startDate = binding.sub('startDate');
        var endDate = binding.sub('endDate');
        var name = binding.sub('name');
        var company = binding.sub('company');
        var description = binding.sub('description');
        var position = binding.sub('position');
        var responsibilities = binding.sub('responsibilities');
        var tags = binding.sub('tags');

        var stringToIntTransformer = function (yearAsString) { return parseInt(yearAsString); };

        var availableYears = CVSvc.getAvailableYears();
        var availableMonths = CVSvc.getAvailableMonths();

        var tl = I18NSvc.forLang(this.props.language);

        var _ = React.DOM;
        return _.div({ className: 'item' },

          _.span({ className: 'date' },
            _.span({ className: 'from' },
              _.span({ className: 'label label_bold' },tl('component.cv.project.startDate')),
              _.select({ value: startDate.get('year'), onChange: Morearty.Callback.set(startDate, 'year', stringToIntTransformer) },
                availableYears.map(function (year, index) {
                  return option({ key: index, value: year.value}, year.name);
                })
              ),
              _.select({ value: startDate.get('month'), onChange: Morearty.Callback.set(startDate, 'month', stringToIntTransformer) },
                availableMonths.map(function (month, index) {
                  return option({ key: index, value: month.value}, month.name);
                })
              )
            ),
            _.span({ className: 'to' },
              _.span({ className: 'label label_bold' },tl('component.cv.project.endDate')),
              _.select({ value: endDate.get('year'), onChange: Morearty.Callback.set(endDate, 'year', stringToIntTransformer) },
                availableYears.map(function (year, index) {
                  return option({ key: index, value: year.value}, year.name);
                })
              ),
              _.select({ value: endDate.get('month'), onChange: Morearty.Callback.set(endDate, 'month', stringToIntTransformer) },
                availableMonths.map(function (month, index) {
                  return option({ key: index, value: month.value}, month.name);
                })
              )
            )
          ),

          _.div({ className: 'project-header' },
            AutoInput({ className: 'title', ref: 'name', binding: name, onChange: Morearty.Callback.set(name), placeholder: tl('component.cv.placeholder.project.name') }),
            _.div({ className: 'company' },
              _.span({ className: 'label label_bold' }, tl('component.cv.project.company')),
              AutoInput({ binding: company, onChange: Morearty.Callback.set(company), placeholder: tl('component.cv.placeholder.project.company') })
            )
          ),

          _.ul(null,
            _.li(null,
              _.span({ className: 'label label_bold' }, tl('component.cv.project.description')),
              AutoTextarea({ className: 'p', binding: description, onChange: Morearty.Callback.set(description), placeholder: tl('component.cv.placeholder.project.description') })
            ),
            _.li(null,
              _.span({ className: 'label label_bold' }, tl('component.cv.project.position')),
              AutoInput({ binding: position, onChange: Morearty.Callback.set(position), placeholder: tl('component.cv.placeholder.project.position') })
            ),
            _.li(null,
              _.div({ className: 'tags-wrapper' },
                _.span({ className: 'label label_bold' }, tl('component.cv.project.tags')),
                TagsList({ binding: tags, placeholder: tl('component.cv.placeholder.project.tag') })
              )
            ),
            _.li(null,
              _.span({ className: 'label label_bold' }, tl('component.cv.project.responsibilities')),
              AutoTextarea({ className: 'p', binding: responsibilities, onChange: Morearty.Callback.set(responsibilities), placeholder: tl('component.cv.placeholder.project.responsibilities') })
            )
          )

        );
      }

    });

  });
