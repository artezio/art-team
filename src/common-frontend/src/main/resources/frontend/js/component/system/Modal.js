define(['react', 'morearty', 'immutable', 'service/I18NSvc', 'service/NotificationSvc'], function (React, Morearty, Imm, I18NSvc, NotificationSvc) {
  'use strict';

  var t = I18NSvc.t;

  var onClick = function (confirmed) {
    NotificationSvc.onModal(confirmed);
  };

  return React.createClass({

    displayName: 'Modal',
    mixins: [Morearty.Mixin],

    getDefaultState: function () {
      return Imm.Map({
        visible: false
      });
    },

    componentDidUpdate: function () {
      var ctx = this.getMoreartyContext();
      var visibleBinding = this.getDefaultBinding().sub('visible');
      if (visibleBinding.get() && ctx.isChanged(visibleBinding)) {
        this.refs.ok.getDOMNode().focus();
      }
    },

    render: function () {
      var binding = this.getDefaultBinding(), value = binding.get();
      var visible = value.get('visible');

      var _ = React.DOM;
      if (visible) {
        var title = value.get('title'), text = value.get('text');

        var onConfirm = onClick.bind(this, true), onCancel = onClick.bind(this, false);

        return _.div({ className: 'shadow-base' },
          _.div({ className: 'modal' },
          _.div({ className: 'modal__header' }, title),
          _.div({ className: 'modal__body' }, null, text),
          _.div({ className: 'modal__footer' },
            _.ul({ className: 'button_multi', onKeyDown: Morearty.Callback.onEscape(onCancel) } ,
              _.li({ className: 'button button_cancel', tabIndex: 0, onClick: onCancel, onKeyDown: Morearty.Callback.onEnter(onCancel) }, t('component.modal.cancel')),
              _.li({ ref: 'ok', className: 'button', tabIndex: 0, onClick: onConfirm, onKeyDown: Morearty.Callback.onEnter(onConfirm) }, t('component.modal.ok'))
            )
          )
        ));
      } else {
        return null;
      }
    }

  });

});
