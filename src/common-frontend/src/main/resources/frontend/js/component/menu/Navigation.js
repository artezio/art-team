define(['react', 'morearty', 'app/Paths', 'session/SessionStorage', 'service/I18NSvc', 'service/SecuritySvc', 'c!component/menu/MenuLink'],
  function (React, Morearty, Paths, SessionStorage, I18NSvc, SecuritySvc, MenuLink) {
    'use strict';

    var t = I18NSvc.t;

    return React.createClass({

      displayName: 'Navigation',
      mixins: [Morearty.Mixin],

      render: function () {
        var username = SessionStorage.getUsername();
        var role = t('component.common.role.' + SessionStorage.getRole());

        var _ = React.DOM;
        return _.ul({ className: 'links' }, null,
          MenuLink({ href: Paths.getCVPath(username), icon: 'cv-page', label: username, title: role, disabled: !(SecuritySvc.isUser() || SecuritySvc.isManager() || SecuritySvc.isAdmin()) }),
          SecuritySvc.canReadOtherPeopleCV() ? MenuLink({ href: Paths.getCVListPath(), icon: 'cvs', label: t('component.sideMenu.allCVs') }) : null,
          SecuritySvc.canReadOtherPeopleCV() ? MenuLink({ href: Paths.getPickUpPath(), icon: 'pickup', label: t('component.sideMenu.pickUp') }) : null,

          MenuLink({ className: 'prefs', href: Paths.getPreferencesPath(), icon: 'prefs', label: t('component.sideMenu.preferences') }),
          MenuLink({ className: 'logout', href: Paths.getLogoutPath(), label: t('component.sideMenu.logout') })
        )
      }

    });

  });
