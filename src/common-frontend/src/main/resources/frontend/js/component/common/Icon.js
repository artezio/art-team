define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  return React.createClass({

    displayName: 'Icon',
    mixins: [Morearty.Mixin],

    render: function () {
      var _ = React.DOM;
      var type = this.props.type ? ' ' + this.props.type : '';
      var className = 'icon' + type;
      return _.i({ className: className });
    }

  });

});
