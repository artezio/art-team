define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  var _ = React.DOM;

  var createNotificationItem = function (notification) {
    var type = notification.get('type');
    var fadingOut = notification.get('fadingOut');
    var liClass = React.addons.classSet({
      notify__item: true,
      notify__item_info: type === 'INFO',
      notify__item_error: type === 'ERROR',
      '_fade-in': !fadingOut,
      '_fade-out': fadingOut
    });
    return _.li({ key: notification.get('created'), className: liClass },
      _.div({ className: 'notify__body' },
        _.span({ className: 'notify__body__text' }, notification.get('message'))
      )
    );
  };

  return React.createClass({

    displayName: 'Notification',
    mixins: [Morearty.Mixin],

    render: function () {
      var binding = this.getDefaultBinding(), notifications = binding.get();

      return _.ul({ className: 'notify' },
        notifications && notifications.map(function (notification) {
          return createNotificationItem(notification);
        }).toArray()
      );
    }

  });

});
