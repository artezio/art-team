define(['react', 'morearty', 'util/CommonUtil', 'service/I18NSvc', 'c!component/common/HighlightedText', 'c!component/cv/row/TagsListView'],
  function (React, Morearty, CommonUtil, I18NSvc, HighlightedText, TagsList) {
    'use strict';

    return React.createClass({

      displayName: 'ProjectRowView',
      mixins: [Morearty.Mixin],

      shouldComponentUpdateOverride: function (shouldComponentUpdate, nextProps) {
        return this.props.filter !== nextProps.filter || shouldComponentUpdate();
      },

      render: function () {
        var binding = this.getDefaultBinding();
        var startDate = binding.sub('startDate');
        var endDate = binding.sub('endDate');

        var filter = this.props.filter;

        var tl = I18NSvc.forLang(this.props.language);

        var _ = React.DOM;
        return _.div({ className: 'item' },

          _.span({ className: 'date' },
            _.span({ className: 'from' },
              _.span({ className: 'label label_bold' }, tl('component.cv.project.startDate')),
              _.span(null, startDate.get('year')), '.', _.span(null, CommonUtil.padNumber(startDate.get('month'), 2))
            ),
            _.span({ className: 'to' },
              _.span({ className: 'label label_bold' },tl('component.cv.project.endDate')),
              _.span(null, endDate.get('year')), '.', _.span(null, CommonUtil.padNumber(endDate.get('month'), 2))
            )
          ),

          _.div({ className: 'project-header' },
            HighlightedText({ className: 'title', text: binding.get('name'), pattern: filter }),
            _.div({ className: 'company' },
              _.span({ className: 'label label_bold' }, tl('component.cv.project.company')),
              _.span(null, binding.get('company'))
            )
          ),

          _.ul(null,
            _.li(null,
              _.span({ className: 'label label_bold' }, tl('component.cv.project.description')),
              _.p({ className: 'p' }, binding.get('description'))
            ),
            _.li(null,
              _.span({ className: 'label label_bold' }, tl('component.cv.project.position')),
              _.span(null, binding.get('position'))
            ),
            _.li(null,
              _.div({ className: 'tags-wrapper' },
                _.span({ className: 'label label_bold' }, tl('component.cv.project.tags')),
                TagsList({ binding: binding.sub('tags'), filter: filter })
              )
            ),
            _.li(null,
              _.span({ className: 'label label_bold' }, tl('component.cv.project.responsibilities')),
              _.p({ className: 'p' }, binding.get('responsibilities'))
            )
          )

        );
      }

    });

  });
