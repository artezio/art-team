define(['react', 'morearty', 'service/I18NSvc', 'service/CVSvc', 'c!component/common/AutoInput'],
  function (React, Morearty, I18NSvc, CVSvc, AutoInput) {
    'use strict';

    var option = React.createFactory(Morearty.DOM.option);

    return React.createClass({

      displayName: 'CertificateRow',
      mixins: [Morearty.Mixin],

      render: function () {
        var self = this;
        var binding = self.getDefaultBinding();
        var description = binding.sub('description');
        var year = binding.sub('year');

        var tl = I18NSvc.forLang(this.props.language);

        var stringToIntTransformer = function (yearAsString) { return parseInt(yearAsString); };
        var availableYears = CVSvc.getAvailableYears();

        var _ = React.DOM;
        return _.span(null,
          _.select({ value: year.get(), onChange: Morearty.Callback.set(year, stringToIntTransformer) },
            availableYears.map(function (year, index) {
              return option({ key: index, value: year.value}, year.name);
            })
          ),
          _.span({ className: 'label left' },
            AutoInput({ binding: description, onChange: Morearty.Callback.set(description), placeholder: tl('component.cv.placeholder.certificate'), autofocus: self.props.autofocus })
          )

        );
      }

    });

  });
