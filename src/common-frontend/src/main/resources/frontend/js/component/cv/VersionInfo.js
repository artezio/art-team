define(['react', 'morearty', 'util/CommonUtil', 'service/I18NSvc'], function (React, Morearty, CommonUtil, I18NSvc) {

  return React.createClass({

    displayName: 'VersionInfo',
    mixins: [Morearty.Mixin],

    render: function () {
      var binding = this.getDefaultBinding();
      var language = this.getBinding('language').get();

      var tl = I18NSvc.forLang(language);

      var version = binding.get('data.version'), draft = binding.get('draft');
      var lastModified = binding.get('data.lastModified'), lastModifiedBy = binding.get('data.lastModifiedBy');

      var _ = React.DOM;
      return _.span({ className: 'cv-version-info' },
        _.div(null, tl('component.cv.version') + tl('common.colon') + ' ' + ((draft ? version + 1 : version) || tl('component.cv.na')) + (draft ? ' (' + tl('component.cv.draft') + ')' : '')),
        _.div(null, tl('component.cv.lastModified') + tl('common.colon') + ' ' + (lastModified ? CommonUtil.formatDate(lastModified, language) : tl('component.cv.never'))),
        _.div(null, tl('component.cv.lastModifiedBy') + tl('common.colon') + ' ' + (lastModifiedBy || tl('component.cv.na')))
      );
    }

  });

});
