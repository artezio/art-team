define(['react', 'morearty', 'util/CommonUtil'], function (React, Morearty, CommonUtil) {
  'use strict';

  var input = React.createFactory(Morearty.DOM.input);

  var MIN_WIDTH, setWidth, focusIfNeeded, onDidMountOrUpdate;

  MIN_WIDTH = 16;

  setWidth = function () {
    var input = this.refs.input.getDOMNode();
    var text = input.value || input.placeholder;

    var width;
    if (text) {
      var calculatedWidth = CommonUtil.calculateWidth(input, text);
      width = Math.max(calculatedWidth, this.props.minWidth || MIN_WIDTH);
    } else {
      width = MIN_WIDTH;
    }

    input.style.width = width + 'px';
  };

  focusIfNeeded = function () {
    if (this.props.autofocus) {
      var input = this.refs.input.getDOMNode();
      if (input !== document.activeElement) {
        input.focus();
        input.setSelectionRange(0, 0);
      }
    }
  };

  onDidMountOrUpdate = function () {
    setWidth.call(this);
    focusIfNeeded.call(this);
  };

  return React.createClass({

    displayName: 'AutoInput',
    mixins: [Morearty.Mixin],

    componentDidMount: function () {
      onDidMountOrUpdate.call(this);
    },

    componentDidUpdate: function () {
      onDidMountOrUpdate.call(this);
    },

    shouldComponentUpdateOverride: function (shouldComponentUpdate, nextProps) {
      return shouldComponentUpdate() ||
        (this.props && nextProps && this.props.placeholder !== nextProps.placeholder);
    },

    render: function () {
      var binding = this.getDefaultBinding();

      var props = {};
      CommonUtil.shallowMerge(this.props, props);
      props.value = binding.get();
      props.ref = 'input';

      props.onBlur = function () {
        if (props.destroyOnEmpty && !binding.get()) {
          binding.delete();
        }
      }.bind(this);

      return input(props);
    }

  });

});
