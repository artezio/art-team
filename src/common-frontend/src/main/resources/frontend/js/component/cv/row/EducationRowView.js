define(['react', 'morearty', 'service/I18NSvc'], function (React, Morearty, I18NSvc) {
  'use strict';

  var t = I18NSvc.t;

  return React.createClass({

    displayName: 'EducationRowView',
    mixins: [Morearty.Mixin],

    render: function () {
      var binding = this.getDefaultBinding();
      var _ = React.DOM;
      return _.span(null,
        _.span(null, binding.get('startYear')),
        _.span({ className: 'dash' }, t('common.endash')),
        _.span(null, binding.get('endYear')),
        _.span({ className: 'label left' },
          _.span(null, binding.get('institution'))
        )
      );
    }

  });

});
