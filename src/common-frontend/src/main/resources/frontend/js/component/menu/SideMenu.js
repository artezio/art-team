define(['react', 'morearty', 'c!component/menu/Navigation', 'c!component/menu/Languages'],
  function (React, Morearty, Navigation, Languages) {
    'use strict';

    return React.createClass({

      displayName: 'SideMenu',
      mixins: [Morearty.Mixin],

      shouldComponentUpdateOverride: function () {
        return true;
      },

      render: function () {
        var language = this.getDefaultBinding();

        var _ = React.DOM;
        return _.div({ className: 'side-menu-wrapper' },
          _.ul({ className: 'side-menu' },
            _.li(null, _.div({ className: 'icon logo' })),
            _.li(null, Navigation()),
            _.li(null, Languages({ binding: language })),
            _.li({ className: 'fill' },
              this.props.main ? this.props.main : null
            ),
            _.li(null,
              _.ul(null,
                this.props.actions
              )
            )
          )
        );
      }

    });

  });
