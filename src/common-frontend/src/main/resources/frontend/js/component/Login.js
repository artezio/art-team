define(
  ['react', 'morearty', 'immutable', 'app/Router', 'util/CommonUtil', 'service/I18NSvc', 'service/SharedDataSvc', 'service/PrefsSvc', 'session/SessionStorage', 'service/SessionSvc'],
  function (React, Morearty, Imm, Router, CommonUtil, I18NSvc, SharedDataSvc, PrefsSvc, SessionStorage, SessionSvc) {
    'use strict';

    var input = React.createFactory(Morearty.DOM.input);

    var t = I18NSvc.t;

    var getDefaultState, resetConfig;

    getDefaultState = function (username) {
      return Imm.Map({
        username: username || '',
        password: '',
        loginFailed: false,
        responseStatus: null
      });
    };

    resetConfig = function (notify) {
      this.getDefaultBinding().atomically().set(getDefaultState()).commit({ notify: notify });
    };

    var focusRef = function (ref) {
      this.refs[ref].getDOMNode().focus();
    };

    var canSubmit, login, getErrorMessage;

    canSubmit = function (self) {
      var binding = self.getDefaultBinding();
      return binding.get('username') && binding.get('password');
    };

    login = function (self) {
      var binding = self.getDefaultBinding();
      var username = binding.get('username'), password = binding.get('password');

      SessionSvc.login(username, password)
        .done(function () {
          SharedDataSvc.fetchSharedData().done(function () {
            var lastPageSet = Router.toLastVisitedPage();
            PrefsSvc.init(!lastPageSet);
            I18NSvc.init();
          });
        })
        .fail(function (xhr) {
          binding.atomically()
            .set('password', '')
            .set('loginFailed', true)
            .set('responseStatus', xhr.status)
            .commit();
        });
    };

    getErrorMessage = function (self) {
      var errorCode = self.getDefaultBinding().get('responseStatus') === 400 ?
        'component.login.badCredentials' :
        'component.login.loginFailed';
      return t(errorCode);
    };

    return React.createClass({

      displayName: 'Login',
      mixins: [Morearty.Mixin],

      getMergeStrategy: function () { return Morearty.MergeStrategy.OVERWRITE; },

      getDefaultState: function () {
        return getDefaultState(SessionStorage.getUsername());
      },

      componentDidMount: function () {
        var username = this.getDefaultBinding().get('username');
        if (!username) {
          focusRef.call(this, 'username');
        } else {
          focusRef.call(this, 'password');
        }
      },

      componentDidUpdate: function () {
        var focusUsernameInput = CommonUtil.getTransientStateOnce(this).focusUsernameInput;
        if (focusUsernameInput) {
          focusRef.call(this, 'username');
        }
      },

      componentWillUnmount: function () {
        resetConfig.call(this, false);
      },

      onLogin: function () {
        if (canSubmit(this)) {
          login(this);
        }
      },

      onUserSwitch: function (event) {
        event.preventDefault();
        SessionStorage.clearSessionInfo();
        resetConfig.call(this, true);
        CommonUtil.updateTransientState(this, function (ts) {
          ts.focusUsernameInput = true;
        });
      },

      render: function () {
        var binding = this.getDefaultBinding();
        var username = binding.get('username');

        var _ = React.DOM;

        return _.div({ className: 'wrapper' },

          _.div({ className: 'login' },
            _.div({ className: 'icon logo login-logo' }),

            _.div({ className: 'modal' },
              _.div({ className: 'body' },
                binding.get('loginFailed') ? _.span({ className: 'login-error' }, getErrorMessage(this)) : null,
                SessionStorage.getUsername() ?
                  _.div(null,
                    _.span({ className: 'login-username' }, username),
                    _.a({ href: '#', onClick: this.onUserSwitch }, t('component.login.switchUser'))
                  ) :
                  input({
                    id: 'username',
                    ref: 'username',
                    value: username,
                    onChange: Morearty.Callback.set(binding, 'username'),
                    placeholder: t('component.login.placeholder.username')
                  }),
                input({
                  id: 'password',
                  ref: 'password',
                  type: 'password',
                  value: binding.get('password'),
                  onChange: Morearty.Callback.set(binding, 'password'),
                  onKeyDown: Morearty.Callback.onEnter(this.onLogin),
                  placeholder: t('component.login.placeholder.password')
                }),
                _.button({ className: 'button', disabled: !canSubmit(this), onClick: this.onLogin }, t('component.login.login'))
              )
            )
          )
        );
      }

    })
  });
