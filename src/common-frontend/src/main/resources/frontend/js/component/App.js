define([
    'react',
    'morearty',
    'app/NowShowing',
    'service/SessionSvc',
    'c!component/system/error/PermissionViolation',
    'c!component/system/error/NotFound',
    'c!component/system/Notification',
    'c!component/system/Modal',
    'c!component/common/Loading',
    'c!component/Login',
    'c!component/Logout',
    'c!component/Sections'
  ],
  function (React, Morearty, NowShowing, SessionSvc, PermissionViolation, NotFound, Notification, Modal, Loading, Login, Logout, Sections) {
    'use strict';

    return React.createClass({

      displayName: 'App',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();
        var nowShowing = binding.get('nowShowing');

        var content = (function () {
          if (nowShowing) {
            switch (nowShowing) {
              case NowShowing.PERMISSION_VIOLATION:
                return PermissionViolation();
              case NowShowing.NOT_FOUND:
                return NotFound();
              case NowShowing.LOGIN:
                return Login({ binding: binding.sub('login') });
              case NowShowing.LOGOUT:
                return Logout();
              default:
                return Sections({ binding: { default: binding.sub('sections'), prefs: binding.sub('prefs') }, nowShowing: nowShowing });
            }
          } else {
            return null;
          }
        })();

        return React.DOM.div({ className: 'wrapper' },
          Notification({ binding: binding.sub('notifications') }),
          Modal({ binding: binding.sub('modal') }),
          Loading({ binding: binding.sub('loading') }),
          content
        );
      }

    });

  });
