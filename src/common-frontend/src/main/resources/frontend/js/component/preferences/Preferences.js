define(['react', 'morearty', 'service/I18NSvc', 'service/SecuritySvc', 'service/NavigationSvc', 'c!component/common/LocationsFilter'],
  function (React, Morearty, I18NSvc, SecuritySvc, NavigationSvc, LocationsFilter) {
    'use strict';

    var t = I18NSvc.t;

    var onChangeLanguage, onChangeDefaultSection;

    onChangeLanguage = function (language) {
      this.getMoreartyContext().queueFullUpdate();
      I18NSvc.setLanguage(language);
    };

    onChangeDefaultSection = function (section) {
      this.getBinding('prefs').set('defaultSection', section);
    };

    return React.createClass({

      displayName: 'Preferences',
      mixins: [Morearty.Mixin],

      render: function () {
        var prefsBinding = this.getBinding('prefs'), languageBinding = this.getBinding('language');
        var interfaceLanguage = I18NSvc.getLanguage();
        var defaultSection = prefsBinding.get('defaultSection') || NavigationSvc.getDefaultSection();

        var canReadOtherPeopleCV = SecuritySvc.canReadOtherPeopleCV();

        var _ = React.DOM;
        return _.div({ className: 'wrapper content' },
          _.div({ className: 'column center' },

            _.div({ className: 'block preferences' },
              _.h2(null, t('component.prefs.language')),
              _.hr(),
              _.span({ className: 'app-radio' },
                _.label({ className: 'app-radio__label', htmlFor: 'language-en'},
                  _.input({ className: 'app-radio__control', type: 'radio', id: 'language-en', value: 'en', checked: interfaceLanguage === 'en', onChange: onChangeLanguage.bind(this, 'en') }),
                  _.span({ className: 'app-radio__text'},
                    t('component.common.language.english')
                  )
                )
              ),
              _.span({ className: 'app-radio' },
                _.label({ className: 'app-radio__label', htmlFor: 'language-ru'},
                  _.input({ className: 'app-radio__control', type: 'radio', id: 'language-ru', value: 'en', checked: interfaceLanguage === 'ru', onChange: onChangeLanguage.bind(this, 'ru') }),
                  _.span({ className: 'app-radio__text'},
                    t('component.common.language.russian')
                  )
                )
              )
            ),

            canReadOtherPeopleCV ? _.div({ className: 'block preferences' },
              _.h2(null, t('component.prefs.defaultSection')),
              _.hr(),
              SecuritySvc.hasCV() ? _.span({ className: 'app-radio' },
                _.label({ className: 'app-radio__label', htmlFor: 'section-cv'},
                  _.input({ className: 'app-radio__control', type: 'radio', id: 'section-cv', value: 'cv', checked: defaultSection === 'cv', onChange: onChangeDefaultSection.bind(this, 'cv') }),
                  _.span({ className: 'app-radio__text'},
                    t('component.prefs.section.cv')
                  )
                )
              ) : null,
              _.span({ className: 'app-radio' },
                _.label({ className: 'app-radio__label', htmlFor: 'section-list'},
                  _.input({ className: 'app-radio__control', type: 'radio', id: 'section-list', value: 'list', checked: defaultSection === 'list', onChange: onChangeDefaultSection.bind(this, 'list') }),
                  _.span({ className: 'app-radio__text'},
                    t('component.prefs.section.list')
                  )
                )
              ),
              _.span({ className: 'app-radio' },
                _.label({ className: 'app-radio__label', htmlFor: 'section-pickup'},
                  _.input({ className: 'app-radio__control', type: 'radio', id: 'section-pickup', value: 'pickup', checked: defaultSection === 'pickup', onChange: onChangeDefaultSection.bind(this, 'pickup') }),
                  _.span({ className: 'app-radio__text'},
                    t('component.prefs.section.pickUp')
                  )
                )
              )
            ) : null,

            canReadOtherPeopleCV ? _.div({ className: 'block preferences' },
              _.h2(null, t('component.prefs.locations')),
              _.hr(),
              LocationsFilter({ binding: { default: prefsBinding.sub('selectedLocations'), language: languageBinding } })
            ) : null

          )
        );
      }

    });

  });
