define(['react', 'morearty'],
  function (React, Morearty) {
    'use strict';

    return React.createClass({

      displayName: 'CertificateRowView',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();
        var _ = React.DOM;
        return _.span(null,
          _.span(null, binding.get('year')),
          _.span({ className: 'label left' },
            _.span(null, binding.get('description'))
          )
        );
      }

    });

  });
