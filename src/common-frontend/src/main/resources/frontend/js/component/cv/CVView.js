define([
    'react',
    'morearty',
    'util/CommonUtil',
    'service/I18NSvc',
    'service/SharedDataSvc',
    'c!component/common/HighlightedText',
    'c!component/common/EditableListView',
    'c!component/cv/VersionInfo',
    'c!component/cv/row/ExpertiseRowView',
    'c!component/cv/row/EducationRowView',
    'c!component/cv/row/CertificateRowView',
    'c!component/cv/row/LanguageRowView',
    'c!component/cv/row/ProjectRowView'
  ],
  function (React, Morearty, CommonUtil, I18NSvc, SharedDataSvc, HighlightedText, EditableList, VersionInfo, ExpertiseRow, EducationRow, CertificateRow, LanguageRow, ProjectRow) {
    'use strict';

    var applyLanguage = function (comp, language) {
      return CommonUtil.papply(comp, { language: language });
    };

    return React.createClass({

      displayName: 'CVView',
      mixins: [Morearty.Mixin],

      render: function () {
        var defaultBinding = this.getDefaultBinding(),
        languageBinding = this.getBinding('language'),
        filterBinding = this.getBinding('filter');

        var language = languageBinding.get();
        var tl = I18NSvc.forLang(language);

        var filter = filterBinding && filterBinding.get();
        var Hl = CommonUtil.papply(HighlightedText, { pattern: filter });

        var cv = defaultBinding.sub('data.cv');
        var firstname = cv.get('name.firstname');
        var lastname = cv.get('name.lastname');
        var position = cv.get('position');
        var description = cv.get('description');

        var location = cv.get('location');
        var locationDescription = SharedDataSvc.getLocationDescription(location, language);

        var summary = cv.sub('summary');
        var expertise = cv.sub('expertise');
        var education = cv.sub('education');
        var certificates = cv.sub('certificates');
        var languages = cv.sub('languages');
        var additionalInfo = cv.sub('additionalInfo');
        var projects = cv.sub('projects');

        var _ = React.DOM;
        return _.div({ className: 'wrapper content' },
          _.div({ className: 'column center' },

            VersionInfo({ binding: this.getBinding() }),

            _.div({ className: 'block' },
              _.span(null,
                _.h1(null, Hl({ text: firstname || tl('component.cv.placeholder.firstname') })), _.h1(null, Hl({ text: lastname || tl('component.cv.placeholder.lastname') }))
              ),
              _.div(null,
                _.span({ className: 'office' },
                  _.span({ className: 'label' }, tl('component.cv.location')),
                  locationDescription
                ),
                _.h3(null, Hl({ text: position }))
              ),
              _.div(null,
                _.p(null, description)
              )
            ),

            summary.get().count() ? _.div({ className: 'block' },
              EditableList({
                binding: summary,
                icon: 'experience',
                title: tl('component.cv.summary')
              })
            ) : null,

            expertise.get().count() ? _.div({ className: 'block' },
              EditableList({
                binding: { default: expertise, filter: filterBinding },
                icon: 'techs',
                title: tl('component.cv.expertise'),
                rowComponent: CommonUtil.papply(ExpertiseRow, { filter: filter })
              })
            ) : null,
            education.get().count() ? _.div({ className: 'block' },
              EditableList({
                binding: education,
                icon: 'education',
                title: tl('component.cv.education'),
                rowComponent: EducationRow
              })
            ) : null,
            certificates.get().count() ? _.div({ className: 'block' },
              EditableList({
                binding: certificates,
                icon: 'certificates',
                title: tl('component.cv.certificates'),
                rowComponent: CertificateRow
              })
            ) : null,
            languages.get().count() ? _.div({ className: 'block' },
              EditableList({
                binding: languages,
                icon: 'languages',
                title: tl('component.cv.languages'),
                rowComponent: applyLanguage(LanguageRow, language)
              })
            ) : null,
            additionalInfo.get().count() ? _.div({ className: 'block' },
              EditableList({
                binding: additionalInfo,
                icon: 'additionalInfo',
                title: tl('component.cv.additionalInfo')
              })
            ) : null,

            projects.get().count() ? _.div({ className: 'projects block' },
              EditableList({
                binding: { default: projects, filter: filterBinding },
                icon: 'projects-tie',
                title: tl('component.cv.projects'),
                rowComponent: CommonUtil.papply(ProjectRow, { filter: filter, language: language })
              })
            ) : null
          )
        );
      }

    });

  });
