define(['react', 'morearty', 'c!component/common/Link'], function (React, Morearty, Link) {
  'use strict';

  return React.createClass({

    displayName: 'MenuLink',
    mixins: [Morearty.Mixin],

    render: function () {
      var _ = React.DOM;
      return _.li({ className: this.props.className },
        Link(this.props)
      );
    }

  });

});
