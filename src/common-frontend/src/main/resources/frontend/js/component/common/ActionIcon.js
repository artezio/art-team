define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  var onClick = function (event) {
    event.preventDefault();
    this.props.onClick();
  };

  return React.createClass({

    displayName: 'ActionIcon',
    mixins: [Morearty.Mixin],

    render: function () {
      var _ = React.DOM;
      var type = this.props.type ? ' ' + this.props.type : '';

      var className = React.addons.classSet({
        action: true,
        icon: true,
        disabled: this.props.disabled
      }) + type;

      return _.a({ href: '#', className: className, onClick: onClick.bind(this), title: this.props.title });
    }

  });

});
