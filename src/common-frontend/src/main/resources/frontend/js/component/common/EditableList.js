define(['react', 'morearty', 'immutable', 'service/I18NSvc', 'util/CommonUtil', 'c!component/common/Icon', 'c!component/common/ActionIcon'],
  function (React, Morearty, Imm, I18NSvc, CommonUtil, Icon, ActionIcon) {
    'use strict';

    var input = React.createFactory(Morearty.DOM.input);

    var t = I18NSvc.t;

    var onDelete, swapElements, onMoveUp, onMoveDown, onAdd;

    onDelete = function (binding) {
      binding.delete();
    };

    swapElements = function (binding, index1, index2) {
      binding.update(function (vec) {
        var arr = vec.toArray();
        var value1 = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = value1;
        return Imm.fromJS(arr);
      });
    };

    onMoveUp = function (binding, index) {
      return swapElements(binding, index - 1, index);
    };

    onMoveDown = function (binding, index) {
      return swapElements(binding, index, index + 1);
    };

    onAdd = function (index) {
      var binding = this.getDefaultBinding();
      var effectiveIndex = Morearty.Util.undefinedOrNull(index) ? (this.props.addFirst ? 0 : binding.get().count()) : index;
      binding.update(function (list) {
        return list.splice(effectiveIndex, 0, this.props.rowTemplate || '');
      }.bind(this));
      CommonUtil.updateTransientState(this, function (ts) {
        ts.focusedRowIndex = effectiveIndex;
      });
    };

    var getRowComponent = function (self, index) {
      var binding = self.getDefaultBinding();
      return CommonUtil.papply(
          self.props.rowComponent || input, {
          destroyOnEmpty: true,
          onKeyDown: Morearty.Callback.onEnter(function (event) {
            if (event.target.value) {
              onAdd.call(self, index + 1);
            }
          }),
          onKeyUp: Morearty.Callback.onEscape(Morearty.Callback.delete(binding, index, Morearty.Util.not))
        },
        false
      );
    };

    return React.createClass({

      displayName: 'EditableList',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();

        var focusedRowIndex = CommonUtil.getTransientStateOnce(this).focusedRowIndex;

        var value = binding.get() || Imm.List.of();
        var lastIndex = value.count() - 1;

        var _ = React.DOM;
        var mkRow = function (rowValue, index) {
          var rowBinding = binding.sub(index);
          var rowComponent = getRowComponent(this, index);
          var autofocus = !Morearty.Util.undefinedOrNull(focusedRowIndex) && focusedRowIndex === index;
          return _.li({ key: index },
            rowComponent({ value: rowValue, binding: rowBinding, onChange: Morearty.Callback.set(rowBinding), autofocus: autofocus }),
            _.span({ className: 'editable-list-actions' },
              index !== 0 ? ActionIcon({ type: 'up-arrow', onClick: onMoveUp.bind(null, binding, index), title: t('component.icons.up') }) : null,
              index !== lastIndex ? ActionIcon({ type: 'down-arrow', onClick: onMoveDown.bind(null, binding, index), title: t('component.icons.down') }) : null,
              ActionIcon({ type: 'remove', onClick: onDelete.bind(null, rowBinding), title: t('component.icons.remove') })
            )
          );
        }.bind(this);

        return _.div({ className: 'editable-list' },
          _.div(null,
            Icon({ type: this.props.icon }),
            _.h2(null, this.props.title),
            _.span({ className: 'editable-list-actions-top' },
              ActionIcon({ type: 'add', onClick: onAdd.bind(this), title: t('component.icons.add') })
            )
          ),
          _.hr(),
          _.ul(null,
            value.map(mkRow).toArray()
          )
        );
      }

    });

  });
