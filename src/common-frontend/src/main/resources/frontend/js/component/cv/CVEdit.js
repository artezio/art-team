define([
    'react',
    'morearty',
    'immutable',
    'util/CommonUtil',
    'service/I18NSvc',
    'service/CVSvc',
    'service/SharedDataSvc',
    'c!component/common/AutoInput',
    'c!component/common/AutoTextarea',
    'c!component/common/EditableList',
    'c!component/cv/VersionInfo',
    'c!component/cv/row/ExpertiseRow',
    'c!component/cv/row/ProjectRow',
    'c!component/cv/row/EducationRow',
    'c!component/cv/row/CertificateRow',
    'c!component/cv/row/LanguageRow'
  ],
  function (React, Morearty, Imm, CommonUtil, I18NSvc, CVSvc, SharedDataSvc, AutoInput, AutoTextarea, EditableList, VersionInfo, ExpertiseRow, ProjectRow, EducationRow, CertificateRow, LanguageRow) {
    'use strict';

    var option = React.createFactory(Morearty.DOM.option);

    var applyLanguage = function (comp, language) {
      return CommonUtil.papply(comp, { language: language });
    };

    return React.createClass({

      displayName: 'CVEdit',
      mixins: [Morearty.Mixin],

      render: function () {
        var defaultBinding = this.getDefaultBinding(), languageBinding = this.getBinding('language');

        var language = languageBinding.get();
        var tl = I18NSvc.forLang(language);

        var cv = defaultBinding.sub('data.cv');
        var firstname = cv.sub('name.firstname');
        var lastname = cv.sub('name.lastname');
        var position = cv.sub('position');
        var description = cv.sub('description');

        var location = cv.sub('location');
        var locations = SharedDataSvc.getLocations(language);

        var defaultYear = CVSvc.getDefaultYear(), defaultMonth = CVSvc.getDefaultMonth();

        var _ = React.DOM;
        return _.div({ className: 'wrapper content' },
          _.div({ className: 'column center' },

            VersionInfo({ binding: this.getBinding() }),

            _.div({ className: 'block' },
              _.span(null,
                AutoInput({ className: 'h1', binding: firstname, onChange: Morearty.Callback.set(firstname), placeholder: tl('component.cv.placeholder.firstname') }),
                AutoInput({ className: 'h1', binding: lastname, onChange: Morearty.Callback.set(lastname), placeholder: tl('component.cv.placeholder.lastname') })
              ),
              _.div(null,
                AutoInput({ className: 'h3', binding: position, onChange: Morearty.Callback.set(position), placeholder: tl('component.cv.placeholder.position') }),
                _.span({ className: 'office' },
                  _.span({ className: 'label' }, tl('component.cv.location')),
                  _.select({ value: location.get(), onChange: Morearty.Callback.set(location) },
                    locations.map(function (loc, index) {
                      return option({ key: index, value: loc.get('code') }, loc.getIn(['descriptions', language]));
                    }).toArray()
                  )
                )
              ),
              _.div(null,
                AutoTextarea({ className: 'p', binding: description, onChange: Morearty.Callback.set(description), placeholder: tl('component.cv.placeholder.description') })
              )
            ),

            _.div({ className: 'block' },
              EditableList({
                binding: cv.sub('summary'),
                icon: 'experience',
                title: tl('component.cv.summary'),
                rowComponent: CommonUtil.papply(AutoInput, { placeholder: tl('component.cv.placeholder.summary') })
              })
            ),

            _.div({ className: 'block' },
              EditableList({
                binding: cv.sub('expertise'),
                icon: 'techs',
                title: tl('component.cv.expertise'),
                rowComponent: applyLanguage(ExpertiseRow, language),
                rowTemplate: Imm.fromJS({
                  area: '',
                  tags: []
                })
              })
            ),
            _.div({ className: 'block' },
              EditableList({
                binding: cv.sub('education'),
                icon: 'education',
                title: tl('component.cv.education'),
                rowComponent: applyLanguage(EducationRow, language),
                rowTemplate: Imm.fromJS({
                  institution: '', startYear: defaultYear, endYear: defaultYear
                })
              })
            ),
            _.div({ className: 'block' },
              EditableList({
                binding: cv.sub('certificates'),
                icon: 'certificates',
                title: tl('component.cv.certificates'),
                rowComponent: applyLanguage(CertificateRow, language),
                rowTemplate: Imm.fromJS({
                  description: '', year: defaultYear
                })
              })
            ),
            _.div({ className: 'block' },
              EditableList({
                binding: cv.sub('languages'),
                icon: 'languages',
                title: tl('component.cv.languages'),
                rowComponent: applyLanguage(LanguageRow, language),
                rowTemplate: Imm.fromJS({
                  language: tl('component.cv.templates.language'), proficiency: CVSvc.getDefaultProficiency()
                })
              })
            ),
            _.div({ className: 'block' },
              EditableList({
                binding: cv.sub('additionalInfo'),
                icon: 'additionalInfo',
                title: tl('component.cv.additionalInfo'),
                rowComponent: CommonUtil.papply(AutoInput, { placeholder: tl('component.cv.placeholder.additionalInfo') })
              })
            ),
            _.div({ className: 'projects block' },
              EditableList({
                binding: cv.sub('projects'),
                icon: 'projects-tie',
                title: tl('component.cv.projects'),
                rowComponent: applyLanguage(ProjectRow, language),
                rowTemplate: Imm.fromJS({
                  startDate: { year: defaultYear, month: defaultMonth },
                  endDate: { year: defaultYear, month: defaultMonth },
                  name: '',
                  company: '',
                  description: '',
                  position: '',
                  responsibilities: '',
                  tags: []
                }),
                addFirst: true
              })
            )
          )
        );
      }

    });

  });
