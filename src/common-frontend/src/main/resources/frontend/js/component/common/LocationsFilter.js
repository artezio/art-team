define(['react', 'morearty', 'service/SharedDataSvc', 'c!component/common/Checkbox'],
  function (React, Morearty, SharedDataSvc, Checkbox) {
    'use strict';

    return React.createClass({

      displayName: 'LocationsFilter',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();
        var languageBinding = this.getBinding('language'), language = languageBinding && languageBinding.get();

        var _ = React.DOM;
        return _.div({ className: 'locations-pane' },
          _.ul({ className: 'fill-grid' },
            SharedDataSvc.getLocations(language).map(function (location) {
              var code = location.get('code');

              return _.li({ key: location, className: 'fill-grid_auto' },
                Checkbox({ key: code + '_' + language, binding: binding.sub(code), label: location.getIn(['descriptions', language]) })
              );
            }).toArray()
          ));
      }

    });

  });
