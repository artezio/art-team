define(['react', 'morearty', 'c!component/cv/CVEdit', 'c!component/cv/CVView'], function (React, Morearty, CVEdit, CVView) {
  'use strict';

  return React.createClass({

    displayName: 'CV',
    mixins: [Morearty.Mixin],

    render: function () {
      var binding = this.getBinding();
      return this.props.readOnly ? CVView({ binding: binding }) : CVEdit({ binding: binding });
    }

  });

});
