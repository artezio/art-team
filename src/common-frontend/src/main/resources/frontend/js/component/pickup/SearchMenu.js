define([
    'react',
    'morearty',
    'immutable',
    'util/CommonUtil',
    'service/SharedDataSvc',
    'service/CVSvc',
    'c!component/common/SearchBar',
    'c!component/common/Checkbox'
  ],
  function (React, Morearty, Imm, CommonUtil, SharedDataSvc, CVSvc, SearchBar, Checkbox) {
    'use strict';

    var applyFilter, onChangeFilter, onClearFilter, onSelectItem, onChangeActive, onGroupTitleClick;

    applyFilter = function () {
      var binding = this.getDefaultBinding();
      var filter = binding.get('filter');
      var mode = binding.get('mode');
      var selected = binding.get('selected');

      if (filter) {
        CVSvc.find(filter, mode).done(function (response) {
          var results = Imm.fromJS(response.results);
          binding.atomically()
            .set('searchResults', results)
            .set('filter', filter)
            .update('cv.active', function (active) {
              return filter || selected.has(active) > 0 ? active : null;
            })
            .commit();
        });
      } else {
        binding.clear('searchResults');
      }
    };

    onChangeFilter = function (value) {
      this.getDefaultBinding().set('filter', value);
      applyFilter.call(this);
    };

    onClearFilter = function () {
      this.getDefaultBinding().set('filter', '');
      this.getDefaultBinding().atomically()
        .set('filter', '')
        .clear('searchResults')
        .clear('cv.active')
        .commit();
    };

    onSelectItem = function (location, owner, checked) {
      var binding = this.getDefaultBinding();
      var searchResults = binding.get('searchResults');
      binding.update('selectedResults', function (selectedResults) {
        if (checked) {
          var enExcerpt = searchResults.getIn(['en', location, owner]), ruExcerpt = searchResults.getIn(['ru', location, owner]);

          return selectedResults.withMutations(function (map) {
            var setExcerpt = function (language, excerpt) {
              if (excerpt) {
                map.setIn([language, location, owner], excerpt);
              }
            };
            setExcerpt('en', enExcerpt);
            setExcerpt('ru', ruExcerpt);
            return map;
          });

        } else {
          return selectedResults.withMutations(function (map) {
            var unsetExcerpt = function (language) {
              map.removeIn([language, location, owner]);
            };
            unsetExcerpt('en');
            unsetExcerpt('ru');
            return map;
          });
        }
      });
    };

    onChangeActive = function (owner) {
      var binding = this.getDefaultBinding();
      var currentOwner = binding.get('cv.active');
      if (currentOwner !== owner) {
        this.getDefaultBinding().set('cv.active', owner);
        window.scrollTo(0, 0);
      }
    };

    onGroupTitleClick = function (binding) {
      binding.update(function (value) {
        return !value;
      });
    };

    var itemsComparator = function (item1, item2) {
      if (item1.get('lastname') !== item2.get('lastname')) {
        return item1.get('lastname') < item2.get('lastname') ? -1 : 1;
      } else {
        return item1.get('firstname') < item2.get('firstname') ? -1 : 1;
      }
    };

    return React.createClass({

      displayName: 'SearchMenu',
      mixins: [Morearty.Mixin],

      componentDidMount: function () {
        applyFilter.call(this);
      },

      componentDidUpdate: function () {
        var ctx = this.getMoreartyContext();
        if (ctx.isChanged(this.getDefaultBinding(), 'mode') || ctx.isChanged(this.getBinding('language'))) {
          applyFilter.call(this);
        }
      },

      render: function () {
        var binding = this.getDefaultBinding(), languageBinding = this.getBinding('language'), prefsBinding = this.getBinding('prefs');

        var searchResults = binding.get('searchResults');
        var selectedResults = binding.get('selectedResults');
        var effectiveSearchResults = selectedResults.mergeDeep(searchResults);

        var active = binding.get('cv.active');
        var language = languageBinding.get();

        var selectedLocationsBinding = prefsBinding.sub('selectedLocations'), selectedLocations = selectedLocationsBinding.get();
        var resultsByLocation = effectiveSearchResults.get(language);
        var locations = resultsByLocation ?
          resultsByLocation.map(function (_, location) {
            return SharedDataSvc.getLocationDescription(location, language);
          }).sort() :
          Imm.Map();

        var _ = React.DOM;
        return _.div({ className: 'block pick-up-menu' },

        _.div({ className: '__absolute-wrapper' },

          SearchBar({
            binding: { default: binding.sub('filter'), language: languageBinding, mode: binding.sub('mode') },
            onChange: onChangeFilter.bind(this),
            onKeyUp: Morearty.Callback.onEscape(onClearFilter.bind(this)),
            autofocus: true,
            switcherEnabled: true
          }),

          _.ul({ className: 'grouped-list' },

            locations.map(function (locationDescription, location) {
              var items = resultsByLocation.get(location);
              if (items.count() > 0) {

                var expanded = selectedLocations.get(location);

                var groupTitleClass = React.addons.classSet({
                  'group-title': true,
                  collapsed: !expanded
                });

                return _.li({ key: location + '_' + language },
                  _.div({ className: groupTitleClass, onClick: onGroupTitleClick.bind(this, selectedLocationsBinding.sub(location)) }, locationDescription),
                  expanded ?
                    _.ul({ className: 'vertical-list' },
                      items.sort(itemsComparator).map(function (excerpt) {
                        if (excerpt) {
                          var owner = excerpt.get('owner');
                          var liClass = React.addons.classSet({ active: owner === active });
                          return _.li({ key: owner + '_' + language, className: liClass },
                            Checkbox({ binding: binding.sub(['selected', owner]), onChange: onSelectItem.bind(this, location, owner), onLabelClick: onChangeActive.bind(this, owner), label: excerpt.get('lastname') + ' ' + excerpt.get('firstname') })
                          );
                        } else {
                          return null;
                        }
                      }.bind(this)).toArray()
                    ) :
                    null
                );
              } else {
                return null;
              }
            }.bind(this)).toArray()

          )

        )
        );
      }

    });

  });
