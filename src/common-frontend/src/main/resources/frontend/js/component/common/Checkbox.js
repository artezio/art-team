define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  var onClick = function (checked) {
    this.getDefaultBinding().set(checked);
    if (this.props.onChange) {
      this.props.onChange(checked);
    }
  };

  var onSpaceOrEnter = function (checked, event) {
    if (event.key === ' ') {
      this.getDefaultBinding().set(checked);
    } else {
      this.props.onLabelClick && this.props.onLabelClick();
    }
  };

  return React.createClass({

    displayName: 'Checkbox',
    mixins: [Morearty.Mixin],

    render: function () {
      var checked = this.getDefaultBinding().get();
      var spanClass = React.addons.classSet({
        checkbox: true,
        checked: checked
      });

      var onClickBound = onClick.bind(this, !checked);
      var onSpaceOrEnterBound = Morearty.Callback.onKey(onSpaceOrEnter.bind(this, !checked), [' ', 'Enter']);

      var _ = React.DOM;
      return _.span({ className: spanClass, title: this.props.label },
        _.span({ className: 'checker', tabIndex: 0, onClick: onClickBound, onKeyDown: onSpaceOrEnterBound }),
        _.span({ className: 'checkbox-label', onClick: this.props.onLabelClick || onClickBound },
          this.props.label
        )
      );
    }
  });

});
