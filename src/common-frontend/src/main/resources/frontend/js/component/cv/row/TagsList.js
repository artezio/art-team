define(['react', 'morearty', 'util/CommonUtil', 'service/I18NSvc', 'c!component/common/AutoInput', 'c!component/common/ActionIcon'],
  function (React, Morearty, CommonUtil, I18NSvc, AutoInput, ActionIcon) {
    'use strict';

    var t = I18NSvc.t;

    var onAddTag = function (binding, index) {
      var effectiveIndex = Morearty.Util.undefinedOrNull(index) ? binding.get().count() : index;
      binding.update(function (tags) {
        return tags.splice(effectiveIndex, 0, '');
      });
      CommonUtil.updateTransientState(this, function (ts) {
        ts.focusedTagIndex = effectiveIndex;
      });
    };

    return React.createClass({

      displayName: 'TagsList',
      mixins: [Morearty.Mixin],

      render: function () {
        var self = this;
        var binding = self.getDefaultBinding();

        var focusedTagIndex = CommonUtil.getTransientStateOnce(self).focusedTagIndex;

        var mkTag = function (tag, index) {
          var tagBinding = binding.sub(index);
          var autofocus = !Morearty.Util.undefinedOrNull(focusedTagIndex) && focusedTagIndex === index;
          return AutoInput({
              className: 'tag',
              key: index,
              binding: tagBinding,
              onChange: Morearty.Callback.set(tagBinding),
              onKeyDown: Morearty.Callback.onEnter(function () {
                if (tagBinding.get()) onAddTag.call(self, binding, index + 1);
              }),
              onKeyUp: Morearty.Callback.onEscape(Morearty.Callback.delete(tagBinding, Morearty.Util.not)),
              placeholder: self.props.placeholder,
              autofocus: autofocus,
              destroyOnEmpty: true }
          );
        };

        var _ = React.DOM;
        return _.span(null,
          binding.get().map(mkTag).toArray(),
          ActionIcon({ type: 'add', onClick: function () { return onAddTag.call(self, binding); }, title: t('component.icons.add') })
        );
      }

    });

  });
