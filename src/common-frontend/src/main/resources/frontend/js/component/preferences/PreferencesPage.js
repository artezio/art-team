define(['react', 'morearty', 'service/I18NSvc', 'service/PrefsSvc', 'c!component/menu/SideMenu', 'c!component/menu/MenuAction', 'c!component/preferences/Preferences'],
  function (React, Morearty, I18NSvc, PrefsSvc, SideMenu, MenuAction, Preferences) {
    'use strict';

    var t = I18NSvc.t;

    var onSave = function () {
      PrefsSvc.save().done(function () {
        this.getDefaultBinding().set('changed', false);
      }.bind(this));
    };

    return React.createClass({

      displayName: 'PreferencePage',
      mixins: [Morearty.Mixin],

      render: function () {
        var changed = this.getDefaultBinding().get('changed');

        var _ = React.DOM;
        return _.div({ className: 'wrapper' },
          SideMenu({
            binding: this.getBinding('language'),
            actions: [
              MenuAction({ key: 'save', onClick: onSave.bind(this), disabled: !changed, icon: 'save', label: t('component.common.action.save') })
            ]
          }),
          Preferences({ binding: this.getBinding() })
        );
      }

    });

  });
