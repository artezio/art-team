define(['react', 'morearty', 'c!component/cv/row/TagsListView'],
  function (React, Morearty, TagsList) {
    'use strict';

    return React.createClass({

      displayName: 'ExpertiseRowView',
      mixins: [Morearty.Mixin],

      shouldComponentUpdateOverride: function (shouldComponentUpdate, nextProps) {
        return this.props.filter !== nextProps.filter || shouldComponentUpdate();
      },

      render: function () {
        var binding = this.getDefaultBinding();
        var tags = binding.sub('tags');

        var _ = React.DOM;
        return _.div({ className: 'tags-wrapper' },
          _.span({ className: 'label' }, binding.get('area')),
          TagsList({ binding: tags, filter: this.props.filter })
        );
      }

    });

  });
