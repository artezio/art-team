define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  return React.createClass({

    displayName: 'Loading',
    mixins: [Morearty.Mixin],

    componentDidUpdate: function () {
      if (this.refs.shadow && this.getDefaultBinding().get()) {
        var node = this.refs.shadow.getDOMNode();
        setTimeout(function () { node.className = 'shadow-base'; }, 500);
      }
    },

    render: function () {
      var _ = React.DOM;
      if (this.getDefaultBinding().get()) {
        return _.div({ ref: 'shadow', className: 'shadow-base _hidden' },
          _.div({ className: 'loading' },
            _.ul({ className: 'loading__loader' },
              _.li(),
              _.li(),
              _.li()
            )
          )
        );
      } else {
        return null;
      }
    }

  });

});
