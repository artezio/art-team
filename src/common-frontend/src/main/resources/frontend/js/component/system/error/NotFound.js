define(['react', 'morearty', 'service/I18NSvc'],
  function (React, Morearty, I18NSvc) {
    'use strict';

    var t = I18NSvc.t;

    return React.createClass({

      displayName: 'NotFound',
      mixins: [Morearty.Mixin],

      render: function () {
        var _ = React.DOM;
        return _.div(null,
          _.span(null, t('component.notFound.text')),
          _.span(null,
            _.a({ href: 'javascript: window.history.go(-1)' }, t('component.permissionViolation.back'))
          )
        );
      }

    });

  });
