define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  var onClick = function () {
    this.setState({ open: !this.state.open })
  };

  return React.createClass({

    displayName: 'Drawer',
    mixins: [Morearty.Mixin],

    getInitialState: function () {
      return {
        open: false
      };
    },

    render: function () {

      var open = this.state.open;

      var divClass = React.addons.classSet({
        drawer: true,
        drawer_open: open
      });

      var _ = React.DOM;
      return _.div({ className: divClass},
        _.div({ className: 'drawer__trigger', onClick: onClick.bind(this)},
         this.props.children[0]
        ),
        _.div({ className: 'drawer__content'},
          this.props.children.slice(1)
        )
      );
    }
  });

});