define(['react', 'morearty', 'immutable', 'c!component/common/Icon'],
  function (React, Morearty, Imm, Icon) {
    'use strict';

    return React.createClass({

      displayName: 'EditableListView',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();

        var value = binding.get() || Imm.List.of();

        var _ = React.DOM;
        var rowComponent = this.props.rowComponent || _.span;
        var mkRow = function (rowValue, index) {

          return _.li({ key: index },
            rowComponent({ binding: binding.sub(index) }, rowValue)
          );
        };

        return _.div({ className: 'editable-list' },
          _.div(null,
            Icon({ type: this.props.icon }), _.h2(null, this.props.title)
          ),
          _.hr(),
          _.ul(null,
            value.map(mkRow).toArray()
          )
        );
      }

    });

  });
