define(['react', 'morearty', 'service/I18NSvc', 'service/CVSvc', 'c!component/common/AutoInput'],
  function (React, Morearty, I18NSvc, CVSvc, AutoInput) {
    'use strict';

    var option = React.createFactory(Morearty.DOM.option);

    var t = I18NSvc.t;

    var focusIfNeeded, onDidMountOrUpdate;

    focusIfNeeded = function (self) {
      if (self.props.autofocus) {
        self.refs.startYearSelect.getDOMNode().focus();
      }
    };

    onDidMountOrUpdate = function (self) {
      focusIfNeeded(self);
    };

    return React.createClass({

      displayName: 'EducationRow',
      mixins: [Morearty.Mixin],

      componentDidMount: function () {
        onDidMountOrUpdate(this);
      },

      componentDidUpdate: function () {
        onDidMountOrUpdate(this);
      },

      render: function () {
        var binding = this.getDefaultBinding();
        var startYear = binding.sub('startYear');
        var endYear = binding.sub('endYear');
        var institution = binding.sub('institution');

        var stringToIntTransformer = function (yearAsString) { return parseInt(yearAsString); };
        var availableYears = CVSvc.getAvailableYears();

        var tl = I18NSvc.forLang(this.props.language);

        var _ = React.DOM;
        return _.span(null,
          _.select({ ref: 'startYearSelect', value: startYear.get(), onChange: Morearty.Callback.set(startYear, stringToIntTransformer) },
            availableYears.map(function (year, index) {
              return option({ key: index, value: year.value}, year.name);
            })
          ),
          _.span({ className: 'dash' }, t('common.endash')),
          _.select({ value: endYear.get(), onChange: Morearty.Callback.set(endYear, stringToIntTransformer) },
            availableYears.map(function (year, index) {
              return option({ key: index, value: year.value}, year.name);
            })
          ),
          _.span({ className: 'label left' },
            AutoInput({ binding: institution, onChange: Morearty.Callback.set(institution), placeholder: tl('component.cv.placeholder.institution') })
          )

        );
      }

    });

  });
