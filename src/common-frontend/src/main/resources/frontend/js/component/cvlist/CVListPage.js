define([
    'react',
    'morearty',
    'immutable',
    'util/CommonUtil',
    'service/I18NSvc',
    'service/CVSvc',
    'service/SharedDataSvc',
    'service/SecuritySvc',
    'c!component/common/Icon',
    'c!component/common/SearchBar',
    'c!component/common/LocationsFilter',
    'c!component/menu/SideMenu',
    'c!component/menu/MenuAction',
    'c!component/menu/ActionGroup',
    'c!component/cvlist/CVList',
    'component/cvlist/Constants'
  ],
  function (React, Morearty, Imm, CommonUtil, I18NSvc, CVSvc, SharedDataSvc, SecuritySvc, Icon, SearchBar, LocationsFilter, SideMenu, MenuAction, ActionGroup, CVList, Constants) {
    'use strict';

    var t = I18NSvc.t;

    var showLoading, fetchCVExcerpts, fetchRoles;

    showLoading = function () {
      return this.getDefaultBinding().get('searchResults').count() > 0;
    };

    fetchCVExcerpts = function () {
      var binding = this.getDefaultBinding();

      return CVSvc.fetchCVExcerpts({ noLoading: showLoading.call(this) }).done(function (response) {
        var searchData = Imm.fromJS(response.results);

        var results = Imm.Map().withMutations(function (m) {
          searchData.forEach(function (m2, location) {
            m.mergeDeep(m2.map(function (excerpts) {
              return excerpts.map(function (excerpt) {
                return excerpt.set('location', location);
              }).toMap();
            }));
          });
          return m;
        });

        var filterF = function (language, excerpt) {
          return !!excerpt.get(language);
        };
        var sortF = function (language, excerpt) {
          return excerpt.getIn([language, 'lastname']);
        };
        var prepare = function (language) {
          return results.filter(filterF.bind(null, language)).sortBy(sortF.bind(null, language));
        };
        var searchResults = Imm.Map({ en: prepare('en'), ru: prepare('ru') });

        var deselected = Imm.Map({ en: false, ru: false });
        var exportLanguages = Imm.Map().withMutations(function (map) {
          results.keySeq().forEach(function (owner) {
            map.set(owner, deselected);
          });
          return map;
        });

        binding.atomically()
          .set(binding.meta(), 'searchData', searchData)
          .set('searchResults', searchResults)
          .merge('exportLanguages', true, exportLanguages)
          .commit({ notify: false });

        applyFilter.call(this);

      }.bind(this));
    };

    fetchRoles = function () {
      if (SecuritySvc.isAdmin()) {
        var binding = this.getDefaultBinding();
        SecuritySvc.fetchRoles({ noLoading: showLoading.call(this) }).done(function (response) {
          binding.set('roles', Imm.fromJS(response.roles));
        });
      }
    };

    var someSelected, matches, getShown, applyFilter;

    someSelected = function () {
      var binding = this.getDefaultBinding();
      var exportLanguages = binding.get('exportLanguages');
      return exportLanguages.find(function (languages) {
        return languages.get('en') || languages.get('ru');
      });
    };

    matches = function (owner, excerpts, filter, mode, language, location) {
      if (filter) {
        var lines = Imm.List.of().withMutations(function (v) {
          v.push(owner.toLowerCase());
          excerpts.forEach(function (excerpt) {
            v.push(excerpt.get('lastname').toLowerCase());
            v.push(excerpt.get('firstname').toLowerCase());
            v.push(excerpt.get('position').toLowerCase());
            v.push(SharedDataSvc.getLocationDescription(location, language).toLowerCase());
          });
        }).toArray();

        return CommonUtil.filterMatches(lines, filter, mode);
      } else {
        return true;
      }
    };

    getShown = function (filter, mode, searchData, language, selectedLocations) {
      return Imm.Set().withMutations(function (shown) {
        if (searchData) {
          searchData.forEach(function (sameLocationExcerpts, location) {
            if (selectedLocations.get(location)) {
              sameLocationExcerpts.forEach(function (excerpts, owner) {
                if (matches(owner, excerpts, filter, mode, language, location)) {
                  shown.add(owner);
                }
              });
            }
          });
        }
      });
    };

    applyFilter = function () {
      var binding = this.getDefaultBinding();
      var language = this.getBinding('language').get();
      var selectedLocations = this.getBinding('prefs').get('selectedLocations');
      var shown = getShown(binding.get('filter'), binding.get('mode'), binding.meta().get('searchData'), language, selectedLocations);
      binding.set('shown', shown);
    };

    var getExportSpec, onGroupExport, onEmail, onFilterChange, onFilterClear, onSelectAll;

    getExportSpec = function (exportLanguages) {
      var spec = {};

      exportLanguages.forEach(function (languages, owner) {
        var selectedLanguages = languages.filter(Morearty.Util.identity).keySeq().toArray();
        if (selectedLanguages.length > 0) {
          spec[owner] = selectedLanguages;
        }
      });

      return spec;
    };

    onGroupExport = function () {
      var spec = getExportSpec(this.getDefaultBinding().get('exportLanguages'));
      CVSvc.groupExport(spec);
    };

    onEmail = function () {
      var spec = getExportSpec(this.getDefaultBinding().get('exportLanguages'));
      CVSvc.groupExportToEmail(spec);
    };

    onFilterChange = function (value) {
      this.getDefaultBinding().atomically()
        .set('filter', value)
        .set('numResults', Constants.RESULTS_BUCKET_SIZE)
        .commit();
      applyFilter.call(this);
    };

    onFilterClear = function () {
      this.getDefaultBinding().set('filter', '');
      applyFilter.call(this);
    };

    onSelectAll = function (binding, language, selected, event) {
      event.preventDefault();
      var searchResultsForLang = binding.get(['searchResults', language]);
      var shown = binding.get('shown');

      binding.sub('exportLanguages').update(function (exportLanguages) {

        return exportLanguages.map(function (languages, owner) {
          return languages.map(function (_value, language) {
            if (!selected || (selected && shown.has(owner))) {
              return selected && searchResultsForLang.get(owner) && searchResultsForLang.get(owner).has(language);
            }
          }).toMap();
        }).toMap();

      });
    };

    return React.createClass({

      displayName: 'CVListPage',
      mixins: [Morearty.Mixin],

      getDefaultState: function () {
        return Imm.Map({
          searchResults: Imm.Map(),
          roles: Imm.Map(),
          filter: '',
          mode: 'and',
          exportLanguages: Imm.Map(),
          shown: Imm.Set(),
          numResults: Constants.RESULTS_BUCKET_SIZE
        });
      },

      componentDidMount: function () {
        fetchCVExcerpts.call(this).done(fetchRoles.bind(this));

        var cb = applyFilter.bind(this);
        this.addBindingListener(this.getDefaultBinding(), 'mode', cb);
        this.addBindingListener(this.getBinding('language'), cb);
        this.addBindingListener(this.getBinding('prefs'), cb);
      },

      shouldRemoveListeners: function () { return true; },

      render: function () {
        var binding = this.getDefaultBinding();
        var languageBinding = this.getBinding('language'), language = languageBinding.get();
        var prefsBinding = this.getBinding('prefs');

        var noneSelected = !someSelected.call(this);
        var found = binding.get('shown').count();
        var numResults = binding.get('numResults');

        var _ = React.DOM;
        return _.div({ className: 'wrapper' },
          SideMenu({
            binding: languageBinding,
            actions: [
              ActionGroup({ key: 'export-group' },
                MenuAction({ key: 'export', onClick: onGroupExport.bind(this), disabled: noneSelected, icon: 'export', label: t('component.common.action.export'), liClass: 'export' }),
                MenuAction({ key: 'email', onClick: onEmail.bind(this), disabled: noneSelected, icon: 'email', label: t('component.common.action.email'), liClass: 'export' })
              )
            ]
          }),

          _.div({ className: 'wrapper content' },

            _.div({ className: 'column center' },
              _.div({ className: 'filters block' },
                _.div({ className: 'search-form'},
                  SearchBar({
                    binding: { default: binding.sub('filter'), language: languageBinding, mode: binding.sub('mode') },
                    placeholder: t('component.cvlist.placeholder.search'),
                    onChange: onFilterChange.bind(this),
                    onKeyUp: Morearty.Callback.onEscape(onFilterClear.bind(this)),
                    autofocus: true,
                    switcherEnabled: true
                  }),
                  LocationsFilter({ binding: { default: prefsBinding.sub('selectedLocations'), language: languageBinding } })
                )
              )
            ),

            _.div({ className: 'users block' },
              _.span({ className: 'found-count' },
                _.span({ className: 'label' }, t('component.cvlist.shown')),
                _.span({ className: 'value' }, Math.min(numResults, found) + "/" + found)
              ),

              _.span({ className: 'select-all' },
                _.a({ href: '#', onClick: onSelectAll.bind(null, binding, language, true) }, t('component.cvlist.selectAll')),
                _.span({ className: 'slash' }, t('common.slash')),
                _.a({ className: 'deselect-all', href: '#', onClick: onSelectAll.bind(null, binding, language, false) }, t('component.cvlist.deselectAll'))
              ),

              CVList({ binding: this.getBinding() })
            )

          )
        );
      }

    });

  });
