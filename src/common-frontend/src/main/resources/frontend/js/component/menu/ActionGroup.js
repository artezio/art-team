define(['react', 'morearty'], function (React, Morearty) {
  'use strict';

  return React.createClass({

    displayName: 'ActionGroup',
    mixins: [Morearty.Mixin],

    render: function () {
      var _ = React.DOM;
      return _.ul({ className: 'button_multi' },
        this.props.children
      );
    }

  });

});
