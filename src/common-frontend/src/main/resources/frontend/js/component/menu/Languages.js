define(['react', 'morearty', 'service/I18NSvc'], function (React, Morearty, I18NSvc) {
  'use strict';

  var t = I18NSvc.t;

  var onClick = function (binding, language, event) {
    event.preventDefault();
    binding.set(language);
  };

  return React.createClass({

    displayName: 'Languages',
    mixins: [Morearty.Mixin],

    render: function () {
      var binding = this.getDefaultBinding();
      var language = binding.get();
      var enClass = React.addons.classSet({ 'button': true, 'active': language === 'en' });
      var ruClass = React.addons.classSet({ 'button': true, 'active': language === 'ru' });

      var _ = React.DOM;
      return _.ul({ className: 'modes' },
        _.li({ key: 'en', className: enClass },
          _.a({ onClick: onClick.bind(null, binding, 'en') }, t('component.cvsidemenu.englishVersion'))
        ),
        _.li({ key: 'ru', className: ruClass },
          _.a({ onClick: onClick.bind(null, binding, 'ru') }, t('component.cvsidemenu.russianVersion'))
        )
      );
    }

  })

});
