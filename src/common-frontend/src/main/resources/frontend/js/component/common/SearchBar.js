define(['react', 'morearty', 'util/CommonUtil', 'service/I18NSvc'], function (React, Morearty, CommonUtil, I18NSvc) {
  'use strict';

  var input = React.createFactory(Morearty.DOM.input);

  var getSwitcher = function () {
    var languageBinding = this.getBinding('language'), language = languageBinding ? languageBinding.get() : I18NSvc.getLanguage();
    var tl = I18NSvc.forLang(language);

    var modeBinding = this.getBinding('mode'), mode = modeBinding.get();
    var ctx = this.getMoreartyContext();
    var getItemClass = function (modeCode) {
      return React.addons.classSet({ 'switcher__item': true, 'switcher__item_active': modeCode === mode });
    };

    var onModeChange = this.props.onModeChange || function (mode) { modeBinding.set(mode); };

    var _ = React.DOM;
    return _.li({ className: 'search-bar__item search-bar__item_width-10' },
      _.span({ className: 'switcher-wrapper' },
        _.ul({ className: 'switcher switcher_single' },
          _.li({ className: getItemClass('or'), onClick: onModeChange.bind(null, 'and') }, tl('component.searchBar.or')),
          _.li({ className: getItemClass('and'), onClick: onModeChange.bind(null, 'or') }, tl('component.searchBar.and'))
        )
      )
    );
  };

  var onChange = function (event) {
    var clearExistingTimeout = function () {
      var existingTimeoutId = CommonUtil.getTransientStateOnce(this).timeoutId;
      if (existingTimeoutId) clearTimeout(existingTimeoutId);
    };
    var queueOnChange = function () {
      var value = event.target.value;
      var timeoutId = setTimeout(function () {
        var cb = this.props.onChange || this.getDefaultBinding().set;
        cb(value);
      }.bind(this), 500);
      CommonUtil.setTransientState(this, { timeoutId: timeoutId });
    };

    clearExistingTimeout.call(this);
    queueOnChange.call(this);
  };

  return React.createClass({

    displayName: 'SearchBar',
    mixins: [Morearty.Mixin],

    getDefaultState: function () {
      return {
        mode: 'or'
      };
    },

    componentDidMount: function () {
      if (this.props.autofocus) {
        var node = this.refs.input.getDOMNode();
        node.focus();

        var textLength = node.value.length;
        node.setSelectionRange(textLength, textLength);
      }
    },

    render: function () {
      var binding = this.getDefaultBinding(), value = binding.get();

      var searchBarClass = React.addons.classSet({ 'search-bar': true, 'is-dirty': value });

      var _ = React.DOM;
      return _.div({ className: 'search-bar-wrapper' },
        _.ul({ className: searchBarClass },
          _.li({ className: 'search-bar__item search-bar__item_width-3' },
            _.i({ className: 'search-bar__icon icon search' })
          ),
          _.li({ className: 'search-bar__item search-bar__item_width-full' },
            input({
              className: 'search-bar__input',
              value: value,
              ref: 'input',
              placeholder: this.props.placeholder,
              onChange: onChange.bind(this),
              onKeyUp: this.props.onKeyUp
            })
          ),
          this.props.switcherEnabled ? getSwitcher.call(this) : null
        )
      );
    }

  });

});
