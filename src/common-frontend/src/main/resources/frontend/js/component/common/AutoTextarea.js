define(['react', 'morearty', 'util/CommonUtil'], function (React, Morearty, CommonUtil) {
  'use strict';

  var textarea = React.createFactory(Morearty.DOM.textarea);

  var MIN_HEIGHT, setHeight, focusIfNeeded, onDidMountOrUpdate;

  MIN_HEIGHT = 20;

  setHeight = function () {
    var textarea = this.refs.input.getDOMNode();
    var text = textarea.value || textarea.placeholder;

    var height;
    if (text) {
      var calculatedHeight = CommonUtil.calculateHeight(textarea, text, textarea.offsetWidth);
      height = Math.max(calculatedHeight, this.props.minHeight || MIN_HEIGHT);
    } else {
      height = MIN_HEIGHT;
    }

    textarea.style.height = height + 'px';
  };

  focusIfNeeded = function () {
    if (this.props.autofocus) {
      var textarea = this.refs.input.getDOMNode();
      if (input !== document.activeElement) {
        textarea.focus();
      }
    }
  };

  onDidMountOrUpdate = function () {
    focusIfNeeded.call(this);
    setHeight.call(this);
  };

  return React.createClass({

    displayName: 'AutoTextarea',
    mixins: [Morearty.Mixin],

    componentDidMount: function () {
      onDidMountOrUpdate.call(this);
    },

    componentDidUpdate: function () {
      onDidMountOrUpdate.call(this);
    },

    shouldComponentUpdateOverride: function (shouldComponentUpdate, nextProps) {
      return shouldComponentUpdate() ||
        (this.props && nextProps && this.props.placeholder !== nextProps.placeholder);
    },

    render: function () {
      var binding = this.getDefaultBinding();

      var props = {};
      CommonUtil.shallowMerge(this.props, props);
      props.value = binding.get();
      props.ref = 'input';

      return textarea(props);
    }

  });

});
