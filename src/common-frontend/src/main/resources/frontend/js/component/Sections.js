define([
    'react',
    'morearty',
    'immutable',
    'app/NowShowing',
    'service/I18NSvc',
    'c!component/preferences/PreferencesPage',
    'c!component/cv/CVPage',
    'c!component/cvlist/CVListPage',
    'c!component/pickup/PickUpPage'
  ],
  function (React, Morearty, Imm, NowShowing, I18NSvc, PreferencesPage, CVPage, CVListPage, PickUpPage) {
    'use strict';

    return React.createClass({

      displayName: 'CV',
      mixins: [Morearty.Mixin],

      getDefaultState: function () {
        return Imm.Map({
          language: I18NSvc.getLanguage()
        });
      },

      shouldComponentUpdateOverride: function (shouldComponentUpdate, nextProps) {
        return shouldComponentUpdate() ||
          (this.props && nextProps && this.props.nowShowing !== nextProps.nowShowing);
      },

      render: function () {
        var binding = this.getDefaultBinding(), prefs = this.getBinding('prefs');
        var languageBinding = binding.sub('language');

        switch (this.props.nowShowing) {
          case NowShowing.PREFS:
            return PreferencesPage({ binding: { default: binding.sub('prefs'), language: languageBinding, prefs: prefs } });
          case NowShowing.CV:
            return CVPage({ binding: { default: binding.sub('cv'), language: languageBinding, prefs: prefs } });
          case NowShowing.CVLIST:
            return CVListPage({ binding: { default: binding.sub('list'), language: languageBinding, prefs: prefs } });
          case NowShowing.PICKUP:
            return PickUpPage({ binding: { default: binding.sub('pickup'), language: languageBinding, prefs: prefs } });
        }
      }

    });

  });
