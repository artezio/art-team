define(['react', 'morearty', 'service/I18NSvc', 'service/CVSvc'],
  function (React, Morearty, I18NSvc, CVSvc) {
    'use strict';

    var option = React.createFactory(Morearty.DOM.option);

    var t = I18NSvc.t;

    var focusIfNeeded, onDidMountOrUpdate;

    focusIfNeeded = function (self) {
      if (self.props.autofocus) {
        self.refs.languageSelect.getDOMNode().focus();
      }
    };

    onDidMountOrUpdate = function (self) {
      focusIfNeeded(self);
    };

    return React.createClass({

      displayName: 'LanguageRow',
      mixins: [Morearty.Mixin],

      componentDidMount: function () {
        onDidMountOrUpdate(this);
      },

      componentDidUpdate: function () {
        onDidMountOrUpdate(this);
      },

      render: function () {
        var binding = this.getDefaultBinding();
        var language = binding.sub('language');
        var availableLanguages = CVSvc.getAvailableLanguages(this.props.language);
        var proficiency = binding.sub('proficiency');
        var proficiencyLevels = CVSvc.getProficiencyLevels(this.props.language);

        var _ = React.DOM;
        return _.span(null,
          _.select({ ref: 'languageSelect', value: language.get(), onChange: Morearty.Callback.set(language) },
            availableLanguages.map(function (language, index) {
              return option({ key: index, value: language.value}, language.name);
            })
          ),
          _.span({ className: 'dash' }, t('common.endash')),
          _.select({ value: proficiency.get(), onChange: Morearty.Callback.set(proficiency) },
            proficiencyLevels.map(function (level, index) {
              return option({ key: index, value: level.value }, level.name);
            })
          )
        );
      }

    });

  });
