define(['react', 'morearty', 'service/I18NSvc', 'service/CVSvc'],
  function (React, Morearty, I18NSvc, CVSvc) {
    'use strict';

    var t = I18NSvc.t;

    return React.createClass({

      displayName: 'LanguageRowView',
      mixins: [Morearty.Mixin],

      render: function () {
        var binding = this.getDefaultBinding();
        var language = binding.get('language');
        var availableLanguages = CVSvc.getAvailableLanguages(this.props.language);
        var proficiency = binding.get('proficiency');
        var proficiencyLevels = CVSvc.getProficiencyLevels(this.props.language);

        var _ = React.DOM;
        return _.span(null,
          _.span(null,
            availableLanguages.find(function (lang) {
              return lang.value === language;
            }).name
          ),
          _.span({ className: 'dash' }, t('common.endash')),
          _.span(null,
            proficiencyLevels.find(function (profLevel) {
              return profLevel.value === proficiency;
            }).name
          )
        );
      }

    });

  });
