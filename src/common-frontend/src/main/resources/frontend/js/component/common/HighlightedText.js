define(['react', 'morearty', 'util/CommonUtil'], function (React, Morearty, CommonUtil) {
  'use strict';

  var split = function (str, patterns, caseSensitive) {
    var re = new RegExp('(' + patterns.map(CommonUtil.escapeRegExp).join('|') + ')', caseSensitive ? '' : 'i');
    var splitted = str.split(re);
    return splitted.map(function (text, index) {
      return {
        text: text,
        highlight: index % 2 !== 0
      };
    });
  };

  return React.createClass({

    displayName: 'HighlightedText',
    mixins: [Morearty.Mixin],

    render: function () {
      var text = this.props.text;
      var pattern = this.props.pattern;

      var _ = React.DOM;
      if (pattern && pattern.length > 1) {
        var compiledPattern = typeof pattern === 'string' ? pattern.split(' ').filter(Morearty.Util.identity) : pattern;

        return _.span(null,
          split(text, compiledPattern, this.props.caseSensitive).map(function (o, index) {
            if (o.text) {
              var classSpec = { highlight: o.highlight };
              if (this.props.className) classSpec[this.props.className] = true;
              var spanClass = React.addons.classSet(classSpec);
              return _.span({ key: index, className: spanClass }, o.text);
            } else {
              return null;
            }
          }.bind(this))
        );
      } else {
        return _.span(this.props, text);
      }
    }

  });

});
