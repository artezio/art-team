define(['react', 'morearty', 'app/Router'],
  function (React, Morearty, Router) {
    'use strict';

    return React.createClass({

      displayName: 'Logout',
      mixins: [Morearty.Mixin],

      componentDidMount: function () {
        setTimeout(Router.toLogin, 100);
      },

      render: function () {
        return null;
      }

    });

  });
