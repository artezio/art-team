define(['react', 'morearty', 'c!component/common/Icon'], function (React, Morearty, Icon) {
  'use strict';

  var getLiClass = function (self) {
    var liClass = {
      'button': true,
      'disabled': self.props.disabled
    };
    if (self.props.liClass) {
      liClass[self.props.liClass] = true;
    }
    return React.addons.classSet(liClass);
  };

  var onClick = function (event) {
    event.preventDefault();
    if (!this.props.disabled) {
      this.props.onClick();
    }
  };

  return React.createClass({

    displayName: 'MenuAction',
    mixins: [Morearty.Mixin],

    render: function () {
      var _ = React.DOM;
      var liClass = getLiClass(this);
      var disabled = this.props.disabled;

      return _.li({ className: liClass, title: this.props.title },
        _.a({ href: '#', onClick: onClick.bind(this), disabled: disabled }, Icon({ type: this.props.icon }), this.props.label)
      );
    }

  });

});
