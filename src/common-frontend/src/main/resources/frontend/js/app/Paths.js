define([], function () {

  var PATHS = {
    PermissionViolation: '/permissionviolation',
    NotFound: '/notfound',
    Login: '/login',
    Logout: '/logout',

    Preferences: '/preferences',
    CVList: '/cvlist',
    PickUp: '/pickup'
  };

  return {

    getLoginPath: function () {
      return PATHS.Login;
    },

    getLogoutPath: function () {
      return PATHS.Logout;
    },

    getPermissionViolationPath: function () {
      return PATHS.PermissionViolation;
    },

    getNotFoundPath: function () {
      return PATHS.NotFound;
    },

    getPreferencesPath: function () {
      return PATHS.Preferences;
    },

    getCVPath: function (username) {
      return '/cv/' + username;
    },

    getCVEditPath: function (username) {
      return '/cv/' + username + '/edit';
    },

    getCVListPath: function () {
      return PATHS.CVList;
    },

    getPickUpPath: function () {
      return PATHS.PickUp;
    }

  };

});
