define([], function () {
  'use strict';

  return Object.freeze({
    PERMISSION_VIOLATION: 'permissionviolation',
    NOT_FOUND: 'notfound',
    LOGIN: 'login',
    LOGOUT: 'logout',
    PREFS: 'prefs',
    CV: 'cv',
    CVLIST: 'cvlist',
    PICKUP: 'pickup'
  });

});
