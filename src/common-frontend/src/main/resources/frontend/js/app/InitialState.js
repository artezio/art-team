define({
  version: 2,

  /* loading, nowShowing, lastPage */

  notifications: [],
  modal: {},

  shared: {
    /* locations */
  },

  prefs: {
    /* language */
    selectedLocations: {},
    defaultSection: null
  },

  login: {
    /* username, password, loginFailed, responseStatus */
  },

  sections: {
    /* language */
    prefs: {
      /* changed */
    },
    cv: {
      /* owner, data, changed */
    },
    list: {
      /* data, filter, shown */
    },
    pickup: {
      /* searchData, data, filter, shown, selected, cv */
    }
  }

});
