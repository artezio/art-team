define(['pubsub'], function (PubSub) {

  return {

    subscribe: function (key, handler) {
      return PubSub.subscribe(key, handler);
    },

    unsubscribe: function (token) {
      PubSub.unsubscribe(token);
    },

    notify: function (key, params_var_args) {
      PubSub.publish.apply(PubSub, arguments);
    }

  };

});
