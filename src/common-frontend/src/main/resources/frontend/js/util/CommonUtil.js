define(['morearty', 'moment', 'bowser'], function (Morearty, moment, bowser) {
  'use strict';

  var dummyElement, createDummyElement, getDummyElement;

  dummyElement = null;

  createDummyElement = function () {
    var element = document.createElement('pre');
    var textNode = document.createTextNode('');
    element.appendChild(textNode);

    element.style.position = 'absolute';
    element.style.visibility = 'hidden';
    element.style.left = '-999px';
    element.style.top = '-999px';

    element.style.whiteSpace = 'pre-wrap';

    if (bowser.firefox) element.style.wordBreak = 'break-all';
    else element.style.wordWrap = 'break-word';

    element.style.margin = 0;
    element.style.padding = 0;
    element.style.outline = 0;
    element.style.border = 0;

    return element;
  };

  getDummyElement = function (text, fontFamily, fontSize, maxWidth, maxHeight) {
    if (dummyElement) {
      document.body.removeChild(dummyElement);
    } else {
      dummyElement = createDummyElement();
    }

    var element = dummyElement;

    element.style.fontFamily = fontFamily;
    element.style.fontSize = fontSize;

    element.style.width = maxWidth ? maxWidth + 'px' : 'auto';
    element.style.height = maxHeight ? maxHeight + 'px' : 'auto';

    var textNode = element.firstChild;
    textNode.nodeValue = text[text.length - 1] === '\n' ? text + '_' : text;

    document.body.appendChild(element);

    return element;
  };

  var dropUnits, toNumber;

  dropUnits = function (s) {
    var i;
    for (i = 0; i < s.length && !isNaN(s[i]); i++) {}
    return Number(s.substr(0, i));
  };

  toNumber = function (s) {
    return s ? dropUnits(s) : 0;
  };

  return {

    range: function (from, to) {
      if (from > to) {
        return [];
      } else {
        var arr = [];
        while (from <= to) arr.push(from++);
        return arr;
      }
    },

    addParams: function (url, var_args) {
      var length = arguments.length;
      if (length == 1) {
        return url;
      } else {
        var pairs = [];
        for (var i = 1; i < arguments.length; i += 2) {
          var name = arguments[i], value = arguments[i + 1];
          pairs.push(name + '=' + value);
        }
        return url + '?' + pairs.join('&');
      }
    },

    formatDate: function (millis, language) {
      return moment(millis).locale(language).format('LLL');
    },

    padNumber: function (num, size) {
      var s = num + "";
      while (s.length < size) s = "0" + s;
      return s;
    },

    escapeRegExp: function (str) {
      return str.replace(/[\-\[\]\/\{}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    },

    filterMatches: function (lines, filter, mode) {
      var words = filter.toLowerCase().split(' ').filter(Morearty.Util.identity);
      var searchF = function (word) { return this.mentions(lines, word); }.bind(this);
      switch (mode || 'or') {
        case 'or':
          return words.find(searchF);
        case 'and':
          return words.every(searchF);
      }
    },

    mentions: function (lines, word, strictEqual) {
      return lines.find(function (line) {
        return line && word && strictEqual ? line === word : line.indexOf(word) !== -1
      });
    },

    getStyleInfo: function (element) {
      var style = window.getComputedStyle(element, null);
      return {
        fontFamily: style.getPropertyValue('font-family'),
        fontSize: style.getPropertyValue('font-size'),

        paddingLeft: style.getPropertyValue('padding-left'),
        paddingRight: style.getPropertyValue('padding-right'),
        paddingTop: style.getPropertyValue('padding-top'),
        paddingBottom: style.getPropertyValue('padding-bottom'),

        borderLeft: style.getPropertyValue('border-left-width'),
        borderRight: style.getPropertyValue('border-right-width'),
        borderTop: style.getPropertyValue('border-top-width'),
        borderBottom: style.getPropertyValue('border-bottom-width')
      };
    },

    getTextDimensions: function (text, fontFamily, fontSize, maxWidth, maxHeight) {
      var element = getDummyElement(text, fontFamily, fontSize, maxWidth, maxHeight);

      return {
        width: element.offsetWidth,
        height: element.offsetHeight
      };
    },

    getTextWidth: function (text, fontFamily, fontSize, maxHeight) {
      var element = getDummyElement(text, fontFamily, fontSize, null, maxHeight);
      return element.offsetWidth;
    },

    getTextHeight: function (text, fontFamily, fontSize, maxWidth) {
      var element = getDummyElement(text, fontFamily, fontSize, maxWidth);
      return element.offsetHeight;
    },

    calculateSize: function (element, text) {
      var si = this.getStyleInfo(element);
      var size = this.getTextDimensions(text, si.fontFamily, si.fontSize);
      return {
        width: size.width + toNumber(si.paddingLeft) + toNumber(si.paddingRight) + toNumber(si.borderLeft) + toNumber(si.borderRight),
        height: size.height + toNumber(si.paddingTop) + toNumber(si.paddingBottom) + toNumber(si.borderTop) + toNumber(si.borderBottom)
      };
    },

    calculateWidth: function (element, text, maxHeight) {
      var si = this.getStyleInfo(element);
      var width = this.getTextWidth(text, si.fontFamily, si.fontSize, maxHeight);
      return width + toNumber(si.paddingLeft) + toNumber(si.paddingRight) + toNumber(si.borderLeft) + toNumber(si.borderRight);
    },

    calculateHeight: function (element, text, maxWidth) {
      var si = this.getStyleInfo(element);
      var height = this.getTextHeight(text, si.fontFamily, si.fontSize, maxWidth);
      return height + toNumber(si.paddingTop) + toNumber(si.paddingBottom) + toNumber(si.borderTop) + toNumber(si.borderBottom);
    },

    setTransientState: function (comp, state) {
      comp.transientState = state;
    },

    updateTransientState: function (comp, f) {
      var transientState = comp.transientState || {};
      f(transientState);
      comp.transientState = transientState;
    },

    clearTransientState: function (comp) {
      comp.transientState = null;
    },

    getTransientState: function (comp) {
      return comp.transientState || {};
    },

    getTransientStateOnce: function (comp) {
      var result = this.getTransientState(comp);
      this.clearTransientState(comp);
      return result;
    },

    memoize: function (f) {
      var stringifyJson = JSON.stringify, cache = {};
      var cachedfun = function () {
        var hash = stringifyJson(arguments);
        return (hash in cache) ? cache[hash] : cache[hash] = f.apply(this, arguments);
      };
      cachedfun.__cache = (function () {
        cache.remove || (cache.remove = function () {
          var hash = stringifyJson(arguments);
          return (delete cache[hash]);
        });
        return cache;
      }).call(this);
      return cachedfun;
    },

    /** alallow merge object properties from source object to dest.
     * @param {Object} source source object
     * @param {Object} dest destination object
     * @return {Object} destination object */
    shallowMerge: function (source, dest) {
      for (var prop in source) {
        if (source.hasOwnProperty(prop)) {
          dest[prop] = source[prop];
        }
      }
      return dest;
    },

    /** Partially apply React component constructor.
     * @param {Function} comp component constructor
     * @param {Object} props properties to apply
     * @param {Boolean} override override existing properties flag, true by default
     * @returns {Function} partially-applied React component constructor */
    papply: function (comp, props, override) {
      var self = this;
      var f = function (props2, children) {
        var effectiveChildren = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : null;
        if (props2) {
          var effectiveProps = {};
          if (f._props) {
            self.shallowMerge(f._props, effectiveProps);
            self.shallowMerge(props2, effectiveProps);
          } else {
            effectiveProps = props2;
          }
          return comp(effectiveProps, effectiveChildren);
        } else {
          return comp(f._props, effectiveChildren);
        }
      };

      if (comp._props) {
        var newCompProps = {};
        if (override !== false) {
          self.shallowMerge(comp._props, newCompProps);
          self.shallowMerge(props, newCompProps);
        } else {
          self.shallowMerge(props, newCompProps);
          self.shallowMerge(comp._props, newCompProps);
        }
        f._props = newCompProps;
      } else {
        f._props = props;
      }

      return f;
    },

  };

});
