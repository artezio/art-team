define([], function () {
  'use strict';

  return {

    load: function (name, parentRequire, onload) {
      parentRequire.load(parentRequire.toUrl(name) + ".json", function (result) {
        onload(JSON.parse(result));
      });
    }

  };

});
