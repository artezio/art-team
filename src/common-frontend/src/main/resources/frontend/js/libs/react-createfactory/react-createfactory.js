define(['react'], function (React) {
  'use strict';

  return {

    load: function (name, parentRequire, onload) {
      parentRequire([name], function (reactClass) {
        onload(React.createFactory(reactClass));
      });
    }

  };

});
