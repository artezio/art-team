define([], function () {

  return Object.freeze({
    CV: 'cv',
    LIST: 'list',
    PICKUP: 'pickup'
  });

});
