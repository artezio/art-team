define(['session/Ajax'], function (Ajax) {
  'use strict';

  var getURL = function (username) {
    return '/api/cv/draft/' + username;
  };

  return {

    get: function (username, language) {
      return Ajax.call(getURL(username) + '?language=' + language, {
        type: 'GET',
        contentType: 'application/json'
      });
    },

    put: function (username, data, params) {
      return Ajax.call(getURL(username), {
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(data)
      }, params);
    },

    remove: function (username, language, params) {
      return Ajax.call(getURL(username) + '?language=' + language, {
        type: 'DELETE'
      }, params);
    }

  };

});
