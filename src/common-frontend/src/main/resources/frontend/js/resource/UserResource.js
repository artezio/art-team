define(['util/CommonUtil', 'session/Ajax'], function (CommonUtil, Ajax) {
  'use strict';

  var getURL = function (username) {
    return '/api/user/' + username;
  };

  return {

    getRoles: function (params) {
      return Ajax.call('/api/roles', {
        type: 'GET',
        contentType: 'application/json'
      }, params);
    },

    update: function (username, role, params) {
      return Ajax.call(getURL(username), {
        type: 'PATCH',
        contentType: 'application/json',
        data: JSON.stringify({
          role: role
        })
      }, params);
    }

  };

});
