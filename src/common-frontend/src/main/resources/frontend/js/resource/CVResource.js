define(['util/CommonUtil', 'session/Ajax'], function (CommonUtil, Ajax) {
  'use strict';

  var getURL = function (username) {
    return '/api/cv/' + username;
  };

  return {

    get: function (username, language) {
      var url = CommonUtil.addParams(getURL(username), 'language', language);
      return Ajax.call(url, {
        type: 'GET',
        contentType: 'application/json'
      });
    },

    put: function (username, data, params) {
      return Ajax.call(getURL(username), {
          type: 'PUT',
          contentType: 'application/json',
          data: JSON.stringify(data)
      }, params);
    },

    getAllExcerpts: function (params) {
      return Ajax.call('/api/cv', {
        type: 'GET',
        contentType: 'application/json'
      }, params);
    },

    find: function (query, mode, params) {
      var url = CommonUtil.addParams('/api/cvs', 'query', encodeURIComponent(query), 'mode', mode);
      return Ajax.call(url, {
        type: 'GET',
        contentType: 'application/json'
      }, params);
    },

    groupExportToEmail: function (owners, params) {
      return Ajax.call('/api/export/cv/email', {
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(owners)
      }, params);
    }

  };

});
