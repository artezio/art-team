define(['session/Ajax'], function (Ajax) {
  'use strict';

  var URL = '/api/prefs';

  return {

    get: function (params) {
      return Ajax.call(URL, {
        type: 'GET',
        contentType: 'application/json'
      }, params);
    },

    put: function (prefs, params) {
      return Ajax.call(URL, {
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(prefs)
      }, params);
    }

  };

});
