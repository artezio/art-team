define(['session/Ajax'], function (Ajax) {
  'use strict';

  return {

    create: function (sessionId, data) {
      return Ajax.call('/api/ticket', {
        type: 'POST',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
      }, {
        overridenSessionId: sessionId
      });
    }

  };

});
