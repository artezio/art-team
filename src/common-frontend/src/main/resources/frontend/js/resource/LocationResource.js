define(['session/Ajax'], function (Ajax) {
  'use strict';

  return {

    getAll: function () {
      return Ajax.call('/api/location', {
        type: 'GET',
        contentType: 'application/json'
      });
    }

  };

});
