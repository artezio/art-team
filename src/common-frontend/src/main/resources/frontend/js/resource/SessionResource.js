define(['session/Ajax'], function (Ajax) {
  'use strict';

  return {

    create: function (username, password) {
      return Ajax.call('/api/session', {
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
          username: username,
          password: password
        })
      }, {
        disableAuth: true
      });
    },

    remove: function (sessionId) {
      return Ajax.call('/api/session/' + sessionId, {
        type: 'DELETE'
      }, {
        disableAuth: true
      });
    }

  };

});
