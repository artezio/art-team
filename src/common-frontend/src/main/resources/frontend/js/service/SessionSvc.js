define(['session/SessionStorage', 'service/TransitSvc', 'event/EventSvc', 'event/EventType', 'resource/SessionResource'],
  function (SessionStorage, TransitSvc, EventSvc, EventType, SessionResource) {
    'use strict';

    return {

      login: function (username, password) {
        return SessionResource.create(username, password).done(function (sessionResponse) {
          var sessionId = sessionResponse.sessionId;
          var role = sessionResponse.role;
          SessionStorage.setSessionInfo(username, role, sessionId);
        })
      },

      logout: function () {
        var sessionId = SessionStorage.getSessionId();
        return SessionResource.remove(sessionId).always(function () {
          SessionStorage.clearSessionInfo();
        })
      },

      isLoggedIn: function () {
        return !!SessionStorage.getSessionId();
      },

      saveSession: function (state) {
        var ts = TransitSvc.toTransit(state);
        SessionStorage.saveState(ts);
      },

      loadSession: function () {
        var ts = SessionStorage.getSavedState();
        try {
          return TransitSvc.fromTransit(ts);
        } catch (e) {
          EventSvc.notify(EventType.UI_NOTIFICATION, {
            group: 'notification.common.sessionCleared',
            type: 'INFO'
          });
          console.error('Session has been cleared. State dump:\n' + ts, e);
          return null;
        }
      }

    };

  });
