define(['session/SessionStorage', 'resource/UserResource'], function (SessionStorage, UserResource) {
  'use strict';

  var isRole = function (role) {
    return SessionStorage.getRole() === role;
  };

  var ROLES_CODES = ['user', 'hr', 'manager', 'admin'];

  return {

    getRolesCodes: function () {
      return ROLES_CODES;
    },

    fetchRoles: function (params) {
      return UserResource.getRoles(params);
    },

    setRole: function (username, role) {
      return UserResource.update(username, role, {
        notification: 'notification.setRole'
      });
    },

    isUser: function () {
      return isRole('user');
    },

    isAdmin: function () {
      return isRole('admin');
    },

    isHR: function () {
      return isRole('hr');
    },

    isManager: function () {
      return isRole('manager');
    },

    canReadOtherPeopleCV: function () {
      return this.isAdmin() || this.isHR() || this.isManager();
    },

    hasCV: function () {
      return this.isUser() || this.isManager() || this.isAdmin();
    }

  };

});
