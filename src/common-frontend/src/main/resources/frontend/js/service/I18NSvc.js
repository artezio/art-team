define(['i18next', 'app/Context'], function (i18next, Ctx) {
  'use strict';

  var languageBinding = Ctx.getBinding().sub('prefs.language');

  return {

    init: function () {
      languageBinding.atomically().merge(true, this.getLanguage()).commit({ notify: false });
    },

    setLanguage: function (language) {
      languageBinding.set(language);
    },

    getLanguage: function () {
      return languageBinding.get() || this.t('language')
    },

    t: function (key) {
      return i18next.t(key, { lng: languageBinding.get() });
    },

    forLang: function (language) {
      return function (key) {
        return i18next.t(key, { lng: language });
      };
    }

  };

});
