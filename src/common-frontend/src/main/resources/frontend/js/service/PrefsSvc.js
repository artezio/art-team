define(['immutable', 'morearty', 'app/Context', 'common/Section', 'session/SessionStorage', 'service/SharedDataSvc', 'service/NavigationSvc', 'resource/PrefsResource'],
  function (Imm, Morearty, Ctx, Section, SessionStorage, SharedDataSvc, NavigationSvc, PrefsResource) {
    'use strict';

    var binding = Ctx.getBinding();
    var prefsBinding = binding.sub('prefs');
    var changedBinding = binding.sub('sections.prefs.changed');

    var loadPrefs = function () {
      return PrefsResource.get({
        notification: 'notification.loadPrefs'
      }).done(function (response) {
        var prefs = Imm.fromJS(response.prefs);
        prefsBinding.atomically()
          .set(prefs)
          .set(changedBinding, false)
          .commit();
      });
    };

    var state = {
      changedListenerId: null
    };

    return {

      init: function (loggingIn) {

        loadPrefs().done(function () {
          prefsBinding.atomically()
            .merge('selectedLocations', true, Imm.Map().withMutations(function (map) {
              SharedDataSvc.getLocations().forEach(function (item) {
                map.set(item.get('code'), true);
              });
              return map;
            }))
            .set(changedBinding, false)
            .commit();
        }).fail(function (xhr) {
          if (xhr.status === 404) {
            prefsBinding.atomically()
              .set('selectedLocations', Imm.Map().withMutations(function (map) {
                SharedDataSvc.getLocations().forEach(function (item) {
                  map.set(item.get('code'), true);
                });
                return map;
              }))
              .set(changedBinding, true)
              .commit();
          }
        }).always(function () {
          state.changedListenerId = prefsBinding.addListener(function () {
            Morearty.Util.async(changedBinding.set.bind(changedBinding, true));
          });

          if (loggingIn) {
            var Router = require('app/Router');
            switch (NavigationSvc.getDefaultSection(this.getDefaultSection())) {
              case Section.CV:
                Router.toCV(SessionStorage.getUsername());
                break;
              case Section.LIST:
                Router.toCVList();
                break;
              case Section.PICKUP:
                Router.toPickUp();
                break;
            }
          }
        }.bind(this));
      },

      save: function () {
        var prefs = prefsBinding.get().toJS();
        if (!prefs.defaultSection) {
          prefs.defaultSection = NavigationSvc.getDefaultSection();
        }

        return PrefsResource.put({
          prefs: prefs
        }, {
          notification: 'notification.savePrefs'
        });
      },

      destroy: function () {
        if (state.changedListenerId) {
          prefsBinding.removeListener(state.changedListenerId);
        }
      },

      getDefaultSection: function () {
        return prefsBinding.get('defaultSection');
      }

    };

  });
