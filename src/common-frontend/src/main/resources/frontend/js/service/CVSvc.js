define(['service/I18NSvc', 'util/CommonUtil', 'session/SessionStorage', 'resource/CVResource', 'resource/CVDraftResource', 'resource/TicketResource'],
  function (I18NSvc, CommonUtil, SessionStorage, CVResource, CVDraftResource, TicketResource) {
    'use strict';

    var getCurrentYear, getCurrentMonth;

    getCurrentYear = function () {
      return new Date().getFullYear();
    };

    getCurrentMonth = function () {
      return new Date().getMonth();
    };

    var LANGUAGE_CODES, getLanguages;

    LANGUAGE_CODES = [
      'by', 'cs', 'de', 'el', 'en', 'es', 'fi', 'fr', 'he', 'it', 'ja', 'lt', 'nl', 'no', 'pl', 'pt', 'ru', 'sv', 'ua', 'zh'
    ];

    getLanguages = CommonUtil.memoize(function (language) {
      var tl = I18NSvc.forLang(language);
      var languages = LANGUAGE_CODES.map(function (code) {
        return { name: tl('languages.' + code), value: code };
      });
      return languages.sort(function (o1, o2) {
        return o1.name > o2.name ? 1 : (o1.name < o2.name ? -1 : 0);
      });
    });

    return {

      fetchCV: function (owner, language) {
        return CVResource.get(owner, language);
      },

      publishCV: function (owner, data) {
        return CVResource.put(owner, data, {
          notification: 'notification.publishCV'
        });
      },

      fetchDraft: function (owner, language) {
        return CVDraftResource.get(owner, language);
      },

      saveDraft: function (owner, data) {
        return CVDraftResource.put(owner, data, {
          notification: 'notification.saveDraft'
        });
      },

      clearDraft: function (owner, language) {
        return CVDraftResource.remove(owner, language, {
          notification: 'notification.clearDraft'
        });
      },

      getTemplate: function (language) {
        var tl = I18NSvc.forLang(language);

        return {
          cv: {
            name: {
              firstname: '',
              lastname: ''
            },
            location: 'minsk',
            position: '',
            description: '',
            summary: [''],
            expertise: [
              { area: tl('component.cv.templates.expertise.area'), tags: [tl('component.cv.templates.expertise.tag.1')] }
            ],
            projects: [],
            education: [],
            certificates: [],
            languages: [
              { language: 'ru', proficiency: 'native' },
              { language: 'en', proficiency: 'intermediate' }
            ],
            additionalInfo: ['']
          },
          language: language,
          version: 0
        };
      },

      getAvailableYears: function () {
        var years = CommonUtil.range(1970, getCurrentYear());
        return years.map(function (year) {
          return { name: year, value: year };
        });
      },

      getAvailableMonths: function () {
        var months = CommonUtil.range(1, 12);
        return months.map(function (month) {
          return { name: month < 10 ? '0' + month : month, value: month };
        });
      },

      getDefaultYear: function () {
        return getCurrentYear();
      },

      getDefaultMonth: function () {
        return getCurrentMonth();
      },

      getAvailableLanguages: function (language) {
        return getLanguages(language);
      },

      getProficiencyLevels: function (language) {
        var tl = I18NSvc.forLang(language);

        return [
          { name: tl('languages.proficiency.elementary'), value: 'elementary' },
          { name: tl('languages.proficiency.beginner'), value: 'beginner' },
          { name: tl('languages.proficiency.intermediate'), value: 'intermediate' },
          { name: tl('languages.proficiency.advanced'), value: 'advanced' },
          { name: tl('languages.proficiency.fluent'), value: 'fluent' },
          { name: tl('languages.proficiency.native'), value: 'native' }
        ];
      },

      getDefaultProficiency: function () {
        return 'intermediate';
      },

      fetchCVExcerpts: function (params) {
        return CVResource.getAllExcerpts(params);
      },

      find: function (query, mode) {
        return CVResource.find(query, mode);
      },

      exportTo: function (owner, language, format) {
        var effectiveFormat = format || 'docx';
        var sessionId = SessionStorage.getSessionId();
        TicketResource.create(sessionId).done(function (ticketResponse) {
          var ticketId = ticketResponse.ticketId;
          var url = CommonUtil.addParams(
            '/api/export/cv/' + effectiveFormat + '/' + owner,
            'language', language, 'ticketId', ticketId
          );
          window.open(url, '_self');
        });
      },

      groupExport: function (spec) {
        if (Object.keys(spec).length > 0) {
          var sessionId = SessionStorage.getSessionId();
          TicketResource.create(sessionId, spec).done(function (ticketResponse) {
            var ticketId = ticketResponse.ticketId;
            var url = CommonUtil.addParams('/api/export/cv/zip', 'ticketId', ticketId);
            window.open(url, '_self');
          });
        }
      },

      exportToEmail: function (owner) {
        var spec = {};
        spec[owner] = ['en', 'ru'];
        this.groupExportToEmail(spec);
      },

      groupExportToEmail: function (spec) {
        if (Object.keys(spec).length > 0) {
          CVResource.groupExportToEmail({ spec: spec }, {
            notification: 'notification.exportToEmail'
          });
        }
      }

    };

  });
