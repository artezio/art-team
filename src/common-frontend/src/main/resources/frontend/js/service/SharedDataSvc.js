define(['immutable', 'app/Context', 'service/I18NSvc', 'resource/LocationResource'], function (Imm, Ctx, I18NSvc, LocationResource) {
  'use strict';

  return {

    fetchSharedData: function () {
      return LocationResource.getAll().done(function (response) {
        var locations = Imm.fromJS(response.locations);
        Ctx.getBinding().atomically()
          .set('shared.locations', locations)
          .update('shared.locationsByCode', function () {
            return Imm.Map().withMutations(function (map) {
              locations.forEach(function (location) {
                map.set(location.get('code'), location);
              });
            });
          })
          .commit({ notify: false });
      });
    },

    getLocations: function (language) {
      var locations = Ctx.getBinding().get('shared.locations');
      return locations ? locations.sortBy(function (location) {
        return location.getIn(['descriptions', language]);
      }) : Imm.Map();
    },

    getLocation: function (code) {
      var locations = Ctx.getBinding().get('shared.locationsByCode');
      return locations && locations.get(code);
    },

    getLocationDescription: function (code, language) {
      var location = this.getLocation(code);
      return location && location.getIn(['descriptions', language || I18NSvc.getLanguage()]);
    }

  };

});
