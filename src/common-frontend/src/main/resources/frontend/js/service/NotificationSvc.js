define(['immutable', 'app/Context', 'event/EventType', 'event/EventSvc', 'service/I18NSvc'],
  function (Imm, Ctx, EventType, EventSvc, I18NSvc) {
    'use strict';

    var t = I18NSvc.t;

    var notify, findLast, scheduleNotificationExpiration;

    notify = function (type, message) {
      Ctx.getBinding().update('notifications', function (notifications) {
        return notifications.push(Imm.Map({
          type: type,
          message: message,
          fadingOut: false,
          created: new Date().getTime()
        }));
      });
    };

    findLast = function (notifications) {
      return notifications.findIndex(function (notification) {
        return !notification.get('fadingOut');
      });
    };

    scheduleNotificationExpiration = function () {
      setTimeout(function () {
        Ctx.getBinding().update('notifications', function (notifications) {
          var index = findLast(notifications, false);
          return notifications.update(index, function (notification) {
            return notification.set('fadingOut', true);
          });
        });
      }, 4000);

      setTimeout(function () {
        Ctx.getBinding().update('notifications', function (notifications) {
          return notifications.rest();
        });
      }, 5000);
    };

    var state = {
      onModal: null
    };

    return {

      init: function () {
        return EventSvc.subscribe(EventType.UI_NOTIFICATION, function (topic, notification) {
          var group = notification.group;
          var message;
          switch (notification.type) {
            case 'INFO':
              message = t(group + '.success');
              break;
            case 'ERROR':
              message = t(group + '.failure');
              break;
            default:
              throw new Error('Invalid notification type: ' + notification.type);
          }

          notify(notification.type, message);
          scheduleNotificationExpiration();
        });
      },

      cleanup: function (notify) {
        Ctx.getBinding().atomically()
          .clear('notifications')
          .clear('modal')
          .commit({ notify: !!notify });
      },

      destroy: function (subscriptionToken) {
        EventSvc.unsubscribe(subscriptionToken);
      },

      showModal: function (group, cb) {
        state.onModal = cb;
        Ctx.getBinding().sub('modal').atomically()
          .set('visible', true)
          .set('title', t(group + '.title'))
          .set('text', t(group + '.text'))
          .commit();
      },

      onModal: function (value) {
        if (state.onModal) {
          state.onModal(value);
          state.onModal = null;

          Ctx.getBinding().sub('modal').atomically()
            .set('visible', false)
            .set('title', null)
            .set('text', null)
            .commit();

          return true;
        }
      }

    };

  });
