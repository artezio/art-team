define(['common/Section', 'service/SecuritySvc'],
  function (Section, SecuritySvc) {
    'use strict';

    return {

      getDefaultSection: function (defaultSectionInPrefs) {
        if (SecuritySvc.isUser()) return Section.CV;
        else {
          if (defaultSectionInPrefs) {
            return defaultSectionInPrefs === Section.CV && !SecuritySvc.hasCV() ? Section.LIST : defaultSectionInPrefs;
          } else {
            return SecuritySvc.isManager() ? Section.CV : Section.LIST;
          }
        }
      }

    };
  });
