package art.team.cvimporter

import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes

import art.team.app.{DefaultAppWideDefs, LifecycleImpl}
import art.team.dao.DaoModuleMongoRegistry

object CVParserApp
  extends LifecycleImpl
  with App
  with DefaultAppWideDefs
  with DaoModuleMongoRegistry
  with CVImporterCmp {

  val dir = args(0)

  Files.walkFileTree(
    Paths.get(dir),
    new SimpleFileVisitor[Path] {
      override def visitFile(path: Path, attrs: BasicFileAttributes): FileVisitResult = {
        val filename = path.getFileName.toString

        if ((filename endsWith ".doc") || (filename endsWith ".docx")) {
          cvImporter.importCV(path)
        }

        FileVisitResult.CONTINUE
      }
    }
  )

}
