package art.team.cvimporter.parsing

import java.util.Locale

/** Document specification.
  * @param sections top level sections
  * @tparam T parse result type */
case class DocumentSpec[T](sections: Section[T]*)

/** Section in document structure.
  * @param name section name
  * @param handler section handler
  * @param occurrence section occurrence
  * @param children sub-sections
  * @tparam T document parse result type */
case class Section[T](name: String,
                      handler: SectionHandler[T],
                      occurrence: Occurrence.Value = Occurrence.?,
                      children: Seq[Section[T]] = Seq.empty)

/** Document section handler. Constists of matcher and parser.
  * @tparam T document parse result type */
trait SectionHandler[T] {
  /** Section's specific result type. */
  type SectionResult

  /** Check whether supplied line matches this section.
    * @param line line of text
    * @param locale locale
    * @return boolean value */
  def matches(line: String, locale: Locale = Locales.En): Boolean

  /** Parse section.
    * @param lines input sequence of lines
    * @param locale locale
    * @return updated parser result */
  def parse(lines: Seq[String], locale: Locale = Locales.En): SectionResult

  /** Merge parsed data into result.
    * @param result accumulated parser result chained through section handlers
    * @param sectionResult parsed value
    * @return updated result */
  def merge(result: T, sectionResult: SectionResult): T
}

/** Section occurence within document structure. */
object Occurrence extends Enumeration {
  /** Occurs at most once. */
  val ? = Value
  /** Exactly once. */
  val ! = Value
  /** Occurs any number of times. */
  val * = Value
}
