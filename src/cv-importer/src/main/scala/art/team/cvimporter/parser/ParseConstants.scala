package art.team.cvimporter.parser

import art.team.model.{CV, DateValue, NameValue}

object ParseConstants {
  val NL = System.lineSeparator
  val Hyphens = Set("-", "–")

  val MinYear = 1950
  val DefaultYear = MinYear
  val DefaultMonth = 1

  val DefaultNameValue = NameValue("", "")
  val DefaultDateValue = DateValue(DefaultYear, DefaultMonth)

  val DefaultCV = CV(
    DefaultNameValue,
    "",
    "",
    "",
    Vector.empty,
    Vector.empty,
    Vector.empty,
    Vector.empty,
    Vector.empty,
    Vector.empty,
    Vector.empty
  )
}
