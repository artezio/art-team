package art.team.cvimporter.parsing

import java.util.Locale

import scala.annotation.tailrec

/** Document parser.
  * Can be used to parse non-strict grammars, like semi-structured text.
  * @tparam T parser result type
  * @see [[art.team.cvimporter.parsing.DocumentSpec]] */
class DocumentParser[T] {
  /** Parse document according to supplied specification.
    * @param lines document lines
    * @param spec document spec
    * @param init initial parse result
    * @param locale locale
    * @return document parse result */
  def parse(lines: Seq[String], spec: DocumentSpec[T], init: T, locale: Locale = Locales.En): T = {
    val matchedSections = splitSections(lines.toVector, spec.sections, locale)
    matchedSections.foldLeft(init) {
      case (result, sec) =>
        val handler = sec.section.handler
        val parseResult = handler.parse(sec.lines, locale)
        handler.merge(result, parseResult)
    }
  }

  // private

  private case class MatchedSection(lines: Vector[String], section: Section[T])

  private def splitSections(lines: Vector[String],
                            sections: Seq[Section[T]],
                            locale: Locale): Vector[MatchedSection] =
    if (sections.isEmpty) Vector.empty
    else {
      val topLevelSections = splitTopLevelSections(lines, sections, locale)
      topLevelSections flatMap { sec =>
        sec +: splitSections(sec.lines, sec.section.children, locale) // eats stack
      }
    }

  private def splitTopLevelSections(lines: Vector[String],
                                    sections: Seq[Section[T]],
                                    locale: Locale): Vector[MatchedSection] = {
    @tailrec
    def loop(lines: Vector[String],
             sections: Seq[Section[T]],
             acc: Vector[MatchedSection]): Vector[MatchedSection] =
      if (lines.isEmpty) acc
      else {
        val currentLine = lines.head
        val remainingLines = lines.tail
        val expectedSections = getExpectedSections(sections)
        val matchedSection = expectedSections find (_.handler.matches(currentLine, locale))

        if (matchedSection.isDefined) {
          val matched = matchedSection.get
          val sectionsLeft = sections dropWhile (section => !(section eq matched))
          loop(
            remainingLines,
            matchedSection.get.occurrence match {
              case Occurrence.* => sectionsLeft
              case Occurrence.! => if (sectionsLeft.isEmpty) Seq.empty else sectionsLeft.tail
              case _ => sectionsLeft.tail
            },
            acc :+ MatchedSection(Vector(currentLine), matched)
          )
        } else {
          val acc2 =
            if (acc.isEmpty) acc
            else {
              val last = acc.last
              acc.init :+ last.copy(lines = last.lines :+ currentLine) // add line to last matched section
            }
          loop(remainingLines, sections, acc2)
        }
      }

    def getExpectedSections(sections: Seq[Section[T]]): Seq[Section[T]] = {
      val (optional, required) = sections.toVector span (_.occurrence != Occurrence.!)
      required.headOption map (optional :+ _) getOrElse optional
    }

    loop(lines, sections, Vector.empty)
  }
}
