package art.team.cvimporter

import java.nio.file.{Files, Path}

import org.apache.poi.hwpf.extractor.WordExtractor
import resource.managed

object CVExtractor {
  def extract(path: Path): Option[String] =
    managed(Files.newInputStream(path)).map(new WordExtractor(_).getText).opt
}
