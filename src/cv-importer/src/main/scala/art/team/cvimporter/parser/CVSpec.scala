package art.team.cvimporter.parser

import art.team.cvimporter.parsing.{DocumentSpec, Occurrence, Section}
import art.team.model.CV

private[parser] object CVSpec extends DocumentSpec[CV](

  Section("name", SectionHandlers.nameSectionHandler),
  Section("position", SectionHandlers.positionSectionHandler),
  Section("summary", SectionHandlers.summarySectionHandler),
  Section("expertise", SectionHandlers.expertiseSectionHandler),

  Section(
    "professional experience", SectionHandlers.experienceSectionHandler,
    occurrence = Occurrence.!, children = Seq(
      Section("project", SectionHandlers.projectSectionHandler, Occurrence.*, children = Seq(
        Section("project description", SectionHandlers.projectDescriptionSectionHandler),
        Section("position on project", SectionHandlers.projectPositionSectionHandler),
        Section("responsibilities", SectionHandlers.projectResponsibilitiesSectionHandler),
        Section("technical summary", SectionHandlers.projectTechSummarySectionHandler)
      ))
    )),

  Section("education", SectionHandlers.educationSectionHandler, children = Seq(
    Section("education record", SectionHandlers.educationRecordSectionHandler, Occurrence.*)
  )),

  Section("certificates and trainings", SectionHandlers.certificatesSectionHandler),
  Section("languages", SectionHandlers.languagesSectionHandler),
  Section("additional info", SectionHandlers.additionalInfoSectionHandler)

)
