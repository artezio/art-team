package art.team.cvimporter

import java.nio.file.Path

import art.team.app.AppWideDefs
import art.team.cvimporter.parser.CVParser
import art.team.dao.CVDaoCmp
import art.team.model.Owner

trait CVImporterCmp {
  self: AppWideDefs with CVDaoCmp =>

  val cvImporter: CVImporter = new CVImporter

  class CVImporter {
    def importCV(path: Path): Unit = {
      val filename = path.getFileName.toString

      val lines = CVExtractor.extract(path)
      val cvOpt = lines flatMap CVParser.parse

      println(s"Importing $filename")

      cvOpt foreach {
        case (cv, language) =>
          cvDao.create(cv, getOwner(filename) , language)
      }

      if (cvOpt.isEmpty) {
        println(s"Failed to import: $filename")
      }
    }

    // private

    private def getOwner(filename: String): Owner = {
      val split = filename.substring(0, filename.indexOf(".")).split('_')
      val firstname = split(0).toLowerCase
      val lastname = split(1).toLowerCase
      Owner(firstname(0) + lastname)
    }
  }

}
