package art.team.cvimporter.parsing

import java.util.Locale

object Locales {
  val En = Locale.ENGLISH
  val Ru = new Locale("ru", "RU")

  val All = Seq(En, Ru)
}
