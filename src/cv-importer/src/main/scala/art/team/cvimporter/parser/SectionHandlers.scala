package art.team.cvimporter.parser

import java.util.Locale

import art.team.cvimporter.parser.ParseConstants._
import art.team.cvimporter.parsing.ParseUtils._
import art.team.cvimporter.parsing.SectionHandler
import art.team.model._
import org.joda.time.DateTime

trait SectionHandlers {
  val nameSectionHandler = new SectionHandler[CV] {
    override type SectionResult = NameValue
    override def matches(line: String, locale: Locale): Boolean =
      line.split("\\s+").size == 2
    override def parse(lines: Seq[String], locale: Locale): NameValue = {
      val words = lines.head.split("\\s+")
      if (words.size == 2) NameValue(words(0), words(1)) else DefaultNameValue
    }
    override def merge(result: CV, name: NameValue): CV =
      result.copy(name = name)
  }

  val positionSectionHandler = new SectionHandler[CV] {
    override type SectionResult = String
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.position(locale))
    override def parse(lines: Seq[String], locale: Locale): String =
      lines.tail.headOption getOrElse ""
    override def merge(result: CV, position: String): CV =
      result.copy(position = position)
  }

  val summarySectionHandler = new SectionHandler[CV] {
    override type SectionResult = Vector[String]
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.summary(locale))
    override def parse(lines: Seq[String], locale: Locale): Vector[String] =
      lines.tail.toVector map (skipToTab(_, tabRequred = false).trim)
    override def merge(result: CV, summary: Vector[String]): CV =
      result.copy(summary = summary)
  }

  val expertiseSectionHandler = new SectionHandler[CV] {
    override type SectionResult = Vector[ExpertiseRecord]
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.expertise(locale))
    override def parse(lines: Seq[String], locale: Locale): Vector[ExpertiseRecord] =
      lines.tail.toVector map { line =>
        val area = indexOf(line, ":").fold(line) { index => line.substring(0, index) }
        ExpertiseRecord(area, Vector.empty)
      }
    override def merge(result: CV, expertise: Vector[ExpertiseRecord]): CV =
      result.copy(expertise = expertise)
  }

  val experienceSectionHandler = new SectionHandler[CV] {
    override type SectionResult = Unit
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.professionalExp(locale))
    override def parse(lines: Seq[String], locale: Locale): Unit = ()
    override def merge(result: CV, meh: Unit): CV = result
  }

  val projectSectionHandler = new SectionHandler[CV] {
    override type SectionResult = Project
    override def matches(line: String, locale: Locale): Boolean = startsWithDate(line)
    override def parse(lines: Seq[String], locale: Locale): Project = {
      val strings = lines(0) split "\t"
      val (from, to) =
        if (strings.length > 0) parseDateRange(strings(0), locale)
        else (DefaultDateValue, DefaultDateValue)
      val company = if (strings.length > 1) strings(1).trim else ""
      Project(from, to, "", shorten(company), "", "", "", Vector.empty)
    }
    override def merge(result: CV, project: Project): CV =
      result.copy(projects = result.projects :+ project)
  }

  val projectDescriptionSectionHandler = new SectionHandler[CV] {
    override type SectionResult = String
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.projectDescription(locale))
    override def parse(lines: Seq[String], locale: Locale): String = skipToTab(lines.head)
    override def merge(result: CV, description: String): CV = {
      val project = getLastProject(result) map (_.copy(name = shorten(description), description = description))
      updateLastProject(result, project)
    }
  }

  val projectPositionSectionHandler = new SectionHandler[CV] {
    override type SectionResult = String
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.projectPosition(locale))
    override def parse(lines: Seq[String], locale: Locale): String = skipToTab(lines.head)
    override def merge(result: CV, position: String): CV = {
      val project = getLastProject(result) map (_.copy(position = position))
      updateLastProject(result, project)
    }
  }

  val projectResponsibilitiesSectionHandler = new SectionHandler[CV] {
    override type SectionResult = String
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.projectResponsibilities(locale))
    override def parse(lines: Seq[String], locale: Locale): String = skipToTab(lines.head)
    override def merge(result: CV, responsibilities: String): CV = {
      val project = getLastProject(result) map (_.copy(responsibilities = responsibilities))
      updateLastProject(result, project)
    }
  }

  val projectTechSummarySectionHandler = new SectionHandler[CV] {
    override type SectionResult = Vector[String]
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.projectTechSummary(locale))
    override def parse(lines: Seq[String], locale: Locale): Vector[String] = {
      (skipToTab(lines.head).split(",").view filter (!_.isEmpty) map (_.trim)).toVector
    }
    override def merge(result: CV, tags: Vector[String]): CV = {
      val project = getLastProject(result) map (_.copy(tags = tags))
      updateLastProject(result, project)
    }
  }

  val educationSectionHandler = new SectionHandler[CV] {
    override type SectionResult = Unit
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.education(locale))
    override def parse(lines: Seq[String], locale: Locale): Unit = ()
    override def merge(result: CV, education: Unit): CV = result
  }

  val educationRecordSectionHandler = new SectionHandler[CV] {
    override type SectionResult = EducationRecord
    override def matches(line: String, locale: Locale): Boolean = startsWithDate(line)
    override def parse(lines: Seq[String], locale: Locale): EducationRecord = {
      val strings = lines(0) split "\t"
      val (from, to) =
        if (strings.length > 0) parseDateRange(strings(0), locale)
        else (DefaultDateValue, DefaultDateValue)
      val institution = if (strings.length > 1) strings(1).trim else ""
      EducationRecord(from.year, to.year, institution)
    }
    override def merge(result: CV, educationRecord: EducationRecord): CV =
      result.copy(education = result.education :+ educationRecord)
  }

  val certificatesSectionHandler = new SectionHandler[CV] {
    override type SectionResult = Vector[CertificateRecord]
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.certificates(locale))
    override def parse(lines: Seq[String], locale: Locale): Vector[CertificateRecord] =
      lines.tail.toVector map { line =>
        CertificateRecord(line, DefaultYear)
      }
    override def merge(result: CV, certificates: Vector[CertificateRecord]): CV =
      result.copy(certificates = certificates)
  }

  val languagesSectionHandler = new SectionHandler[CV] {
    override type SectionResult = Vector[LanguageRecord]
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.languages(locale))
    override def parse(lines: Seq[String], locale: Locale): Vector[LanguageRecord] =
      lines.tail.toVector flatMap { line =>
        parseLanguage(line, locale).fold(Vector.empty: Vector[LanguageRecord]) { language =>
          Vector(LanguageRecord(language, LanguageProficiency.Intermediate))
        }
      }
    override def merge(result: CV, languages: Vector[LanguageRecord]): CV =
      result.copy(languages = languages)
  }

  val additionalInfoSectionHandler = new SectionHandler[CV] {
    override type SectionResult = Vector[String]
    override def matches(line: String, locale: Locale): Boolean =
      startsWith(line, ResourceLoader.additionalInfo(locale))
    override def parse(lines: Seq[String], locale: Locale): Vector[String] =
      lines.tail.toVector
    override def merge(result: CV, additionalInfo: Vector[String]): CV =
      result.copy(additionalInfo = additionalInfo)
  }

  // private

  private def startsWith(s: String, prefix: Vector[String]): Boolean = prefix exists s.toLowerCase.startsWith

  private def skipToTab(line: String, tabRequred: Boolean = true): String = {
    val textAfterTab = indexOf(line, "\t") map (i => line.substring(i + 1))
    textAfterTab getOrElse (if (tabRequred) "" else line)
  }

  private def startsWithDate(line: String): Boolean = {
    def isYear(s: String): Boolean = {
      val curYear = DateTime.now.getYear
      val curDecade = (curYear - 2000) / 10             // fix in next Millennium
      val regex = s"19[5-9][0-9]|20[0-$curDecade][0-9]" // fix in next Century
      s matches regex
    }

    (ParseConstants.Hyphens + "\t") exists {
      indexOf(line, _) exists {
        i =>
          splitToWords(line.substring(0, i)) exists isYear
      }
    }
  }

  private def parseDateRange(s: String, locale: Locale): (DateValue, DateValue) = {
    ParseConstants.Hyphens find { s.contains(_) } flatMap { hyphen =>
      indexOf(s, hyphen) map { i =>
        val from = s.substring(0, i)
        val to = s.substring(i + 1)
        (parseDate(from, locale), parseDate(to, locale))
      }
    } getOrElse ((DefaultDateValue, DefaultDateValue))
  }

  private[parser] def parseDate(date: String, locale: Locale): DateValue = {
    val words = splitToWords(date)
    val m = words.toSet.groupBy(wordKind)
    val nums = getOfKind(m, WordKind.Num)
    val alphas = getOfKind(m, WordKind.Alpha)

    val year = lookupYear(nums)
    val month = lookupAlphaMonth(alphas, locale) orElse lookupNumericMonth(nums)
    DateValue(year getOrElse DefaultYear, month getOrElse DefaultMonth)
  }

  private object WordKind extends Enumeration {
    type Type = Value
    val Alpha, Num, Alphanum = Value
  }

  private def wordKind(w: String): WordKind.Type =
    if (w.forall(_.isLetter)) WordKind.Alpha
    else if (w.forall(_.isDigit)) WordKind.Num
    else WordKind.Alphanum

  private def getOfKind(m: Map[WordKind.Type, Set[String]], kind: WordKind.Type): Set[String] =
    m.getOrElse(kind, Set())

  private def lookupYear(candidates: Set[String]): Option[Int] =
    candidates map (_.toInt) find (y => y >= MinYear && y <= DateTime.now.getYear)

  private def lookupAlphaMonth(candidates: Set[String], locale: Locale): Option[Int] = {
    val months = ResourceLoader.months(locale)
    val invertedMonths = months flatMap {
      case (m, spellings) => spellings map ((_, m))
    }
    val result = for {
      candidate <- candidates
      monthNo   <- invertedMonths.get(candidate)
    } yield monthNo
    result.headOption
  }

  private def lookupNumericMonth(candidates: Set[String]): Option[Int] =
    candidates map (_.toInt) find (m => m >= 1 && m <= 12)

  private def getLastProject(cv: CV): Option[Project] = cv.projects.lastOption
  private def updateLastProject(cv: CV, project: Option[Project]): CV =
    project map (p => cv.copy(projects = cv.projects.init :+ p)) getOrElse cv

  private def shorten(s: String, max: Int = 40): String =
    if (s.length > max) s.substring(0, max) + "..." else s

  private def parseLanguage(line: String, locale: Locale): Option[Language.Value] = {
    val effectiveLine = line.toLowerCase
    if (effectiveLine.contains(ResourceLoader.english(locale))) Some(Language.English)
    else if (effectiveLine.contains(ResourceLoader.russian(locale))) Some(Language.Russian)
    else None
  }
}

object SectionHandlers extends SectionHandlers
