package art.team.cvimporter.parsing

object ParseUtils {
  def indexOf(s: String, substring: String): Option[Int] = {
    val i = s.indexOf(substring)
    if (i == -1) None else Some(i)
  }

  def splitToWords(s: String): Array[String] =
    s.toLowerCase.split(WordSeparatorsRegex)

  // private

  private val WordSeparatorsRegex = Seq("-", "_", ",", "\\.", ";", ":", "/", "\\|", " ", "\t").mkString("|")
}
