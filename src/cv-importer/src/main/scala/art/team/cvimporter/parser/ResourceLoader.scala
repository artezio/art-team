package art.team.cvimporter.parser

import java.util.{Locale, ResourceBundle}

import art.team.cvimporter.parsing.Locales

object ResourceLoader {
  /** Get localized 'position' section header labels. */
  def position(locale: Locale): Vector[String] = getStrings(locale, "position")
  /** Get localized 'summary' section header labels. */
  def summary(locale: Locale): Vector[String] = getStrings(locale, "summary")
  /** Get localized 'expertise' section header labels. */
  def expertise(locale: Locale): Vector[String] = getStrings(locale, "expertise")

  /** Get localized 'professional experience' section header labels. */
  def professionalExp(locale: Locale): Vector[String] = getStrings(locale, "professional.experience")
  /** Get localized 'project description' section header labels. */
  def projectDescription(locale: Locale): Vector[String] = getStrings(locale, "project")
  /** Get localized 'project position' section header labels. */
  def projectPosition(locale: Locale): Vector[String] = getStrings(locale, "project.position")
  /** Get localized 'responsibilities' section header labels. */
  def projectResponsibilities(locale: Locale): Vector[String] = getStrings(locale, "project.responsibilities")
  /** Get localized 'tech summary' section header labels. */
  def projectTechSummary(locale: Locale): Vector[String] = getStrings(locale, "project.tech.summary")

  /** Get localized 'educational background' section header labels. */
  def education(locale: Locale): Vector[String] = getStrings(locale, "educational.background")
  /** Get localized 'certificates and trainings' section header labels. */
  def certificates(locale: Locale): Vector[String] = getStrings(locale, "certificates")

  /** Get localized 'languages' section header labels. */
  def languages(locale: Locale): Vector[String] = getStrings(locale, "languages")

  /** Get localized 'additional info' section header labels. */
  def additionalInfo(locale: Locale): Vector[String] = getStrings(locale, "additional.info")

  /** Map month number to its spelling variants.
    * Longer spellings come first, i.e., 'January', then 'Jan'. */
  def months(locale: Locale): Map[Int, Vector[String]] = {
    val bundle = bundles(locale)
    val pairs = (1 to 12) map { i =>
      val variants = bundle.getString("month." + i)
      (i, variants.split(Separator).toVector)
    }
    pairs.toMap
  }

  def english(locale: Locale): String = getString(locale, "language.en")
  def russian(locale: Locale): String = getString(locale, "language.ru")

  // private

  private val bundles = {
    def getBundle(locale: Locale): ResourceBundle = ResourceBundle.getBundle("cvparser.props", locale)
    (Locales.All map ((locale: Locale) => (locale, getBundle(locale)))).toMap
  }

  private val Separator = ';'

  private def getString(locale: Locale, key: String): String = bundles(locale).getString(key)
  private def getStrings(locale: Locale, key: String): Vector[String] =
    (getString(locale, key) split Separator).toVector
}
