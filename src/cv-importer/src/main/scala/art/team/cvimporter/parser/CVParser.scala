package art.team.cvimporter.parser

import java.nio.charset.Charset
import java.util.Locale

import art.team.cvimporter.parser.ParseConstants._
import art.team.cvimporter.parsing.ParseUtils._
import art.team.cvimporter.parsing.{DocumentParser, Locales}
import art.team.model.{CV, Language}

object CVParser {

  def parse(s: String): Option[(CV, Language.Value)] = for {
    (header, rest) <- skipHeader(s)
  } yield {
    val locale = guessLocale(header)
    (parseImpl(skipFooter(rest), locale), localeToLanguage(locale))
  }

  // private

  private def skipHeader(s: String): Option[(String, String)] =
    indexOf(s, NL + NL) map { i =>
      (s.take(i), s.drop(i).dropWhile(_.isWhitespace))
    }

  private def skipFooter(s: String): String = indexOf(s, "PAGE") map (s.substring(0, _)) getOrElse s

  private def guessLocale(header: String): Locale = {
    def isLatin(s: String): Boolean = Charset.forName("ISO-8859-1").newEncoder.canEncode(s)
    if (isLatin(header)) Locales.En else Locales.Ru
  }

  private def localeToLanguage(locale: Locale): Language.Value =
    if (locale.equals(Locales.Ru)) Language.Russian
    else Language.English

  private val parser = new DocumentParser[CV]
  private def parseImpl(input: String, locale: Locale): CV = {
    val lines = input.split(NL).view map (_.trim) filter (!_.isEmpty)
    parser.parse(lines, CVSpec, DefaultCV, locale)
  }
}
