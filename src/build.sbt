import common._
import dependencies._
import com.typesafe.sbt.SbtNativePackager._

name := "ART-TEAM"

lazy val common_ =
  DefProject("common").
    settings(libraryDependencies ++= commonDependencies)
lazy val commonFrontend =
  DefProject("common-frontend")

lazy val daoInterface =
  DefProject("dao-interface").
    dependsOn(common_ % "compile->compile;test->test").
    settings(libraryDependencies ++= daoInterfaceDependencies)
lazy val daoImpl =
  DefProject("dao-impl").
    dependsOn(daoInterface).
    dependsOn(common_ % "compile->compile;test->test").
    settings(libraryDependencies ++= daoImplDependencies)

lazy val cvImporter =
  DefProject("cv-importer").
    dependsOn(daoImpl).
    dependsOn(common_ % "compile->compile;test->test").
    settings(libraryDependencies ++= cvImporterDependencies)

lazy val cvExporterInterface = DefProject("cv-exporter-interface").
  dependsOn(common_ % "compile->compile;test->test").
  settings(libraryDependencies ++= cvExporterInterfaceDependencies)
lazy val cvExporterImpl =
  DefProject("cv-exporter-impl").
    dependsOn(cvExporterInterface).
    dependsOn(common_ % "compile->compile;test->test").
    settings(libraryDependencies ++= cvExporterImplDependencies)

lazy val serviceInterface =
  DefProject("service-interface").
    dependsOn(common_ % "compile->compile;test->test").
    settings(libraryDependencies ++= serviceInterfaceDependencies)
lazy val serviceImpl =
  DefProject("service-impl").
    dependsOn(serviceInterface).
    dependsOn(cvExporterInterface).
    dependsOn(common_ % "compile->compile;test->test").
    dependsOn(daoInterface).
    settings(libraryDependencies ++= serviceImplDependencies)

lazy val serverRenderer =
  DefProject("server-renderer").
    dependsOn(commonFrontend).
    dependsOn(serviceInterface).
    dependsOn(common_ % "compile->compile;test->test").
    dependsOn(RootProject(uri("git://github.com/Tvaroh/amd-scala.git"))).
    settings(libraryDependencies ++= serverRendererDependencies)

lazy val controller =
  DefProject("controller").
    dependsOn(serviceInterface).
    dependsOn(serverRenderer).
    dependsOn(common_ % "compile->compile;test->test").
    settings(libraryDependencies ++= controllerDependencies)

lazy val frontend =
  DefProject("frontend").
    dependsOn(commonFrontend)

lazy val mongoMigration =
  DefProject("mongo-migration").
    dependsOn(daoImpl).
    dependsOn(common_ % "compile->compile;test->test").
    settings(libraryDependencies ++= mongoMigrationDependencies)

lazy val app =
  DefProject("app").
    dependsOn(common_ % "compile->compile;test->test").
    dependsOn(daoInterface).
    dependsOn(daoImpl).
    dependsOn(cvExporterInterface).
    dependsOn(cvExporterImpl).
    dependsOn(serviceInterface).
    dependsOn(serviceImpl).
    dependsOn(controller).
    dependsOn(frontend).
    dependsOn(mongoMigration).
    dependsOn(RootProject(uri("git://github.com/Tvaroh/slf4j-log4j2.git"))).
    settings(libraryDependencies ++= appDependencies).
    settings(packageArchetype.java_application: _*)
