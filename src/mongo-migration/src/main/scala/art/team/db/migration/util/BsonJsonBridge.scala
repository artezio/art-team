package art.team.db.migration.util

import reactivemongo.bson._
import spray.json._

object BsonJsonBridge {

  def jsonToBson(json: JsValue): BSONValue = json match {
    case JsString(s) => BSONString(s)
    case JsArray(values) => BSONArray(values map jsonToBson)
    case JsObject(obj) => BSONDocument(obj.toList map { case (k, v) => k -> jsonToBson(v)})
    case JsNumber(number) => BSONDouble(number.toDouble)
    case JsBoolean(value) => BSONBoolean(value)
    case JsNull => BSONNull
    case _ => sys.error("Unsupported JSON field type")
  }

  def bsonToJson(bson: BSONValue): JsValue = bson match {
    case BSONString(value) => JsString(value)
    case arr@BSONArray(_) => JsArray(arr.values map bsonToJson: _*)
    case doc@BSONDocument(_) => JsObject(doc.elements map { case (k, v) => k -> bsonToJson(v)}: _*)
    case BSONDouble(value) => JsNumber(value)
    case BSONBoolean(value) => JsBoolean(value)
    case BSONNull => JsNull
    case _ => sys.error("Unsupported BSON field type")
  }

}
