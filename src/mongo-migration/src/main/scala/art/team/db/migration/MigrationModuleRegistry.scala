package art.team.db.migration

import art.team.app.AppWideConf
import art.team.dao.DaoModuleMongoRegistry
import art.team.db.MongoDB
import art.team.db.migration.service.MigrationSvcCmp
import art.team.db.migration.util.MigrationUtil

trait MigrationModuleRegistry
  extends MigrationUtil
  with ChangeLog
  with MigrationSvcCmp {

  self: AppWideConf with MongoDB with DaoModuleMongoRegistry =>
}
