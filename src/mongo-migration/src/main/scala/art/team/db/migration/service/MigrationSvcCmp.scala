package art.team.db.migration.service

import art.team.app.AppWideConf
import art.team.dao.DaoModuleMongoRegistry
import art.team.db.migration.ChangeLog
import art.team.db.migration.dao.MigrationDaoMongoCmp
import art.team.db.migration.model.Migration

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Promise, Future}

trait MigrationSvcCmp extends MigrationDaoMongoCmp {
  self: AppWideConf with DaoModuleMongoRegistry with ChangeLog =>

  val migrationSvc = new MigrationSvc()

  class MigrationSvc {
    def run(): Future[Boolean] = {
      val p = Promise[Boolean]()

      fetchNextRevision() map applyChangeSets(p)

      p.future
    }

    def runBlocking(timeout: Duration = Duration.Inf): Boolean =
      Await.result(run(), timeout)

    // private

    private def fetchNextRevision(): Future[Int] =
      migrationDao.findFailedRevision() flatMap { failedRevisionOpt =>
        failedRevisionOpt.fold {
          migrationDao.findCurrentRevision() map { currentRevisionOpt =>
            currentRevisionOpt map (_ + 1) getOrElse 1
          }
        }(Future.successful)
      }

    private def applyChangeSets(p: Promise[Boolean])(revision: Int): Unit = {
      val changeSetOpt = ChangeSets.get(revision)

      if (changeSetOpt.isEmpty) {
        p.success(true)
      } else {

        migrationDao.isSuccessful(revision) map { successfulOpt =>
          val successful = successfulOpt getOrElse false

          if (successful) {
            applyChangeSets(p)(revision + 1)
          } else {
            changeSetOpt foreach { changeSet =>
              applyChangeSet(
                revision,
                changeSet,
                onSuccess = applyChangeSets(p)(revision + 1),
                onFailure = p.success(false)
              )
            }
          }

        }

      }
    }

    private def applyChangeSet(revision: Int, changeSet: () => Future[_],
                               onSuccess: => Unit, onFailure: => Unit): Unit = {
      appLogger.info(s"Upgrading DB to revision #$revision")

      val result = changeSet()

      result onSuccess {
        case _ =>
          migrationDao.create(Migration(revision))
          appLogger.info(s"DB successfully upgraded to revision #$revision")
          onSuccess
      }

      result onFailure {
        case e =>
          val errorDesc = e.getMessage
          appLogger.error(s"DB upgrade to revision #$revision failed. Reason: $errorDesc")
          migrationDao.create(Migration(revision, successful = false, Some(errorDesc)))
          onFailure
      }
    }

  }

}
