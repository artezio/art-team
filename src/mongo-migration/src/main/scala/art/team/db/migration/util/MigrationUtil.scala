package art.team.db.migration.util

import art.team.app.AppWideConf
import play.api.libs.iteratee.Enumerator
import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.bson.{BSONWriter, BSON, BSONDocument}
import spray.json._

import scala.concurrent.Future

trait MigrationUtil {
  self: AppWideConf =>

  def bulkInsert[T: JsonReader, R: ({type λ[α] = BSONWriter[R, BSONDocument]})#λ](coll: BSONCollection,
                                                                                  filename: String,
                                                                                  decorator: T => R): Future[Int] = {
    val reader = implicitly[JsonReader[T]]

    val ts = readArray(filename, reader)
    val docs = ts map { t => BSON.write(decorator(t)) }

    bulkInsert(coll, docs)
  }

  // private

  private def jsonFromFile(filename: String): String = {
    val url = this.getClass.getResource(s"/changesets/$filename.json")
    scala.io.Source.fromURL(url).mkString
  }

  private def readArray[T](filename: String, reader: JsonReader[T]): Seq[T] = {
    val content = jsonFromFile(filename)
    val jsArray = content.parseJson.asInstanceOf[JsArray]
    jsArray.elements map { reader.read }
  }

  private def bulkInsert(coll: BSONCollection, docs: Seq[BSONDocument]): Future[Int] = {
    val enumerator = Enumerator.enumerate(docs)
    coll.bulkInsert(enumerator)
  }

}
