package art.team.db.migration

import art.team.app.AppWideConf
import art.team.db.Handlers._
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records.LocationRecord
import art.team.db.migration.util.MigrationUtil
import art.team.json.JsonFormats._
import art.team.model.Location
import reactivemongo.bson.{BSONString, BSONDocument}

import scala.concurrent.Future

trait ChangeLog {
  self: AppWideConf with MongoDB with MigrationUtil =>

  val ChangeSets: Map[Int, () => Future[_]] = Map(

    1 -> ChangeSet {
      val coll = collection(db, "location")
      bulkInsert[Location, LocationRecord](coll, "1-locations", location => LocationRecord(genId(), location))
    },

    2 -> ChangeSet {
      val coll = collection(db, "prefs")
      coll.update(BSONDocument(), mkSet(BSONDocument("prefs.defaultSection" -> BSONString("cv"))), multi = true)
    }

  )

}
