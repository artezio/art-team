package art.team.db.migration.dao

import art.team.app.{AppWideConf, Lifecycle}
import art.team.db.MongoDB
import art.team.db.MongoUtil._
import art.team.db.Records.{HasId, HasLastModified}
import art.team.db.migration.model.Migration
import org.joda.time.DateTime
import reactivemongo.bson._
import reactivemongo.core.commands._

import scala.concurrent.Future

trait MigrationDaoMongoCmp extends Lifecycle {
  self: AppWideConf with MongoDB =>

  val migrationDao = new MigrationDaoMongo

  abstract override def start(): Unit = {
    super.start()
    ensureIndex(
      migrationDao.coll, MigrationRecord.fields.Revision,
      options = mkDoc(Indexes.Unique -> BSONBoolean(value = true))
    )
  }

  abstract override def stop(): Unit = {
    super.stop()
  }

  class MigrationDaoMongo {

    def findFailedRevision(): Future[Option[Int]] = {
      val MinRevision = "minRevision"
      val agg = Aggregate(coll.name, Seq(
        Match(mkDoc(MigrationRecord.fields.Successful -> BSONBoolean(value = false))),
        Project(
          HasId.fields.Id -> BSONInteger(0),
          MigrationRecord.fields.Revision -> BSONInteger(1)
        ),
        GroupField("meh")(
          MinRevision -> Min(MigrationRecord.fields.Revision)
        )
      ))

      db.command(agg) map { _.headOption flatMap { _.getAs[Int](MinRevision) } }
    }

    def findCurrentRevision(): Future[Option[Int]] = {
      val MaxRevision = "maxRevision"
      val agg = Aggregate(coll.name, Seq(
        Project(
          HasId.fields.Id -> BSONInteger(0),
          MigrationRecord.fields.Revision -> BSONInteger(1)
        ),
        GroupField("meh")(
          MaxRevision -> Max(MigrationRecord.fields.Revision)
        )
      ))

      db.command(agg) map { _.headOption flatMap { _.getAs[Int](MaxRevision) } }
    }

    def isSuccessful(revision: Int): Future[Option[Boolean]] =
      coll.find(
        mkDoc(MigrationRecord.fields.Revision -> BSONInteger(revision)),
        mkDoc(MigrationRecord.fields.Successful -> BSONInteger(1))
      ).cursor[BSONDocument].headOption map { docOpt =>
        docOpt map { doc =>
          readPathAs[Boolean](doc, MigrationRecord.fields.Successful)
        }
      }

    def create(migration: Migration): Future[_] = {
      val query = mkDoc(MigrationRecord.fields.Revision -> BSONInteger(migration.revision))
      val update = mkDoc(
        HasLastModified.fields.LastModified -> BSON.write(DateTime.now()),
        MigrationRecord.fields.Migration -> BSON.write(migration)
      )
      db.command(
        FindAndModify(
          coll.name,
          query,
          Update(mkSet(update), fetchNewObject = false),
          upsert = true
        )
      )
    }

    // private

    private[MigrationDaoMongoCmp] lazy val coll = collection(db, "migration")

    private implicit val jodaHandler = art.team.db.Handlers.jodaHandler
    private implicit val migrationHandler = Macros.handler[Migration]
    private implicit val migrationRecordHandler = Macros.handler[MigrationRecord]
  }

  // private

  private case class MigrationRecord(override val _id: BSONObjectID,
                                     override val lastModified: DateTime,
                                     migration: Migration)
    extends HasId
    with HasLastModified
  private object MigrationRecord {
    object fields {
      val Migration = "migration"
      val Revision = s"$Migration.revision"
      val Successful = s"$Migration.successful"
    }
  }

}
