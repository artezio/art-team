package art.team.db.migration.model

case class Migration(revision: Int, successful: Boolean = true, errorDesc: Option[String] = None)
