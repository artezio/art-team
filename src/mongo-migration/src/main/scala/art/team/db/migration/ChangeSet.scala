package art.team.db.migration

import scala.concurrent.Future

object ChangeSet {
  def apply(code: => Future[_]) = () => code
}
