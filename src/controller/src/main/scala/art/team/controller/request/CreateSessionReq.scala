package art.team.controller.request

import spray.json.DefaultJsonProtocol

case class CreateSessionReq(username: String, password: String)

object CreateSessionReq extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat2(CreateSessionReq.apply)
}
