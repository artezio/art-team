package art.team.controller.response

import art.team.json.JsonFormats._
import art.team.model.Location
import spray.json.DefaultJsonProtocol

case class GetLocationsResp(locations: Seq[Location])

object GetLocationsResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(GetLocationsResp.apply)
}
