package art.team.controller.response

import art.team.dto.CVExcerpt
import art.team.json.JsonFormats._
import art.team.model.{Language, LocationCode, Owner}
import spray.json.DefaultJsonProtocol

case class CVOverviewResp(results: Map[LocationCode, Map[Owner, Map[Language.Value, CVExcerpt]]])

object CVOverviewResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(CVOverviewResp.apply)
}
