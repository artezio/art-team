package art.team.controller

import art.team.app.AppWideConf
import art.team.controller.request.UpdateUserReq
import art.team.controller.response.FetchRolesResp
import art.team.service.UserSvcCmp
import spray.http.HttpResponse
import spray.http.StatusCodes._
import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport

import scala.concurrent.Future

trait UserControllerCmp {
  self: AppWideConf with UserSvcCmp =>

  val userController: UserController = new UserController

  class UserController extends SprayJsonSupport {

    def fetchRoles(): Future[HttpResponse] =
      userSvc.fetchRoles() map { roles =>
        HttpResponse(OK, marshalUnsafe(FetchRolesResp(roles)))
      }

    def updateUser(username: String, req: UpdateUserReq, caller: String): Future[HttpResponse] =
      if (canUpdate(username, req, caller)) {
        userSvc.updateUser(username, req.role) map { updated =>
          val code = if (updated) OK else BadRequest
          HttpResponse(code)
        }
      } else Future.successful(HttpResponse(Forbidden))

    // private

    private def canUpdate(username: String, req: UpdateUserReq, caller: String): Boolean =
      !(username == caller && req.role.isDefined)

  }

}
