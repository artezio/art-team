package art.team.controller.request

import art.team.json.JsonFormats._
import art.team.model.{CV, Language}
import spray.json.DefaultJsonProtocol

case class SetDraftReq(cv: CV, language: Language.Value, version: Int)

object SetDraftReq extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat3(SetDraftReq.apply)
}
