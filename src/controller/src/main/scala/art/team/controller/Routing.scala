package art.team.controller

import art.team.app.AppWideConf
import art.team.auth.AuthenticatorRegistry
import art.team.common.Format
import art.team.controller.request._
import art.team.dto.SearchMode
import art.team.model.{Owner, Language}
import art.team.security.Task
import art.team.serverrenderer.model.NowShowing
import art.team.service.AuthSvcCmp
import spray.http.{DateTime, HttpHeader}
import spray.http.HttpHeaders.`If-Modified-Since`
import spray.routing.{Directive0, Route, RoutingSettings}

import scala.async.Async.{async, await}
import scala.concurrent.Future

trait Routing {
  self: AppWideConf with ControllerRegistry with AuthenticatorRegistry with AuthSvcCmp =>

  val routes =
    pathPrefix("api") {
      pathPrefix("session") {

        pathEndOrSingleSlash {
          post {
            entity(as[CreateSessionReq]) { req =>
              complete {
                sessionController.create(req)
              }
            }
          }
        } ~
        path(Rest) { sessionId =>
          delete {
            complete {
              sessionController.delete(sessionId)
            }
          }
        }

      } ~
      pathPrefix("ticket") {

        pathEndOrSingleSlash {
          authenticate(ArtAuthAuthenticator) { caller =>
            post {
              entity(as[String]) { data =>
                complete {
                  ticketController.create(caller, if (!data.isEmpty) Some(data) else None)
                }
              }
            }
          }
        }

      } ~
      pathPrefix("roles") {

        pathEndOrSingleSlash {
          authenticate(ArtAuthAuthenticator) { caller =>
            authorize(canReadRoles(caller)) {
              get {
                complete {
                  userController.fetchRoles()
                }
              }
            }
          }
        }

      } ~
      pathPrefix("user") {

        path(Rest) { username =>
          authenticate(ArtAuthAuthenticator) { caller =>
            authorize(canSetRoles(caller)) {
              patch {
                entity(as[UpdateUserReq]) { req =>
                  complete {
                    userController.updateUser(username, req, caller)
                  }
                }
              }
            }
          }
        }

      } ~
      pathPrefix("export" / "cv") {
        pathPrefix("pdf") {
          path(Rest) { owner =>
            exportTo(owner, Format.PDF)
          }
        } ~
        pathPrefix("docx") {
          path(Rest) { owner =>
            exportTo(owner, Format.Docx)
          }
        } ~
        path("zip") {

          authenticate(TicketAuthenticator) { ticketInfo =>
            authorize(canReadOtherPeopleCV(ticketInfo.username)) {
              val routingSettings = implicitly[RoutingSettings]
              autoChunk(routingSettings.fileChunkingThresholdSize, routingSettings.fileChunkingChunkSize) {
                complete {
                  cvController.groupExport(ticketInfo.data)
                }
              }
            }
          }

        } ~
        path("email") {
          pathEndOrSingleSlash {

            authenticate(ArtAuthAuthenticator) { caller =>
              post {
                entity(as[ExportToEmailReq]) { req =>
                  authorize(canExport(caller, req.spec)) {
                    complete {
                      cvController.exportToEmail(caller, req)
                    }
                  }
                }
              }
            }

          }
        }
      } ~
      authenticate(ArtAuthAuthenticator) { caller =>
        pathPrefix("cv") {

          pathPrefix("draft") {
            path(Rest) { owner =>

              authorize(canAccessCV(caller, owner)) {

                put {
                  entity(as[SetDraftReq]) { req =>
                    complete {
                      cvController.setDraft(req, Owner(owner), caller)
                    }
                  }
                } ~
                readLanguage { language =>
                  get {
                    complete {
                      cvController.readDraft(Owner(owner), language)
                    }
                  } ~
                  delete {
                    complete {
                      cvController.clearDraft(Owner(owner), language)
                    }
                  }
                }

              }

            }
          } ~
          path(Rest) { owner =>

            authorize(canAccessCV(caller, owner)) {

              put {
                entity(as[CreateOrUpdateCVReq]) { req =>
                  complete {
                    cvController.createOrUpdate(req, Owner(owner), caller)
                  }
                }
              } ~
              readLanguage { language =>
                get {
                  complete {
                    cvController.read(Owner(owner), language)
                  }
                }
              }

            }

          } ~
          pathEnd {

            authorize(canReadOtherPeopleCV(caller)) {

              get {
                optionalHeaderValue(extractIfModifiedSince) { ifModifiedSinceOpt =>
                  compressResponseIfRequested() {
                    complete {
                      cvController.fetchExcerpts(ifModifiedSinceOpt map toJodaTime)
                    }
                  }
                }
              }

            }

          }
        } ~
        pathPrefix("cvs") {
          authorize(canReadOtherPeopleCV(caller)) {

            get {
              parameter('query) { query =>
                parameter('mode.?) { modeOpt =>
                  complete {
                    cvController.find(query, modeOpt.fold(SearchMode.And)(SearchMode.withName))
                  }
                }
              }
            }

          }
        } ~
        pathPrefix("prefs") {
          pathEnd {

            get {
              complete {
                prefsController.readPrefs(caller)
              }
            } ~
            put {
              entity(as[SetPrefsReq]) { req =>
                complete {
                  prefsController.setPrefs(req, caller)
                }
              }
            }

          }
        } ~
        pathPrefix("location") {
          pathEnd {

            get {
              complete {
                cvController.fetchAvailableLocations()
              }
            }

          }
        }
      }
    } ~
    parameter('serverrender) { _ =>
      optionalCookie("username") { usernameOpt =>
        pathPrefix("login") {
          pathEnd {

            get {
              complete {
                serverRendererController.render(NowShowing.Login, usernameOpt map (_.content))
              }
            }

          }
        }
      }
    } ~
    pathEndOrSingleSlash(Index) ~
    pathPrefix("login" | "logout" | "permissionviolation" | "notfound" | "preferences" | "cv" | "cvlist" | "pickup")(Index) ~
    compressResponseIfRequested() {
      getFromResourceDirectory("frontend")
    }

  // private

  private lazy val Index = getFromResource("frontend/index.html")

  private def authorize(authorizer: => Future[Boolean]): Directive0 =
    onSuccess(authorizer).flatMap(check => authorize(check))

  private def canPerform(caller: String, task: Task.Value): Future[Boolean] = async {
    await(authSvc.canPerform(caller, task)) match {
      case Right(true) =>
        true
      case _ =>
        false
    }
  }

  private def canReadOtherPeopleCV(caller: String): Future[Boolean] = canPerform(caller, Task.ReadOtherPeopleCV)
  private def canReadRoles(caller: String): Future[Boolean] = canPerform(caller, Task.ReadRoles)
  private def canSetRoles(caller: String): Future[Boolean] = canPerform(caller, Task.SetRoles)

  private def canExport(caller: String, spec: GroupExportSpec): Future[Boolean] =
    if (spec.size == 1 && spec.contains(new Owner(caller))) Future.successful(true)
    else canReadOtherPeopleCV(caller)

  private def canAccessCV(caller: String, owner: String): Future[Boolean] =
    if (caller == owner) Future.successful(true)
    else canReadOtherPeopleCV(caller)

  private def readLanguage(f: Language.Value => Route): Route =
    parameter('language.?) { languageOpt =>
      val language = languageOpt.fold(Language.English)(Language.withName)
      f(language)
    }

  private def extractIfModifiedSince: HttpHeader => Option[DateTime] = {
    case `If-Modified-Since`(date) => Some(date)
    case x => None
  }

  private def exportTo(owner: String, format: Format.Value): Route = {
    authenticate(TicketAuthenticator) { ticketInfo =>
      authorize(canAccessCV(ticketInfo.username, owner)) {
        readLanguage { language =>
          val routingSettings = implicitly[RoutingSettings]
          autoChunk(routingSettings.fileChunkingThresholdSize, routingSettings.fileChunkingChunkSize) {
            complete {
              cvController.export(Owner(owner), language, format)
            }
          }
        }
      }
    }
  }

  private def toJodaTime(dt: DateTime): org.joda.time.DateTime =
    new org.joda.time.DateTime(dt.clicks)

}
