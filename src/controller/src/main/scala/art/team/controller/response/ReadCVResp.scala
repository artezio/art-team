package art.team.controller.response

import art.team.json.JsonFormats._
import art.team.model.{CV, Language}
import org.joda.time.DateTime
import spray.json.DefaultJsonProtocol

case class ReadCVResp(cv: CV, language: Language.Value, version: Int, lastModified: DateTime, lastModifiedBy: String)

object ReadCVResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat5(ReadCVResp.apply)
}
