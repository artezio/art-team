package art.team.controller

import art.team.service.{Failure, GenericFailure, InternalServerError}
import spray.http.{HttpResponse, StatusCode, StatusCodes}

import scala.concurrent.{ExecutionContext, Future}

object ControllerUtil {
  def toResponse[T, V](result: Future[Option[T]],
                       errorResponse: HttpResponse = HttpResponse(StatusCodes.InternalServerError))
                      (f: T => HttpResponse)
             (implicit executionContext: ExecutionContext): Future[HttpResponse] =
    result map {
      case Some(t) =>
        f(t)
      case None =>
        errorResponse
    }

  def translateFailure(failure: Failure, genericFailureErrorCode: StatusCode): HttpResponse = {
    val code = failure match {
      case GenericFailure =>
        genericFailureErrorCode
      case InternalServerError =>
        StatusCodes.InternalServerError
    }
    HttpResponse(code)
  }
}
