package art.team.controller

import art.team.app.AppWideDefs
import art.team.controller.ControllerUtil._
import art.team.controller.response.CreateTicketResponse
import art.team.service.TicketSvcCmp
import spray.http.{HttpResponse, StatusCodes}
import spray.httpx.SprayJsonSupport
import spray.httpx.marshalling._
import spray.routing.HttpService

import scala.concurrent.Future

trait TicketControllerCmp extends HttpService with SprayJsonSupport {
  self: AppWideDefs with TicketSvcCmp =>

  val ticketController = new TicketController

  class TicketController {

    def create(caller: String, data: Option[String]): Future[HttpResponse] =
      toResponse(ticketSvc.create(caller, data)) { ticketId =>
        HttpResponse(StatusCodes.OK, marshalUnsafe(CreateTicketResponse(ticketId)))
      }

  }

}
