package art.team.controller.response

import art.team.dto.CVExcerpt
import art.team.json.JsonFormats._
import art.team.model.{Owner, Language, LocationCode}
import spray.json.DefaultJsonProtocol

case class FindCVsResp(results: Map[Language.Value, Map[LocationCode, Map[Owner, CVExcerpt]]])

object FindCVsResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(FindCVsResp.apply)
}
