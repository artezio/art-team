package art.team.controller.response

import art.team.model.Prefs
import art.team.json.JsonFormats._
import spray.json.DefaultJsonProtocol

case class ReadPrefsResp(prefs: Prefs)

object ReadPrefsResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(ReadPrefsResp.apply)
}
