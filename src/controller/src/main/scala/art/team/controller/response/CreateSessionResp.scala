package art.team.controller.response

import art.team.json.JsonFormats._
import art.team.model.Role
import spray.json.DefaultJsonProtocol

case class CreateSessionResp(sessionId: String, role: Role.Value)

object CreateSessionResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat2(CreateSessionResp.apply)
}
