package art.team.controller

import art.team.app.AppWideDefs
import art.team.controller.ControllerUtil._
import art.team.controller.request.SetPrefsReq
import art.team.controller.response.ReadPrefsResp
import art.team.service.PrefsSvcCmp
import spray.http.HttpResponse
import spray.http.StatusCodes._
import spray.httpx.SprayJsonSupport
import spray.httpx.marshalling._

import scala.concurrent.Future

trait PrefsControllerCmp {
  self: AppWideDefs with PrefsSvcCmp =>

  val prefsController = new PrefsController

  class PrefsController extends SprayJsonSupport {

    def setPrefs(req: SetPrefsReq, caller: String): Future[HttpResponse] =
      prefsSvc.setPrefs(caller, req.prefs) map (_ => HttpResponse(OK))

    def readPrefs(username: String): Future[HttpResponse] =
      toResponse(prefsSvc.readPrefs(username), HttpResponse(NotFound)) {
        prefs =>
          HttpResponse(OK, marshalUnsafe(ReadPrefsResp(prefs)))
      }

  }
}
