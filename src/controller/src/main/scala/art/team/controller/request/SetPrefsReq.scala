package art.team.controller.request

import art.team.json.JsonFormats._
import art.team.model.Prefs
import spray.json.DefaultJsonProtocol

case class SetPrefsReq(prefs: Prefs)

object SetPrefsReq extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(SetPrefsReq.apply)
}
