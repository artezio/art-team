package art.team.controller

import art.team.app.AppWideDefs
import art.team.serverrenderer.ServerRendererCmp
import art.team.serverrenderer.model.NowShowing
import spray.http.HttpCharsets._
import spray.http.MediaTypes._
import spray.http.StatusCodes._
import spray.http.{HttpEntity, ContentType, HttpResponse}

import scala.concurrent.Future

trait ServerRendererControllerCmp {
  self: AppWideDefs with ServerRendererCmp =>

  val serverRendererController = new ServerRendererController

  class ServerRendererController {

    def render(nowShowing: NowShowing.Value, username: Option[String]): Future[HttpResponse] =
      serverRenderer.render(nowShowing, username) map { renderResult =>
        HttpResponse(
          OK,
          HttpEntity(ContentType(`text/html`, `UTF-8`), renderResult)
        )
      }

  }

}
