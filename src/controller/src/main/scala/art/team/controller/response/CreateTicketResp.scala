package art.team.controller.response

import spray.json.DefaultJsonProtocol

case class CreateTicketResponse(ticketId: String)

object CreateTicketResponse extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(CreateTicketResponse.apply)
}
