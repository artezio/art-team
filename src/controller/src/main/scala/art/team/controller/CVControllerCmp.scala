package art.team.controller

import art.team.app.AppWideDefs
import art.team.common.Format
import art.team.controller.ControllerUtil._
import art.team.controller.request._
import art.team.controller.response._
import art.team.dto.{CVExcerpt, SearchMode}
import art.team.model.{CV, Language, LocationCode, Owner}
import art.team.service.CVSvcCmp
import spray.caching.{Cache, LruCache}
import spray.http.HttpHeaders.{`Content-Disposition`, `Last-Modified`}
import spray.http.MediaTypes._
import spray.http.StatusCodes._
import spray.http.{DateTime, HttpEntity, HttpResponse, MediaType}
import spray.httpx.SprayJsonSupport
import spray.httpx.marshalling._
import spray.json._

import scala.async.Async.{async, await}
import scala.concurrent.Future
import scala.concurrent.duration._

trait CVControllerCmp {
  self: AppWideDefs with CVSvcCmp =>

  val cvController = new CVController

  class CVController extends SprayJsonSupport {

    def createOrUpdate(req: CreateOrUpdateCVReq, owner: Owner, caller: String): Future[HttpResponse] =
      if (req.version == 0) createCV(req.cv, owner, req.language)
      else update(req.cv, owner, req.language, req.version, caller)

    def read(owner: Owner, language: Language.Value): Future[HttpResponse] =
      toResponse(cvSvc.read(owner, language), HttpResponse(NotFound)) {
        case (cv, version, lastModified, lastModifiedBy) =>
          HttpResponse(OK, marshalUnsafe(ReadCVResp(cv, language, version, lastModified, lastModifiedBy)))
      }

    def setDraft(req: SetDraftReq, owner: Owner, caller: String): Future[HttpResponse] =
      cvSvc.setDraft(req.cv, owner, req.language, req.version, caller) map (_ => HttpResponse(OK))

    def readDraft(owner: Owner, language: Language.Value): Future[HttpResponse] =
      toResponse(cvSvc.readDraft(owner, language), HttpResponse(NotFound)) {
        case (cv, version, lastModified, lastModifiedBy) =>
          HttpResponse(OK, marshalUnsafe(ReadCVResp(cv, language, version, lastModified, lastModifiedBy)))
      }

    def clearDraft(owner: Owner, language: Language.Value): Future[HttpResponse] =
      cvSvc.clearDraft(owner, language) map (_ => HttpResponse(OK))

    def fetchAvailableLocations(): Future[HttpResponse] =
      cvSvc.fetchAvailableLocations() map { locations =>
        HttpResponse(OK, marshalUnsafe(GetLocationsResp(locations)))
      }

    def fetchExcerpts(ifModifiedSinceOpt: Option[org.joda.time.DateTime]): Future[HttpResponse] = {
      ExcerptsCache('any) {
        cvSvc.fetchExcerpts() map { results =>
          (results, org.joda.time.DateTime.now().withMillisOfSecond(0))
        }
      } map {
        case (excerpts, lastModified) =>
          val modified = ifModifiedSinceOpt.fold(true) { lastModified.isAfter }

          if (modified)
            HttpResponse(
              OK,
              marshalUnsafe(CVOverviewResp(excerpts)),
              headers = List(
                `Last-Modified`(DateTime(lastModified.getMillis))
              )
            )
          else HttpResponse(NotModified)
      }

    }

    def find(query: String, mode: SearchMode.Value): Future[HttpResponse] =
      cvSvc.find(query, mode) map { results =>
        HttpResponse(OK, marshalUnsafe(FindCVsResp(results)))
      }

    def export(owner: Owner, language: Language.Value, format: Format.Value): Future[HttpResponse] =
      cvSvc.export(owner, language, format) map {
        case Some(export) =>
          HttpResponse(
            OK,
            HttpEntity(getMediaType(format), export.bytes),
            headers = List(
              `Content-Disposition`("attachment", Map("filename" -> export.filename)),
              `Last-Modified`(DateTime(export.lastModified.getMillis))
            )
          )
        case None =>
          HttpResponse(NotFound)
      }

    def groupExport(specStringOpt: Option[String]): Future[HttpResponse] =
      specStringOpt map { specString =>
        val spec = specString.parseJson.convertTo[GroupExportSpec]
        cvSvc.groupExport(spec) map {
          case Some(export) =>
            HttpResponse(
              OK,
              HttpEntity(`application/zip`, export.bytes),
              headers = List(
                `Content-Disposition`("attachment", Map("filename" -> export.filename))
              )
            )
          case None =>
            HttpResponse(NotFound)
        }
      } getOrElse Future.successful(HttpResponse(BadRequest))

    def exportToEmail(caller: String, req: ExportToEmailReq): Future[HttpResponse] =
      cvSvc.exportToEmail(caller, req.spec) map { _ => HttpResponse(OK) }

    // private

    private lazy val ExcerptsCache: Cache[(Map[LocationCode, Map[Owner, Map[Language.Value, CVExcerpt]]], org.joda.time.DateTime)] =
      LruCache(timeToLive = 30.seconds)

    private def createCV(cv: CV, owner: Owner, language: Language.Value): Future[HttpResponse] =
      toResponse(cvSvc.create(cv, owner, language)) { version =>
        HttpResponse(Created, marshalUnsafe(CreateOrUpdateCVResp(version)))
      }

    private def update(cv: CV, owner: Owner, language: Language.Value,
                       version: Int, caller: String): Future[HttpResponse] = async {
      await(cvSvc.update(cv, owner, language, version, caller)) match {
        case Right(nextVersion) =>
          HttpResponse(OK, marshalUnsafe(CreateOrUpdateCVResp(nextVersion)))
        case Left(failure) =>
          translateFailure(failure, NotFound)
      }
    }

    private def getMediaType(format: Format.Value): MediaType = format match {
      case Format.PDF => `application/pdf`
      case Format.Docx => MediaType.custom("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
    }

  }
}
