package art.team.controller.request

import art.team.json.JsonFormats.roleFormat
import art.team.model.Role
import spray.json.DefaultJsonProtocol

case class UpdateUserReq(role: Option[Role.Value])

object UpdateUserReq extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(UpdateUserReq.apply)
}
