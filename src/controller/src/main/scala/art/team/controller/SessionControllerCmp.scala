package art.team.controller

import art.team.app.AppWideDefs
import art.team.controller.ControllerUtil._
import art.team.controller.request.CreateSessionReq
import art.team.controller.response.CreateSessionResp
import art.team.service.{AuthSvcCmp, SessionSvcCmp}
import spray.http._
import spray.httpx.SprayJsonSupport
import spray.httpx.marshalling._
import spray.routing.HttpService

import scala.async.Async.{async, await}
import scala.concurrent.Future

trait SessionControllerCmp extends HttpService with SprayJsonSupport {
  self: AppWideDefs with SessionSvcCmp with AuthSvcCmp =>

  val sessionController = new SessionController

  class SessionController {

    def create(req: CreateSessionReq): Future[HttpResponse] = async {
      await(authSvc.authenticate(req.username, req.password)) match {
        case Right((sessionId, role)) =>
          HttpResponse(StatusCodes.Created, marshalUnsafe(CreateSessionResp(sessionId, role)))
        case Left(failure) =>
          translateFailure(failure, StatusCodes.BadRequest)
      }
    }

    def delete(sessionId: String): Future[HttpResponse] =
      sessionSvc.delete(sessionId) map {
        case true =>
          HttpResponse(StatusCodes.NoContent)
        case false =>
          HttpResponse(StatusCodes.BadRequest)
      }

  }
}
