package art.team.controller.request

import spray.json.DefaultJsonProtocol

case class ExportToEmailReq(spec: GroupExportSpec)

object ExportToEmailReq extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(ExportToEmailReq.apply)
}
