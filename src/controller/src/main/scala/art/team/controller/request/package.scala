package art.team.controller

import art.team.json.JsonFormats._
import art.team.model.{Language, Owner}
import spray.json.JsonFormat

package object request {

  type GroupExportSpec = Map[Owner, Set[Language.Value]]

  implicit val groupExportSpecFormat = implicitly[JsonFormat[Map[Owner, Set[Language.Value]]]]

}
