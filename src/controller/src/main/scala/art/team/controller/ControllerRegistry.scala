package art.team.controller

import art.team.app.AppWideDefs
import art.team.serverrenderer.ServerRendererCmp
import art.team.service.SvcModuleRegistry

trait ControllerRegistry
  extends SessionControllerCmp
  with TicketControllerCmp
  with UserControllerCmp
  with CVControllerCmp
  with PrefsControllerCmp
  with ServerRendererControllerCmp {

  self: AppWideDefs with SvcModuleRegistry with ServerRendererCmp =>
}
