package art.team.controller.response

import art.team.json.JsonFormats.roleFormat
import art.team.model.Role
import spray.json.DefaultJsonProtocol

case class FetchRolesResp(roles: Map[String, Role.Value])

object FetchRolesResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(FetchRolesResp.apply)
}
