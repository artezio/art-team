package art.team.controller.response

import spray.json.DefaultJsonProtocol

case class CreateOrUpdateCVResp(version: Int)

object CreateOrUpdateCVResp extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat1(CreateOrUpdateCVResp.apply)
}
