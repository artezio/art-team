package art.team.controller.request

import art.team.json.JsonFormats._
import art.team.model.{CV, Language}
import spray.json.DefaultJsonProtocol

case class CreateOrUpdateCVReq(cv: CV, language: Language.Value, version: Int)

object CreateOrUpdateCVReq extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat3(CreateOrUpdateCVReq.apply)
}
