package art.team.auth

import art.team.app.AppWideDefs
import art.team.auth.AuthenticatorUtil._
import art.team.dto.TicketInfo
import art.team.service.TicketSvcCmp
import spray.routing.AuthenticationFailedRejection.{CredentialsMissing, CredentialsRejected}
import spray.routing.authentication.ContextAuthenticator
import spray.routing.{Rejection, RequestContext}

import scala.async.Async.{async, await}
import scala.concurrent.Future

trait TicketAuthenticatorCmp {
  self: AppWideDefs with TicketSvcCmp =>

  val TicketAuthenticator = new TicketAuthenticator

  class TicketAuthenticator extends ContextAuthenticator[TicketInfo] {
    override def apply(ctx: RequestContext): Future[Either[Rejection, TicketInfo]] = {
      getTicketId(ctx).fold(Future.successful(mkLeftResult[TicketInfo](mkAuthFailedRejection(CredentialsMissing)))) {
        ticketId =>
          async {
            await(ticketSvc.use(ticketId)).fold(mkLeftResult[TicketInfo](mkAuthFailedRejection(CredentialsRejected))) {
              ticketInfo =>
                Right(ticketInfo)
            }
          }
      }
    }

    // private

    private def getTicketId(ctx: RequestContext): Option[String] =
      ctx.request.uri.query.get("ticketId")
  }
}
