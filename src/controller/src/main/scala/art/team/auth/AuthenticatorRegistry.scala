package art.team.auth

import art.team.app.AppWideDefs
import art.team.service.{SessionSvcCmp, TicketSvcCmp, UserSvcCmp}

trait AuthenticatorRegistry
  extends ArtAuthAuthenticatorCmp
  with TicketAuthenticatorCmp {

  self: AppWideDefs with SessionSvcCmp with TicketSvcCmp with UserSvcCmp =>
}
