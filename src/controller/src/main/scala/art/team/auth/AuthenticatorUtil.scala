package art.team.auth

import spray.routing.AuthenticationFailedRejection.Cause
import spray.routing.{AuthenticationFailedRejection, Rejection}

object AuthenticatorUtil {
  def mkAuthFailedRejection(cause: Cause) = AuthenticationFailedRejection(cause, List.empty)
  def mkLeftResult[R](rejection: Rejection): Either[Rejection, R] = Left(rejection)
}
