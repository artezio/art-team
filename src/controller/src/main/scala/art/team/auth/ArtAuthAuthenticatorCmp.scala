package art.team.auth

import art.team.app.AppWideDefs
import art.team.auth.AuthenticatorUtil._
import art.team.service.{SessionSvcCmp, UserSvcCmp}
import spray.http.GenericHttpCredentials
import spray.http.HttpHeaders.Authorization
import spray.routing.AuthenticationFailedRejection.{CredentialsMissing, CredentialsRejected}
import spray.routing.authentication.ContextAuthenticator
import spray.routing.{Rejection, RequestContext}
import spray.util._

import scala.async.Async.{async, await}
import scala.concurrent.Future

trait ArtAuthAuthenticatorCmp {
  self: AppWideDefs with SessionSvcCmp with UserSvcCmp =>

  val ArtAuthAuthenticator = new ArtAuthAuthenticator

  class ArtAuthAuthenticator extends ContextAuthenticator[String] {
    override def apply(ctx: RequestContext): Future[Either[Rejection, String]] = {
      getSessionId(ctx).fold(Future.successful(mkLeftResult[String](mkAuthFailedRejection(CredentialsMissing)))) {
        sessionId =>
          async {
            await(sessionSvc.verifyAndGetUsername(sessionId)).
              fold(mkLeftResult[String](mkAuthFailedRejection(CredentialsRejected)))(Right(_))
          }
      }
    }

    // private

    private def getSessionId(ctx: RequestContext): Option[String] =
      ctx.request.headers.findByType[`Authorization`] flatMap { authHeader =>
        val creds = authHeader.credentials.asInstanceOf[GenericHttpCredentials]
        if (creds.scheme == "ARTAuth") creds.params.get("sessionId")
        else None
      }
  }
}
