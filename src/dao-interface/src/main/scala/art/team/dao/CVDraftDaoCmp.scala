package art.team.dao

import art.team.model.{CV, Language, Owner}
import org.joda.time.DateTime

import scala.concurrent.Future

trait CVDraftDaoCmp {
  def cvDraftDao: CVDraftDao

  trait CVDraftDao {
    def setDraft(cv: CV, owner: Owner, language: Language.Value, basedOnVersion: Int, lastModifiedBy: String): Future[_]
    def readDraft(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]]
    def clearDraft(owner: Owner, language: Language.Value): Future[Boolean]
  }
}
