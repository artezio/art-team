package art.team.dao

import art.team.model.{Role, User}

import scala.concurrent.Future

trait UserDaoCmp {
  def userDao: UserDao

  trait UserDao {
    def create(username: String, passwordHash: String, role: Role.Value): Future[Boolean]
    def read(username: String): Future[Option[User]]
    def expirePasswordHashes(): Future[Boolean]
    def renewPasswordHash(username: String, passwordHash: String): Future[Boolean]
    def fetchRoles(): Future[Map[String, Role.Value]]
    def updateUser(username: String, role: Option[Role.Value]): Future[Boolean]
  }
}
