package art.team.dao

import art.team.dto.TicketInfo

import scala.concurrent.Future

trait TicketDaoCmp {
  def ticketDao: TicketDao

  trait TicketDao {
    def create(username: String, ticketIdHash: String, data: Option[String]): Future[Boolean]
    def use(ticketIdHash: String): Future[Option[TicketInfo]]
  }
}
