package art.team.dao

import art.team.model.{Owner, CV, Language}

import scala.concurrent.Future

trait CVHistoryDaoCmp {
  def cvHistoryDao: CVHistoryDao

  trait CVHistoryDao {
    def create(cv: CV, owner: Owner, language: Language.Value, version: Int): Future[Boolean]
    def read(owner: Owner, language: Language.Value, version: Int): Future[Option[CV]]
  }
}
