package art.team.dao

import art.team.dto.CVExcerpt
import art.team.model.{CV, Language, LocationCode, Owner}
import org.joda.time.DateTime
import play.api.libs.iteratee.Enumerator

import scala.concurrent.Future

trait CVDaoCmp {
  def cvDao: CVDao

  trait CVDao {
    def create(cv: CV, owner: Owner, language: Language.Value): Future[Option[Int]]
    def read(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]]
    def update(cv: CV, owner: Owner, language: Language.Value,
               version: Int, lastModifiedBy: String): Future[Option[(CV, Int)]]

    def fetchExcerpts(): Future[Map[LocationCode, Seq[CVExcerpt]]]
    def fetchAll(): Enumerator[(CV, Owner, Language.Value)]
  }
}
