package art.team.dao

import art.team.model.Prefs

import scala.concurrent.Future

trait PrefsDaoCmp {
  def prefsDao: PrefsDao

  trait PrefsDao {
    def setPrefs(owner: String, prefs: Prefs): Future[_]
    def readPrefs(owner: String): Future[Option[Prefs]]
  }
}
