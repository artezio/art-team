package art.team.dao

import scala.concurrent.Future

trait SessionDaoCmp {
  def sessionDao: SessionDao

  trait SessionDao {
    def create(username: String, sessionIdHash: String): Future[Boolean]
    def delete(sessionIdHash: String): Future[Boolean]
    def getUsername(sessionIdHash: String): Future[Option[String]]
  }
}
