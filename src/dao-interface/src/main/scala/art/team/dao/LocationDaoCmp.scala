package art.team.dao

import art.team.model.Location

import scala.concurrent.Future

trait LocationDaoCmp {
  def locationDao: LocationDao

  trait LocationDao {
    def fetchAvailableLocations(): Future[Seq[Location]]
  }
}
