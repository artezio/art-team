package art.team.dao

trait DaoModuleRegistry
  extends SessionDaoCmp
  with TicketDaoCmp
  with UserDaoCmp
  with CVDaoCmp
  with CVDraftDaoCmp
  with CVHistoryDaoCmp
  with LocationDaoCmp
  with PrefsDaoCmp
