package art.team.service

sealed trait Failure

object GenericFailure extends Failure
object InternalServerError extends Failure
