package art.team.service

trait SvcModuleRegistry
  extends SessionSvcCmp
  with UserSvcCmp
  with TicketSvcCmp
  with AuthSvcCmp
  with CVSvcCmp
  with PrefsSvcCmp
