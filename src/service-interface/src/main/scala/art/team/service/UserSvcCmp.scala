package art.team.service

import art.team.model.{Role, User}

import scala.concurrent.Future

trait UserSvcCmp {
  def userSvc: UserSvc

  trait UserSvc {
    def create(username: String, passwordHash: String, role: Role.Value): Future[Boolean]
    def read(username: String): Future[Option[User]]
    def renewPasswordHash(username: String, passwordHash: String): Future[Boolean]
    def fetchRoles(): Future[Map[String, Role.Value]]
    def updateUser(username: String, role: Option[Role.Value]): Future[Boolean]
  }
}
