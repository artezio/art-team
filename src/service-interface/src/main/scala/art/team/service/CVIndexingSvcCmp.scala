package art.team.service

import art.team.dto.{CVExcerpt, SearchMode}
import art.team.model.{CV, Language, LocationCode, Owner}

trait CVIndexingSvcCmp {
  def indexingSvc: IndexingSvc

  trait IndexingSvc {
    def create(cv: CV, owner: Owner, language: Language.Value): Unit
    def update(cv: CV, owner: Owner, language: Language.Value): Unit

    def commit(): Unit
    def rollback(): Unit
    def close(): Unit

    def find(query: String, mode: SearchMode.Value): Map[Language.Value, Map[LocationCode, Map[Owner, CVExcerpt]]]
  }
}
