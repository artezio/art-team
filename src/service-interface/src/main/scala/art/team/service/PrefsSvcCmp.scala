package art.team.service

import art.team.model.Prefs

import scala.concurrent.Future

trait PrefsSvcCmp {
  def prefsSvc: PrefsSvc

  trait PrefsSvc {
    def setPrefs(owner: String, prefs: Prefs): Future[_]
    def readPrefs(owner: String): Future[Option[Prefs]]
  }
}
