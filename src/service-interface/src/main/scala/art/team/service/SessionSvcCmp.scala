package art.team.service

import scala.concurrent.Future

trait SessionSvcCmp {
  def sessionSvc: SessionSvc

  trait SessionSvc {
    def create(username: String): Future[Option[String]]
    def delete(sessionId: String): Future[Boolean]
    def verifyAndGetUsername(sessionId: String): Future[Option[String]]
  }
}
