package art.team.service

import art.team.common.Format
import art.team.dto._
import art.team.model._
import org.joda.time.DateTime

import scala.concurrent.Future

trait CVSvcCmp {
  def cvSvc: CVSvc

  trait CVSvc {
    def create(cv: CV, owner: Owner, language: Language.Value): Future[Option[Int]]
    def read(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]]
    def update(cv: CV, owner: Owner, language: Language.Value,
               version: Int, caller: String): Future[Either[Failure, Int]]

    def setDraft(cv: CV, owner: Owner, language: Language.Value,
                 basedOnVersion: Int, caller: String): Future[_]
    def readDraft(owner: Owner, language: Language.Value): Future[Option[(CV, Int, DateTime, String)]]
    def clearDraft(owner: Owner, language: Language.Value): Future[_]

    def fetchAvailableLocations(): Future[Seq[Location]]
    def fetchExcerpts(): Future[Map[LocationCode, Map[Owner, Map[Language.Value, CVExcerpt]]]]
    def find(query: String, mode: SearchMode.Value): Future[Map[Language.Value, Map[LocationCode, Map[Owner, CVExcerpt]]]]

    def export(owner: Owner, language: Language.Value, format: Format.Value): Future[Option[CVExport]]
    def groupExport(spec: Map[Owner, Set[Language.Value]]): Future[Option[CVGroupExport]]
    def exportToEmail(caller: String, spec: Map[Owner, Set[Language.Value]]): Future[Unit]
  }
}
