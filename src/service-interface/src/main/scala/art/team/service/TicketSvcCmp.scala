package art.team.service

import art.team.dto.TicketInfo

import scala.concurrent.Future

trait TicketSvcCmp {
  def ticketSvc: TicketSvc

  trait TicketSvc {
    def create(username: String, data: Option[String]): Future[Option[String]]
    def use(sessionId: String): Future[Option[TicketInfo]]
  }
}
