package art.team.service

import art.team.model.Role
import art.team.security.Task

import scala.concurrent.Future

trait AuthSvcCmp {
  def authSvc: AuthSvc

  trait AuthSvc {
    def authenticate(username: String, password: String): Future[Either[Failure, (String, Role.Value)]]
    def canPerform(caller: String, task: Task.Value): Future[Either[Failure, Boolean]]
  }
}
