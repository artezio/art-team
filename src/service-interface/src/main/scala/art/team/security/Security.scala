package art.team.security

import art.team.model.Role

object Security {
  val Config = Map(

    Role.User -> Set[Task.Value](
    ),

    Role.Admin -> Set(
      Task.ReadOtherPeopleCV,
      Task.ReadRoles,
      Task.SetRoles
    ),

    Role.HR -> Set(
      Task.ReadOtherPeopleCV
    ),

    Role.Manager -> Set(
      Task.ReadOtherPeopleCV
    )
  )
}
