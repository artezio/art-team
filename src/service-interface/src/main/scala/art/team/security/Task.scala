package art.team.security

object Task extends Enumeration {
  val ReadOtherPeopleCV = Value
  val ReadRoles = Value
  val SetRoles = Value
}
